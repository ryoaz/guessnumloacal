﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour {

    Text timerText;
    public float timer;

    bool isStoped = false;

	// Use this for initialization
	void Start () {

        timerText = GetComponent<Text>();
		
	}
	
	// Update is called once per frame
	void Update () {

        if (!isStoped)
        {
            timer -= Time.deltaTime;
        }

        timerText.text = "Time:" + Mathf.CeilToInt(timer).ToString();
		
	}

    public float GetTimer()
    {
        return timer;
    }

    public void StopTimer()
    {
        isStoped = true;
    }

    public void ResetTimer()
    {
        timer = 100;
    }
}
