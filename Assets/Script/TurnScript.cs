﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnScript : MonoBehaviour {

    int turnCount = 0;
    public Text turnText;

	// Use this for initialization
	void Start () {

        turnText.text = turnCount + "th Turn";
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TurnCount()
    {
        turnCount++;

        if(turnCount == 1)
        {
            turnText.text = turnCount + "st Turn";
        }
        else if(turnCount == 2)
        {
            turnText.text = turnCount + "nd Turn";
        }
        else if(turnCount == 3)
        {
            turnText.text = turnCount + "rd Turn";
        }
        else
        {
            turnText.text = turnCount + "th Turn";
        }
    }
}
