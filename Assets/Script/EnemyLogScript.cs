﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class EnemyLogScript : MonoBehaviourPunCallbacks {

    int hitCount = 0;
    int blowCount = 0;

    public GameObject enemyLogTextView;
    public GameObject content;

    public GameObject enter;
    Enter enterScript;

    EnemyLogTextView script;

	// Use this for initialization
	void Start () {

        enter = GameObject.Find("Enter");
        enterScript = enter.GetComponent<Enter>();

    }
	
	// Update is called once per frame
	void Update () {

    }

    public void JudgeHit(string enemy, string you)
    {
        if(enemy == you)
        {
            hitCount++;

            if (hitCount == 3)
            {
                enterScript.YouLose();
            }
        }
    }

    public void JudgeBlow(string enemy, string you)
    {
        if(enemy == you)
        {
            blowCount++;
        }
    }

    public void JudgeDisplay()
    {
        script = enemyLogTextView.GetComponent<EnemyLogTextView>();
        script.DisPlay(hitCount, blowCount);

        GameObject Obj;
        Obj = PhotonNetwork.Instantiate(enemyLogTextView.name, Vector3.zero, Quaternion.identity, 0);
        Obj.transform.parent = content.transform;

        hitCount = 0;
        blowCount = 0;
    }
}
