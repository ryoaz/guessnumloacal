﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HomeScript : MonoBehaviour {

    GameObject resultManager;
    ResultManager resultManagerScript;

    // Use this for initialization
    void Start () {

        resultManager = GameObject.Find("ResultManager");
        resultManagerScript = resultManager.GetComponent<ResultManager>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClick()
    {
        Destroy(resultManagerScript.gameObject);
        
        //タイトル画面に戻る
        SceneManager.LoadScene("Title");
    }
}
