﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class HighAndLowScr : MonoBehaviourPunCallbacks {

    string enemyFirstNum;
    string enemySecondNum;
    string enemyThirdNum;

    GameObject yourLTV;
    YourLogTextView yourLTVScript;
    GameObject yourContent;

    GameObject enemyLTV;
    EnemyLogTextView enemyLTVScript;
    GameObject enemyContent;

    string firstHL;
    string secondHL;
    string thirdHL;
    bool isUsedHL = false;

    // Use this for initialization
    void Start () {

        yourLTV = Resources.Load("YourLogTextView") as GameObject;
        yourContent = GameObject.Find("YourContent");

        enemyLTV = Resources.Load("EnemyLogTextView") as GameObject;
        enemyContent = GameObject.Find("EnemyContent");

    }

    // Update is called once per frame
    void Update () {
		
	}

    public void HighAndLow()
    {
        if (!isUsedHL)
        {
            enemyFirstNum = Enter.enemyFirstNum;
            enemySecondNum = Enter.enemySecondNum;
            enemyThirdNum = Enter.enemyThirdNum;

            int first = int.Parse(enemyFirstNum);
            int second = int.Parse(enemySecondNum);
            int third = int.Parse(enemyThirdNum);

            if (first <= 4)
            {
                firstHL = "↓";
            }
            else if (first >= 5)
            {
                firstHL = "↑";
            }

            if (second <= 4)
            {
                secondHL = "↓";
            }
            else if (second >= 5)
            {
                secondHL = "↑";
            }

            if (third <= 4)
            {
                thirdHL = "↓";
            }
            else if (third >= 5)
            {
                thirdHL = "↑";
            }

            yourLTVScript = yourLTV.GetComponent<YourLogTextView>();
            yourLTVScript.HLDisPlay(firstHL, secondHL, thirdHL);

            GameObject Obj;
            Obj = PhotonNetwork.Instantiate(yourLTV.name, Vector3.zero, Quaternion.identity, 0);
            Obj.transform.parent = yourContent.transform;

            //相手のログに自分がアイテムを使ったことを書く
            photonView.RPC("WriteEnemyHL", RpcTarget.Others, firstHL, secondHL, thirdHL);

            isUsedHL = true;
        }
    }

    [PunRPC]
    void WriteEnemyHL(string enemyFirstHL, string enemySecondHL, string enemyThirdHL)
    {
        enemyLTVScript = enemyLTV.GetComponent<EnemyLogTextView>();
        enemyLTVScript.HLDisPlay(enemyFirstHL, enemySecondHL, enemyThirdHL);

        GameObject Obj;
        Obj = PhotonNetwork.Instantiate(enemyLTV.name, Vector3.zero, Quaternion.identity, 0);
        Obj.transform.parent = enemyContent.transform;
    }
}
