﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultManager : MonoBehaviour {

    public string resultCoad;

	// Use this for initialization
	void Start () {

        resultCoad = "Result";
        DontDestroyOnLoad(this);

    }

    // Update is called once per frame
    void Update () {
		
	}

    public void YouWin()
    {
        resultCoad = "You Win!";
    }

    public void YouLose()
    {
        resultCoad = "You Lose...";
    }

    public void Draw()
    {
        resultCoad = "Draw";
    }

    //このオブジェクトを破壊する
    public void DestroyThis()
    {
        Destroy(this);
    }
}
