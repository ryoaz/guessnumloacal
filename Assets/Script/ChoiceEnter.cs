﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class ChoiceEnter : MonoBehaviourPunCallbacks {

    public static string setFirstNum;
    public static string setSecondNum;
    public static string setThirdNum;

    bool hasSetFirst = false;
    bool hasSetSecond = false;
    bool hasSetThird = false;
    bool hasSet = false;

    public GameObject numError;
    public GameObject enemyWaiting;

    bool isChoiceEntered = false;
    bool isChildChoiceEntered = false;

    bool hasLoad = false;

    public GameObject timerObject;
    float timer;

    public GameObject firstNum0;
    public GameObject secondNum1;
    public GameObject thirdNum2;

	// Use this for initialization
	void Start () {

        DontDestroyOnLoad(this);

        numError.SetActive(false);
        enemyWaiting.SetActive(false);
		
	}
	
	// Update is called once per frame
	void Update () {

        if(PhotonNetwork.IsMasterClient && isChoiceEntered && isChildChoiceEntered)
        {
            if (!hasLoad)
            {
                PhotonNetwork.LoadLevel("GamePlay");
            }

            hasLoad = true;
        }

        //timerの数値を取得
        timer = timerObject.GetComponent<TimerScript>().GetTimer();

        //timerが0になったときの処理
        if (timer < 0f)
        {
            if (!hasSet)
            {
                firstNum0.GetComponent<Image>().color = Color.yellow;
                secondNum1.GetComponent<Image>().color = Color.yellow;
                thirdNum2.GetComponent<Image>().color = Color.yellow;

                OnClick();
            }
        }
    }

    public void OnClick()
    {

        //選択したボタン(黄色のボタン)の数字(名前)をYourNumで取り出せるようにstatic関数に保存
        GameObject[] first = GameObject.FindGameObjectsWithTag("firstNum");
        foreach (GameObject obj in first)
        {
            if (obj.GetComponent<Image>().color == Color.yellow)
            {
                setFirstNum = obj.transform.name;

                hasSetFirst = true;
            }
        }

        GameObject[] second = GameObject.FindGameObjectsWithTag("secondNum");
        foreach (GameObject obj in second)
        {
            if (obj.GetComponent<Image>().color == Color.yellow)
            {
                setSecondNum = obj.transform.name;

                hasSetSecond = true;
            }
        }

        GameObject[] third = GameObject.FindGameObjectsWithTag("thirdNum");
        foreach (GameObject obj in third)
        {
            if (obj.GetComponent<Image>().color == Color.yellow)
            {
                setThirdNum = obj.transform.name;

                hasSetThird = true;
            }
        }

        //数字を選択してくださいと表示し、Enterを押さなかったことにする
        if (!hasSetFirst || !hasSetSecond || !hasSetThird)
        {
            numError.SetActive(true);

            hasSet = false;
        }

        //いづれアイテムも選択されている状態に
        if(hasSetFirst && hasSetSecond && hasSetThird)
        {
            hasSet = true;
        }

        //ちゃんとセット出来ているとき
        if (hasSet)
        {
            //親のとき自分のisChoiceEnteredをtrueにする
            if (PhotonNetwork.IsMasterClient)
            {
                isChoiceEntered = true;
            }

            //子のとき親のisChoiceEnteredをtureにする
            if (!PhotonNetwork.IsMasterClient)
            {
                photonView.RPC("IsChildChoiceEntered", RpcTarget.Others);
            }

            enemyWaiting.SetActive(true);
        }
    }

    [PunRPC]
    void IsChildChoiceEntered()
    {
        isChildChoiceEntered = true;
    }

}
