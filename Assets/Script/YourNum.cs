﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class YourNum : MonoBehaviour {

    public Text yourNumText;

    string yourFirstNum = ChoiceEnter.setFirstNum;
    string yourSecondNum = ChoiceEnter.setSecondNum;
    string yourThirdNum = ChoiceEnter.setThirdNum;

    // Use this for initialization
    void Start()
    {

        yourNumText.text = yourFirstNum + yourSecondNum + yourThirdNum;

    }

    // Update is called once per frame
    void Update () {
		
	}
}
