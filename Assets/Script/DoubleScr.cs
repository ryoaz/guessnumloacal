﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class DoubleScr : MonoBehaviourPunCallbacks {

    string enemyFirstNum;
    string enemySecondNum;
    string enemyThirdNum;

    GameObject yourLTV;
    YourLogTextView yourLTVScript;
    GameObject yourContent;
    GameObject enemyLTV;
    EnemyLogTextView enemyLTVScript;
    GameObject enemyContent;

    GameObject usingDouble;
    bool isUsedDouble = false;

    GameObject enter;
    Button enterButton;

    // Use this for initialization
    void Start () {
        yourLTV = Resources.Load("YourLogTextView") as GameObject;
        yourContent = GameObject.Find("YourContent");
        enemyLTV = Resources.Load("EnemyLogTextView") as GameObject;
        enemyContent = GameObject.Find("EnemyContent");

        usingDouble = GameObject.Find("UsingDouble");
        usingDouble.SetActive(false);

    }

    // Update is called once per frame
    void Update () {
		
	}

    public void Double()
    {
        usingDouble.SetActive(true);

        ChangeButton();
    }

    //EnterのOnClickをDoubleEnterに変更
    void ChangeButton()
    {
        enter = GameObject.Find("Enter");
        enterButton = enter.GetComponent<Button>();
        enterButton.onClick.AddListener(() => DoubleEnter());
    }

    //Double使用時にEnter押した時の処理
    public void DoubleEnter()
    {
        Debug.Log("DoubleEnter");
    }
}
