﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class EnemyLogTextView : MonoBehaviourPunCallbacks {

    public Text enemyLogText;
    string setEnemyNum;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void DisPlay(int hitCount, int blowCount)
    {
        setEnemyNum = Enter.setEnemyFirstNum + Enter.setEnemySecondNum + Enter.setEnemyThirdNum;

        enemyLogText.text = " " + setEnemyNum + " - " + hitCount.ToString() + "hit " + blowCount.ToString() + "blow";
    }

    public void HLDisPlay(string enemyFirstHL, string enemySecondHL, string enemyThirdHL)
    {
        enemyLogText.text = " High and Low - " + enemyFirstHL + " " + enemySecondHL + " " + enemyThirdHL;
    }
}
