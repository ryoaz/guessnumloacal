﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;

public class BackScript : MonoBehaviourPunCallbacks {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClick()
    {
        PhotonNetwork.LeaveRoom();
        Debug.Log("部屋から出ました");

        SceneManager.LoadScene("Title");
    }
}
