﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;

public class Enter : MonoBehaviourPunCallbacks {

    //YourNum
    string yourFirstNum = ChoiceEnter.setFirstNum;
    string yourSecondNum = ChoiceEnter.setSecondNum;
    string yourThirdNum = ChoiceEnter.setThirdNum;

    //EnemyNum
    public static string enemyFirstNum;
    public static string enemySecondNum;
    public static string enemyThirdNum;

    //Enter押したときの自分の数字
    public static string setFirstNum;
    public static string setSecondNum;
    public static string setThirdNum;

    //Enter押したときの相手の数字
    public static string setEnemyFirstNum;
    public static string setEnemySecondNum;
    public static string setEnemyThirdNum;

    GameObject yourLog;
    YourLogScript yourLogScript;

    GameObject enemyLog;
    EnemyLogScript enemyLogScript;

    GameObject turn;
    TurnScript turnScript;

    GameObject timerObject;
    TimerScript timerScript;
    float timer;

    GameObject resultManager;
    ResultManager resultManagerScript;

    GameObject choiceEnter;
    ChoiceEnter choiceEnterScript;

    public GameObject numError;
    public GameObject enemyWaiting;

    bool hasSetFirst = false;
    bool hasSetSecond = false;
    bool hasSetThird = false;
    bool hasSet = false;

    bool isEntered = false;
    bool isEnemyEntered = false;

    bool isFinishedReadingSetEnemyNum = false;

    bool isYouWin = false;
    bool isYouLose = false;
    bool hasLoad = false;

    // Use this for initialization
    void Start () {

        yourLog = GameObject.Find("YourLog");
        yourLogScript = yourLog.GetComponent<YourLogScript>();

        enemyLog = GameObject.Find("EnemyLog");
        enemyLogScript = enemyLog.GetComponent<EnemyLogScript>();

        turn = GameObject.Find("Turn");
        turnScript = turn.GetComponent<TurnScript>();

        timerObject = GameObject.Find("Timer");
        timerScript = timerObject.GetComponent<TimerScript>();

        resultManager = GameObject.Find("ResultManager");
        resultManagerScript = resultManager.GetComponent<ResultManager>();

        yourFirstNum = ChoiceEnter.setFirstNum;
        yourSecondNum = ChoiceEnter.setSecondNum;
        yourThirdNum = ChoiceEnter.setThirdNum;

        //相手が自分の数字を読み取る
        photonView.RPC("ReadEnemyNum", RpcTarget.Others, yourFirstNum, yourSecondNum, yourThirdNum);

        numError.SetActive(false);
        enemyWaiting.SetActive(false);

    }
	
	// Update is called once per frame
	void Update () {

        if (isEntered && isEnemyEntered)
        {
            //お互いに敵が入力した数字を読み取る
            photonView.RPC("ReadSetEnemyNum", RpcTarget.Others, setFirstNum, setSecondNum, setThirdNum);

            isEntered = false;
            isEnemyEntered = false;
            hasSet = false;
            hasSetFirst = false;
            hasSetSecond = false;
            hasSetThird = false;
            numError.SetActive(false);
            enemyWaiting.SetActive(false);
        }

        if (isFinishedReadingSetEnemyNum)
        {
            //自分が入力した数字と相手の数字でジャッジ
            yourLogScript.JudgeHit(setFirstNum, enemyFirstNum);
            yourLogScript.JudgeHit(setSecondNum, enemySecondNum);
            yourLogScript.JudgeHit(setThirdNum, enemyThirdNum);
            yourLogScript.JudgeBlow(setFirstNum, enemySecondNum);
            yourLogScript.JudgeBlow(setFirstNum, enemyThirdNum);
            yourLogScript.JudgeBlow(setSecondNum, enemyFirstNum);
            yourLogScript.JudgeBlow(setSecondNum, enemyThirdNum);
            yourLogScript.JudgeBlow(setThirdNum, enemyFirstNum);
            yourLogScript.JudgeBlow(setThirdNum, enemySecondNum);
            yourLogScript.JudgeDisplay();

            //相手が入力した数字と自分の数字でジャッジ
            enemyLogScript.JudgeHit(setEnemyFirstNum, yourFirstNum);
            enemyLogScript.JudgeHit(setEnemySecondNum, yourSecondNum);
            enemyLogScript.JudgeHit(setEnemyThirdNum, yourThirdNum);
            enemyLogScript.JudgeBlow(setEnemyFirstNum, yourSecondNum);
            enemyLogScript.JudgeBlow(setEnemyFirstNum, yourThirdNum);
            enemyLogScript.JudgeBlow(setEnemySecondNum, yourFirstNum);
            enemyLogScript.JudgeBlow(setEnemySecondNum, yourThirdNum);
            enemyLogScript.JudgeBlow(setEnemyThirdNum, yourFirstNum);
            enemyLogScript.JudgeBlow(setEnemyThirdNum, yourSecondNum);
            enemyLogScript.JudgeDisplay();

            turnScript.TurnCount();

            GameObject[] first = GameObject.FindGameObjectsWithTag("firstNum");
            foreach (GameObject obj in first)
            {
                obj.GetComponent<Image>().color = Color.white;
            }

            GameObject[] second = GameObject.FindGameObjectsWithTag("secondNum");
            foreach (GameObject obj in second)
            {
                obj.GetComponent<Image>().color = Color.white;
            }

            GameObject[] third = GameObject.FindGameObjectsWithTag("thirdNum");
            foreach (GameObject obj in third)
            {
                obj.GetComponent<Image>().color = Color.white;
            }

            //タイマーをリセット
            timerScript.ResetTimer();

            isFinishedReadingSetEnemyNum = false;
        }

        //timerの数値を取得
        timer = timerObject.GetComponent<TimerScript>().GetTimer();
        //timerが0になった時の処理
        if (timer < 0)
        {
            isYouLose = true;
        }

        //ドローの時のシーン遷移
        if (isYouWin && isYouLose)
        {
            if (!hasLoad)
            {
                PhotonNetwork.LeaveRoom();

                resultManagerScript.Draw();

                SceneManager.LoadScene("Result");
            }

            hasLoad = true;
        }
        //あなたが勝っている時のシーン遷移
        else if (isYouWin)
        {
            if (!hasLoad)
            {
                //部屋から出る
                PhotonNetwork.LeaveRoom();

                resultManagerScript.YouWin();

                SceneManager.LoadScene("Result");
            }

            hasLoad = true;
        }
        //敵が勝っている時のシーン遷移
        else if (isYouLose)
        {
            if (!hasLoad)
            {
                PhotonNetwork.LeaveRoom();

                resultManagerScript.YouLose();

                SceneManager.LoadScene("Result");
            }

            hasLoad = true;
        }
    }

    public void OnClick()
    {

        GameObject[] first = GameObject.FindGameObjectsWithTag("firstNum");
        foreach (GameObject obj in first)
        {
            if (obj.GetComponent<Image>().color == Color.yellow)
            {
                setFirstNum = obj.transform.name;

                hasSetFirst = true;
            }
        }

        GameObject[] second = GameObject.FindGameObjectsWithTag("secondNum");
        foreach (GameObject obj in second)
        {
            if (obj.GetComponent<Image>().color == Color.yellow)
            {
                setSecondNum = obj.transform.name;

                hasSetSecond = true;
            }
        }

        GameObject[] third = GameObject.FindGameObjectsWithTag("thirdNum");
        foreach (GameObject obj in third)
        {
            if (obj.GetComponent<Image>().color == Color.yellow)
            {
                setThirdNum = obj.transform.name;

                hasSetThird = true;
            }
        }

        //数字を選択してくださいと表示し、Enterを押さなかったことにする
        if (!hasSetFirst || !hasSetSecond || !hasSetThird)
        {
            numError.SetActive(true);

            hasSet = false;
        }

        //すべてセット出来ている
        if (hasSetFirst && hasSetSecond && hasSetThird)
        {
            hasSet = true;
        }

        //ちゃんとセット出来ているとき
        if (hasSet)
        {
            isEntered = true;
            photonView.RPC("IsEnemyEnterd", RpcTarget.Others);

            enemyWaiting.SetActive(true);

            timerScript.StopTimer();
        }
    }

    //あなたが勝ったかの判断
    public void YouWin()
    {
        isYouWin = true;
    }

    //敵が勝ったかの判断
    public void YouLose()
    {
        isYouLose = true;
    }

    //相手の数字を読み取る
    [PunRPC]
    void ReadEnemyNum(string _enemyFirstNum, string _enemySecondNum, string _enemyThirdNum)
    {
        enemyFirstNum = _enemyFirstNum;
        enemySecondNum = _enemySecondNum;
        enemyThirdNum = _enemyThirdNum;
    }

    //相手が入力した数字を読み取る
    [PunRPC]
    void ReadSetEnemyNum(string _setEnemyFirstNum, string _setEnemySecondNum, string _setEnemyThirdNum)
    {
        setEnemyFirstNum = _setEnemyFirstNum;
        setEnemySecondNum = _setEnemySecondNum;
        setEnemyThirdNum = _setEnemyThirdNum;

        isFinishedReadingSetEnemyNum = true;
    }

    [PunRPC]
    void IsEnemyEnterd()
    {
        isEnemyEntered = true;
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        isYouWin = true;
    }

}
