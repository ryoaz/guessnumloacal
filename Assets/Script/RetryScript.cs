﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RetryScript : MonoBehaviour {

    GameObject resultManager;
    ResultManager resultManagerScript;

    // Use this for initialization
    void Start () {

        resultManager = GameObject.Find("ResultManager");
        resultManagerScript = resultManager.GetComponent<ResultManager>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClick()
    {
        Destroy(resultManager);

        SceneManager.LoadScene("RandomMathing");
    }
}
