﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class YourLogTextView : MonoBehaviourPunCallbacks {

    public Text yourLogText; 
    string setYourNum;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Display(int hitCount, int blowCount)
    {
        setYourNum = Enter.setFirstNum + Enter.setSecondNum + Enter.setThirdNum;

        yourLogText.text = " " + setYourNum + " - " + hitCount.ToString() + "hit " + blowCount.ToString() + "blow";
    }

    public void HLDisPlay(string firstHL, string secondHL, string thirdHL)
    {
        yourLogText.text = " High and Low - " + firstHL + " " + secondHL + " " + thirdHL;
    }
}
