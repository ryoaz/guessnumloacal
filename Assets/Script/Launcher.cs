﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class Launcher : MonoBehaviourPunCallbacks {

    string gameVersion = "1.0";

    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    // Use this for initialization
    void Start () {

        Connect();
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Connect()
    {
        PhotonNetwork.GameVersion = gameVersion;

        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.JoinRandomRoom();
        }
        else
        {
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinRandomRoom();
        Debug.Log("マスターに繋がりました");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.LogFormat("接続が切れました {0}", cause);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("部屋に入りました");
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("誰もいませんでした");
        PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = 2 });
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("部屋を作りました");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel("Choice");
        }
    }
}
