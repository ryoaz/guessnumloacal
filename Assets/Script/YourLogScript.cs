﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class YourLogScript : MonoBehaviourPunCallbacks {

    int hitCount = 0;
    int blowCount = 0;

    public GameObject yourLogTextView;
    public GameObject content;
    YourLogTextView script;

    GameObject enter;
    Enter enterScript;

    string setYourNum;

    // Use this for initialization
    void Start () {

        enter = GameObject.Find("Enter");
        enterScript = enter.GetComponent<Enter>();

	}
	
	// Update is called once per frame
	void Update () {

    }

    public void JudgeHit(string you, string enemy)
    {
        if (you == enemy)
        {
            hitCount++;

            if (hitCount == 3)
            {
                enterScript.YouWin();
            }
        }
    }

    public void JudgeBlow(string you, string enemy)
    {
        if (you == enemy)
        {
            blowCount++;
        }
    }

    //PhotonNetのInstantiateで生成する！
    public void JudgeDisplay()
    {
        script = yourLogTextView.GetComponent<YourLogTextView>();
        script.Display(hitCount, blowCount);

        GameObject Obj;
        Obj = PhotonNetwork.Instantiate(yourLogTextView.name, Vector3.zero, Quaternion.identity, 0);
        Obj.transform.parent = content.transform;

        hitCount = 0;
        blowCount = 0;
    }
}
