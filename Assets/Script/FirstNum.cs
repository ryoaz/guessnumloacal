﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FirstNum : MonoBehaviour {

    string yellowNum;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClick()
    {

        //firstNumの色を白に変える
        GameObject[] first = GameObject.FindGameObjectsWithTag("firstNum");
        foreach (GameObject obj in first)
        {
            obj.GetComponent<Image>().color = Color.white;
        }

        //押したボタンの色のみ黄色にする
        GetComponent<Image>().color = Color.yellow;

        yellowNum = this.name;

        //同じ数字を押したとき黄色にならなくする
        GameObject[] second = GameObject.FindGameObjectsWithTag("secondNum");
        foreach (GameObject obj in second)
        {
            if (obj.GetComponent<Image>().color == Color.yellow)
            {
                if (obj.transform.name == yellowNum)
                {
                    obj.GetComponent<Image>().color = Color.white;
                }
            }
        }
        GameObject[] third = GameObject.FindGameObjectsWithTag("thirdNum");
        foreach (GameObject obj in third)
        {
            if (obj.GetComponent<Image>().color == Color.yellow)
            {
                if (obj.transform.name == yellowNum)
                {
                    obj.GetComponent<Image>().color = Color.white;
                }
            }
        }
    }
}
