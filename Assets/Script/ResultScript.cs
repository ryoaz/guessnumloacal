﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultScript : MonoBehaviour {

    public string resultCoad;

    public Text resultText;

    GameObject resultManager;
    ResultManager resultManagerScript;

	// Use this for initialization
	void Start () {

        resultText = GetComponent<Text>();

        resultManager = GameObject.Find("ResultManager");
        resultManagerScript = resultManager.GetComponent<ResultManager>();

        resultCoad = resultManagerScript.resultCoad;

        ResultDisplay();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ResultDisplay()
    {
        resultText.text = resultCoad;
    }
}
