﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ChatGui
struct ChatGui_t3673337520;
// ExitGames.Client.Photon.EncryptorManaged.Decryptor
struct Decryptor_t2116099858;
// ExitGames.Client.Photon.EncryptorManaged.Encryptor
struct Encryptor_t200327285;
// ExitGames.Client.Photon.IPhotonPeerListener
struct IPhotonPeerListener_t2581629031;
// ExitGames.Client.Photon.PeerBase
struct PeerBase_t2956237011;
// ExitGames.Client.Photon.PhotonPeer
struct PhotonPeer_t1608153861;
// ExitGames.Client.Photon.TrafficStats
struct TrafficStats_t1302902347;
// ExitGames.Client.Photon.TrafficStatsGameLevel
struct TrafficStatsGameLevel_t4013908777;
// Photon.Chat.ChatClient
struct ChatClient_t792052210;
// Photon.Pun.PhotonView
struct PhotonView_t3684715584;
// Photon.Pun.UtilityScripts.CellTree
struct CellTree_t656254725;
// Photon.Pun.UtilityScripts.CellTreeNode
struct CellTreeNode_t3709723763;
// Photon.Pun.UtilityScripts.CountdownTimer/CountdownTimerHasExpired
struct CountdownTimerHasExpired_t4234756006;
// Photon.Pun.UtilityScripts.CullArea
struct CullArea_t635391622;
// Photon.Pun.UtilityScripts.IPunTurnManagerCallbacks
struct IPunTurnManagerCallbacks_t1795442957;
// Photon.Pun.UtilityScripts.OnClickDestroy
struct OnClickDestroy_t3019334366;
// Photon.Pun.UtilityScripts.OnClickRpc
struct OnClickRpc_t2528495255;
// Photon.Pun.UtilityScripts.PlayerNumbering/PlayerNumberingChanged
struct PlayerNumberingChanged_t966975091;
// Photon.Pun.UtilityScripts.TabViewManager
struct TabViewManager_t3686055887;
// Photon.Pun.UtilityScripts.TabViewManager/Tab
struct Tab_t117203701;
// Photon.Pun.UtilityScripts.TabViewManager/TabChangeEvent
struct TabChangeEvent_t3080003849;
// Photon.Pun.UtilityScripts.TabViewManager/Tab[]
struct TabU5BU5D_t533311896;
// Photon.Realtime.AppSettings
struct AppSettings_t3860110421;
// Photon.Realtime.ConnectionHandler
struct ConnectionHandler_t3678426725;
// Photon.Realtime.LoadBalancingClient
struct LoadBalancingClient_t609581828;
// Photon.Realtime.Player[]
struct PlayerU5BU5D_t3651776216;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>
struct Dictionary_2_t1720840067;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type>
struct Dictionary_2_t1253839074;
// System.Collections.Generic.Dictionary`2<Photon.Pun.UtilityScripts.PunTeams/Team,System.Collections.Generic.List`1<Photon.Realtime.Player>>
struct Dictionary_2_t660995199;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject>
struct Dictionary_2_t2349950;
// System.Collections.Generic.Dictionary`2<System.String,FriendItem>
struct Dictionary_2_t2701966392;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UI.Toggle>
struct Dictionary_2_t2520633360;
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.Toggle,Photon.Pun.UtilityScripts.TabViewManager/Tab>
struct Dictionary_2_t1152131052;
// System.Collections.Generic.HashSet`1<Photon.Realtime.Player>
struct HashSet_1_t1444519063;
// System.Collections.Generic.List`1<Photon.Pun.UtilityScripts.CellTreeNode>
struct List_1_t886831209;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t2606371118;
// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam>
struct List_1_t3952196670;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget>
struct List_1_t235857739;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Diagnostics.Stopwatch
struct Stopwatch_t305734070;
// System.Func`2<Photon.Realtime.Player,System.Int32>
struct Func_2_t3125806854;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct AnalyticsEventParamListContainer_t587083383;
// UnityEngine.Analytics.AnalyticsEventTracker
struct AnalyticsEventTracker_t2285229262;
// UnityEngine.Analytics.EventTrigger
struct EventTrigger_t2527451695;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.StandardEventPayload
struct StandardEventPayload_t1629891255;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.TrackableProperty
struct TrackableProperty_t3943537984;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t123837990;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;




#ifndef U3CMODULEU3E_T692745551_H
#define U3CMODULEU3E_T692745551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745551 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745551_H
#ifndef U3CMODULEU3E_T692745549_H
#define U3CMODULEU3E_T692745549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745549 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745549_H
#ifndef U3CMODULEU3E_T692745550_H
#define U3CMODULEU3E_T692745550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745550 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745550_H
#ifndef U3CMODULEU3E_T692745552_H
#define U3CMODULEU3E_T692745552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745552 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745552_H
#ifndef U3CMODULEU3E_T692745548_H
#define U3CMODULEU3E_T692745548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745548 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745548_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CHATOPERATIONCODE_T2913553512_H
#define CHATOPERATIONCODE_T2913553512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Chat.ChatOperationCode
struct  ChatOperationCode_t2913553512  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATOPERATIONCODE_T2913553512_H
#ifndef CHATPARAMETERCODE_T3986735616_H
#define CHATPARAMETERCODE_T3986735616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Chat.ChatParameterCode
struct  ChatParameterCode_t3986735616  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATPARAMETERCODE_T3986735616_H
#ifndef CHATUSERSTATUS_T4200752496_H
#define CHATUSERSTATUS_T4200752496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Chat.ChatUserStatus
struct  ChatUserStatus_t4200752496  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATUSERSTATUS_T4200752496_H
#ifndef ERRORCODE_T424885541_H
#define ERRORCODE_T424885541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Chat.ErrorCode
struct  ErrorCode_t424885541  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORCODE_T424885541_H
#ifndef PARAMETERCODE_T52387945_H
#define PARAMETERCODE_T52387945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Chat.ParameterCode
struct  ParameterCode_t52387945  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERCODE_T52387945_H
#ifndef CELLTREE_T656254725_H
#define CELLTREE_T656254725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.CellTree
struct  CellTree_t656254725  : public RuntimeObject
{
public:
	// Photon.Pun.UtilityScripts.CellTreeNode Photon.Pun.UtilityScripts.CellTree::<RootNode>k__BackingField
	CellTreeNode_t3709723763 * ___U3CRootNodeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CRootNodeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CellTree_t656254725, ___U3CRootNodeU3Ek__BackingField_0)); }
	inline CellTreeNode_t3709723763 * get_U3CRootNodeU3Ek__BackingField_0() const { return ___U3CRootNodeU3Ek__BackingField_0; }
	inline CellTreeNode_t3709723763 ** get_address_of_U3CRootNodeU3Ek__BackingField_0() { return &___U3CRootNodeU3Ek__BackingField_0; }
	inline void set_U3CRootNodeU3Ek__BackingField_0(CellTreeNode_t3709723763 * value)
	{
		___U3CRootNodeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootNodeU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CELLTREE_T656254725_H
#ifndef U3CDESTROYRPCU3EC__ITERATOR0_T785359983_H
#define U3CDESTROYRPCU3EC__ITERATOR0_T785359983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.OnClickDestroy/<DestroyRpc>c__Iterator0
struct  U3CDestroyRpcU3Ec__Iterator0_t785359983  : public RuntimeObject
{
public:
	// Photon.Pun.UtilityScripts.OnClickDestroy Photon.Pun.UtilityScripts.OnClickDestroy/<DestroyRpc>c__Iterator0::$this
	OnClickDestroy_t3019334366 * ___U24this_0;
	// System.Object Photon.Pun.UtilityScripts.OnClickDestroy/<DestroyRpc>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Photon.Pun.UtilityScripts.OnClickDestroy/<DestroyRpc>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Photon.Pun.UtilityScripts.OnClickDestroy/<DestroyRpc>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDestroyRpcU3Ec__Iterator0_t785359983, ___U24this_0)); }
	inline OnClickDestroy_t3019334366 * get_U24this_0() const { return ___U24this_0; }
	inline OnClickDestroy_t3019334366 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(OnClickDestroy_t3019334366 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDestroyRpcU3Ec__Iterator0_t785359983, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDestroyRpcU3Ec__Iterator0_t785359983, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDestroyRpcU3Ec__Iterator0_t785359983, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESTROYRPCU3EC__ITERATOR0_T785359983_H
#ifndef PLAYERNUMBERINGEXTENSIONS_T2795551351_H
#define PLAYERNUMBERINGEXTENSIONS_T2795551351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.PlayerNumberingExtensions
struct  PlayerNumberingExtensions_t2795551351  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERNUMBERINGEXTENSIONS_T2795551351_H
#ifndef SCOREEXTENSIONS_T2818287168_H
#define SCOREEXTENSIONS_T2818287168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.ScoreExtensions
struct  ScoreExtensions_t2818287168  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOREEXTENSIONS_T2818287168_H
#ifndef U3CSTARTU3EC__ANONSTOREY0_T1921436154_H
#define U3CSTARTU3EC__ANONSTOREY0_T1921436154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.TabViewManager/<Start>c__AnonStorey0
struct  U3CStartU3Ec__AnonStorey0_t1921436154  : public RuntimeObject
{
public:
	// Photon.Pun.UtilityScripts.TabViewManager/Tab Photon.Pun.UtilityScripts.TabViewManager/<Start>c__AnonStorey0::_tab
	Tab_t117203701 * ____tab_0;
	// Photon.Pun.UtilityScripts.TabViewManager Photon.Pun.UtilityScripts.TabViewManager/<Start>c__AnonStorey0::$this
	TabViewManager_t3686055887 * ___U24this_1;

public:
	inline static int32_t get_offset_of__tab_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t1921436154, ____tab_0)); }
	inline Tab_t117203701 * get__tab_0() const { return ____tab_0; }
	inline Tab_t117203701 ** get_address_of__tab_0() { return &____tab_0; }
	inline void set__tab_0(Tab_t117203701 * value)
	{
		____tab_0 = value;
		Il2CppCodeGenWriteBarrier((&____tab_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t1921436154, ___U24this_1)); }
	inline TabViewManager_t3686055887 * get_U24this_1() const { return ___U24this_1; }
	inline TabViewManager_t3686055887 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TabViewManager_t3686055887 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ANONSTOREY0_T1921436154_H
#ifndef TAB_T117203701_H
#define TAB_T117203701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.TabViewManager/Tab
struct  Tab_t117203701  : public RuntimeObject
{
public:
	// System.String Photon.Pun.UtilityScripts.TabViewManager/Tab::ID
	String_t* ___ID_0;
	// UnityEngine.UI.Toggle Photon.Pun.UtilityScripts.TabViewManager/Tab::Toggle
	Toggle_t2735377061 * ___Toggle_1;
	// UnityEngine.RectTransform Photon.Pun.UtilityScripts.TabViewManager/Tab::View
	RectTransform_t3704657025 * ___View_2;

public:
	inline static int32_t get_offset_of_ID_0() { return static_cast<int32_t>(offsetof(Tab_t117203701, ___ID_0)); }
	inline String_t* get_ID_0() const { return ___ID_0; }
	inline String_t** get_address_of_ID_0() { return &___ID_0; }
	inline void set_ID_0(String_t* value)
	{
		___ID_0 = value;
		Il2CppCodeGenWriteBarrier((&___ID_0), value);
	}

	inline static int32_t get_offset_of_Toggle_1() { return static_cast<int32_t>(offsetof(Tab_t117203701, ___Toggle_1)); }
	inline Toggle_t2735377061 * get_Toggle_1() const { return ___Toggle_1; }
	inline Toggle_t2735377061 ** get_address_of_Toggle_1() { return &___Toggle_1; }
	inline void set_Toggle_1(Toggle_t2735377061 * value)
	{
		___Toggle_1 = value;
		Il2CppCodeGenWriteBarrier((&___Toggle_1), value);
	}

	inline static int32_t get_offset_of_View_2() { return static_cast<int32_t>(offsetof(Tab_t117203701, ___View_2)); }
	inline RectTransform_t3704657025 * get_View_2() const { return ___View_2; }
	inline RectTransform_t3704657025 ** get_address_of_View_2() { return &___View_2; }
	inline void set_View_2(RectTransform_t3704657025 * value)
	{
		___View_2 = value;
		Il2CppCodeGenWriteBarrier((&___View_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAB_T117203701_H
#ifndef TEAMEXTENSIONS_T1434078711_H
#define TEAMEXTENSIONS_T1434078711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.TeamExtensions
struct  TeamExtensions_t1434078711  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAMEXTENSIONS_T1434078711_H
#ifndef TURNEXTENSIONS_T575626914_H
#define TURNEXTENSIONS_T575626914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.TurnExtensions
struct  TurnExtensions_t575626914  : public RuntimeObject
{
public:

public:
};

struct TurnExtensions_t575626914_StaticFields
{
public:
	// System.String Photon.Pun.UtilityScripts.TurnExtensions::TurnPropKey
	String_t* ___TurnPropKey_0;
	// System.String Photon.Pun.UtilityScripts.TurnExtensions::TurnStartPropKey
	String_t* ___TurnStartPropKey_1;
	// System.String Photon.Pun.UtilityScripts.TurnExtensions::FinishedTurnPropKey
	String_t* ___FinishedTurnPropKey_2;

public:
	inline static int32_t get_offset_of_TurnPropKey_0() { return static_cast<int32_t>(offsetof(TurnExtensions_t575626914_StaticFields, ___TurnPropKey_0)); }
	inline String_t* get_TurnPropKey_0() const { return ___TurnPropKey_0; }
	inline String_t** get_address_of_TurnPropKey_0() { return &___TurnPropKey_0; }
	inline void set_TurnPropKey_0(String_t* value)
	{
		___TurnPropKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___TurnPropKey_0), value);
	}

	inline static int32_t get_offset_of_TurnStartPropKey_1() { return static_cast<int32_t>(offsetof(TurnExtensions_t575626914_StaticFields, ___TurnStartPropKey_1)); }
	inline String_t* get_TurnStartPropKey_1() const { return ___TurnStartPropKey_1; }
	inline String_t** get_address_of_TurnStartPropKey_1() { return &___TurnStartPropKey_1; }
	inline void set_TurnStartPropKey_1(String_t* value)
	{
		___TurnStartPropKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___TurnStartPropKey_1), value);
	}

	inline static int32_t get_offset_of_FinishedTurnPropKey_2() { return static_cast<int32_t>(offsetof(TurnExtensions_t575626914_StaticFields, ___FinishedTurnPropKey_2)); }
	inline String_t* get_FinishedTurnPropKey_2() const { return ___FinishedTurnPropKey_2; }
	inline String_t** get_address_of_FinishedTurnPropKey_2() { return &___FinishedTurnPropKey_2; }
	inline void set_FinishedTurnPropKey_2(String_t* value)
	{
		___FinishedTurnPropKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___FinishedTurnPropKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TURNEXTENSIONS_T575626914_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ANALYTICSEVENT_T4058973021_H
#define ANALYTICSEVENT_T4058973021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEvent
struct  AnalyticsEvent_t4058973021  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEvent_t4058973021_StaticFields
{
public:
	// System.String UnityEngine.Analytics.AnalyticsEvent::k_SdkVersion
	String_t* ___k_SdkVersion_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsEvent::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEvent::_debugMode
	bool ____debugMode_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Analytics.AnalyticsEvent::enumRenameTable
	Dictionary_2_t1632706988 * ___enumRenameTable_3;

public:
	inline static int32_t get_offset_of_k_SdkVersion_0() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___k_SdkVersion_0)); }
	inline String_t* get_k_SdkVersion_0() const { return ___k_SdkVersion_0; }
	inline String_t** get_address_of_k_SdkVersion_0() { return &___k_SdkVersion_0; }
	inline void set_k_SdkVersion_0(String_t* value)
	{
		___k_SdkVersion_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_SdkVersion_0), value);
	}

	inline static int32_t get_offset_of_m_EventData_1() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___m_EventData_1)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_1() const { return ___m_EventData_1; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_1() { return &___m_EventData_1; }
	inline void set_m_EventData_1(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_1), value);
	}

	inline static int32_t get_offset_of__debugMode_2() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ____debugMode_2)); }
	inline bool get__debugMode_2() const { return ____debugMode_2; }
	inline bool* get_address_of__debugMode_2() { return &____debugMode_2; }
	inline void set__debugMode_2(bool value)
	{
		____debugMode_2 = value;
	}

	inline static int32_t get_offset_of_enumRenameTable_3() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___enumRenameTable_3)); }
	inline Dictionary_2_t1632706988 * get_enumRenameTable_3() const { return ___enumRenameTable_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_enumRenameTable_3() { return &___enumRenameTable_3; }
	inline void set_enumRenameTable_3(Dictionary_2_t1632706988 * value)
	{
		___enumRenameTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___enumRenameTable_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENT_T4058973021_H
#ifndef ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#define ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct  AnalyticsEventParamListContainer_t587083383  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam> UnityEngine.Analytics.AnalyticsEventParamListContainer::m_Parameters
	List_1_t3952196670 * ___m_Parameters_0;

public:
	inline static int32_t get_offset_of_m_Parameters_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParamListContainer_t587083383, ___m_Parameters_0)); }
	inline List_1_t3952196670 * get_m_Parameters_0() const { return ___m_Parameters_0; }
	inline List_1_t3952196670 ** get_address_of_m_Parameters_0() { return &___m_Parameters_0; }
	inline void set_m_Parameters_0(List_1_t3952196670 * value)
	{
		___m_Parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifndef U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#define U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0
struct  U3CTimedTriggerU3Ec__Iterator0_t3813435494  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventTracker UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$this
	AnalyticsEventTracker_t2285229262 * ___U24this_0;
	// System.Object UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24this_0)); }
	inline AnalyticsEventTracker_t2285229262 * get_U24this_0() const { return ___U24this_0; }
	inline AnalyticsEventTracker_t2285229262 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnalyticsEventTracker_t2285229262 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifndef ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#define ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTrackerSettings
struct  AnalyticsEventTrackerSettings_t480422680  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEventTrackerSettings_t480422680_StaticFields
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::paramCountMax
	int32_t ___paramCountMax_0;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::triggerRuleCountMax
	int32_t ___triggerRuleCountMax_1;

public:
	inline static int32_t get_offset_of_paramCountMax_0() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___paramCountMax_0)); }
	inline int32_t get_paramCountMax_0() const { return ___paramCountMax_0; }
	inline int32_t* get_address_of_paramCountMax_0() { return &___paramCountMax_0; }
	inline void set_paramCountMax_0(int32_t value)
	{
		___paramCountMax_0 = value;
	}

	inline static int32_t get_offset_of_triggerRuleCountMax_1() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___triggerRuleCountMax_1)); }
	inline int32_t get_triggerRuleCountMax_1() const { return ___triggerRuleCountMax_1; }
	inline int32_t* get_address_of_triggerRuleCountMax_1() { return &___triggerRuleCountMax_1; }
	inline void set_triggerRuleCountMax_1(int32_t value)
	{
		___triggerRuleCountMax_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifndef STANDARDEVENTPAYLOAD_T1629891255_H
#define STANDARDEVENTPAYLOAD_T1629891255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.StandardEventPayload
struct  StandardEventPayload_t1629891255  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.StandardEventPayload::m_IsEventExpanded
	bool ___m_IsEventExpanded_0;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_StandardEventType
	String_t* ___m_StandardEventType_1;
	// System.Type UnityEngine.Analytics.StandardEventPayload::standardEventType
	Type_t * ___standardEventType_2;
	// UnityEngine.Analytics.AnalyticsEventParamListContainer UnityEngine.Analytics.StandardEventPayload::m_Parameters
	AnalyticsEventParamListContainer_t587083383 * ___m_Parameters_3;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_Name
	String_t* ___m_Name_5;

public:
	inline static int32_t get_offset_of_m_IsEventExpanded_0() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_IsEventExpanded_0)); }
	inline bool get_m_IsEventExpanded_0() const { return ___m_IsEventExpanded_0; }
	inline bool* get_address_of_m_IsEventExpanded_0() { return &___m_IsEventExpanded_0; }
	inline void set_m_IsEventExpanded_0(bool value)
	{
		___m_IsEventExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_StandardEventType_1() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_StandardEventType_1)); }
	inline String_t* get_m_StandardEventType_1() const { return ___m_StandardEventType_1; }
	inline String_t** get_address_of_m_StandardEventType_1() { return &___m_StandardEventType_1; }
	inline void set_m_StandardEventType_1(String_t* value)
	{
		___m_StandardEventType_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_StandardEventType_1), value);
	}

	inline static int32_t get_offset_of_standardEventType_2() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___standardEventType_2)); }
	inline Type_t * get_standardEventType_2() const { return ___standardEventType_2; }
	inline Type_t ** get_address_of_standardEventType_2() { return &___standardEventType_2; }
	inline void set_standardEventType_2(Type_t * value)
	{
		___standardEventType_2 = value;
		Il2CppCodeGenWriteBarrier((&___standardEventType_2), value);
	}

	inline static int32_t get_offset_of_m_Parameters_3() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Parameters_3)); }
	inline AnalyticsEventParamListContainer_t587083383 * get_m_Parameters_3() const { return ___m_Parameters_3; }
	inline AnalyticsEventParamListContainer_t587083383 ** get_address_of_m_Parameters_3() { return &___m_Parameters_3; }
	inline void set_m_Parameters_3(AnalyticsEventParamListContainer_t587083383 * value)
	{
		___m_Parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_3), value);
	}

	inline static int32_t get_offset_of_m_Name_5() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Name_5)); }
	inline String_t* get_m_Name_5() const { return ___m_Name_5; }
	inline String_t** get_address_of_m_Name_5() { return &___m_Name_5; }
	inline void set_m_Name_5(String_t* value)
	{
		___m_Name_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_5), value);
	}
};

struct StandardEventPayload_t1629891255_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.StandardEventPayload::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_4;

public:
	inline static int32_t get_offset_of_m_EventData_4() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255_StaticFields, ___m_EventData_4)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_4() const { return ___m_EventData_4; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_4() { return &___m_EventData_4; }
	inline void set_m_EventData_4(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDEVENTPAYLOAD_T1629891255_H
#ifndef TRACKABLEPROPERTY_T3943537984_H
#define TRACKABLEPROPERTY_T3943537984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty
struct  TrackableProperty_t3943537984  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget> UnityEngine.Analytics.TrackableProperty::m_Fields
	List_1_t235857739 * ___m_Fields_1;

public:
	inline static int32_t get_offset_of_m_Fields_1() { return static_cast<int32_t>(offsetof(TrackableProperty_t3943537984, ___m_Fields_1)); }
	inline List_1_t235857739 * get_m_Fields_1() const { return ___m_Fields_1; }
	inline List_1_t235857739 ** get_address_of_m_Fields_1() { return &___m_Fields_1; }
	inline void set_m_Fields_1(List_1_t235857739 * value)
	{
		___m_Fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTY_T3943537984_H
#ifndef FIELDWITHTARGET_T3058750293_H
#define FIELDWITHTARGET_T3058750293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty/FieldWithTarget
struct  FieldWithTarget_t3058750293  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_ParamName
	String_t* ___m_ParamName_0;
	// UnityEngine.Object UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_Target
	Object_t631007953 * ___m_Target_1;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_FieldPath
	String_t* ___m_FieldPath_2;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_TypeString
	String_t* ___m_TypeString_3;
	// System.Boolean UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_DoStatic
	bool ___m_DoStatic_4;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_StaticString
	String_t* ___m_StaticString_5;

public:
	inline static int32_t get_offset_of_m_ParamName_0() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_ParamName_0)); }
	inline String_t* get_m_ParamName_0() const { return ___m_ParamName_0; }
	inline String_t** get_address_of_m_ParamName_0() { return &___m_ParamName_0; }
	inline void set_m_ParamName_0(String_t* value)
	{
		___m_ParamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParamName_0), value);
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_Target_1)); }
	inline Object_t631007953 * get_m_Target_1() const { return ___m_Target_1; }
	inline Object_t631007953 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(Object_t631007953 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_1), value);
	}

	inline static int32_t get_offset_of_m_FieldPath_2() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_FieldPath_2)); }
	inline String_t* get_m_FieldPath_2() const { return ___m_FieldPath_2; }
	inline String_t** get_address_of_m_FieldPath_2() { return &___m_FieldPath_2; }
	inline void set_m_FieldPath_2(String_t* value)
	{
		___m_FieldPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FieldPath_2), value);
	}

	inline static int32_t get_offset_of_m_TypeString_3() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_TypeString_3)); }
	inline String_t* get_m_TypeString_3() const { return ___m_TypeString_3; }
	inline String_t** get_address_of_m_TypeString_3() { return &___m_TypeString_3; }
	inline void set_m_TypeString_3(String_t* value)
	{
		___m_TypeString_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeString_3), value);
	}

	inline static int32_t get_offset_of_m_DoStatic_4() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_DoStatic_4)); }
	inline bool get_m_DoStatic_4() const { return ___m_DoStatic_4; }
	inline bool* get_address_of_m_DoStatic_4() { return &___m_DoStatic_4; }
	inline void set_m_DoStatic_4(bool value)
	{
		___m_DoStatic_4 = value;
	}

	inline static int32_t get_offset_of_m_StaticString_5() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_StaticString_5)); }
	inline String_t* get_m_StaticString_5() const { return ___m_StaticString_5; }
	inline String_t** get_address_of_m_StaticString_5() { return &___m_StaticString_5; }
	inline void set_m_StaticString_5(String_t* value)
	{
		___m_StaticString_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StaticString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDWITHTARGET_T3058750293_H
#ifndef TRACKABLEPROPERTYBASE_T2121532948_H
#define TRACKABLEPROPERTYBASE_T2121532948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackablePropertyBase
struct  TrackablePropertyBase_t2121532948  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Analytics.TrackablePropertyBase::m_Target
	Object_t631007953 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackablePropertyBase::m_Path
	String_t* ___m_Path_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Target_0)); }
	inline Object_t631007953 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t631007953 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t631007953 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Path_1)); }
	inline String_t* get_m_Path_1() const { return ___m_Path_1; }
	inline String_t** get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(String_t* value)
	{
		___m_Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTYBASE_T2121532948_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef U24ARRAYTYPEU3D16_T3253128245_H
#define U24ARRAYTYPEU3D16_T3253128245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D16_t3253128245 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D16_t3253128245__padding[16];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T3253128245_H
#ifndef U24ARRAYTYPEU3D32_T3651253610_H
#define U24ARRAYTYPEU3D32_T3651253610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=32
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D32_t3651253610 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D32_t3651253610__padding[32];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D32_T3651253610_H
#ifndef U24ARRAYTYPEU3D48_T1336283963_H
#define U24ARRAYTYPEU3D48_T1336283963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=48
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D48_t1336283963 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D48_t1336283963__padding[48];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D48_T1336283963_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef TRACKABLEFIELD_T1772682203_H
#define TRACKABLEFIELD_T1772682203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableField
struct  TrackableField_t1772682203  : public TrackablePropertyBase_t2121532948
{
public:
	// System.String[] UnityEngine.Analytics.TrackableField::m_ValidTypeNames
	StringU5BU5D_t1281789340* ___m_ValidTypeNames_2;
	// System.String UnityEngine.Analytics.TrackableField::m_Type
	String_t* ___m_Type_3;
	// System.String UnityEngine.Analytics.TrackableField::m_EnumType
	String_t* ___m_EnumType_4;

public:
	inline static int32_t get_offset_of_m_ValidTypeNames_2() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_ValidTypeNames_2)); }
	inline StringU5BU5D_t1281789340* get_m_ValidTypeNames_2() const { return ___m_ValidTypeNames_2; }
	inline StringU5BU5D_t1281789340** get_address_of_m_ValidTypeNames_2() { return &___m_ValidTypeNames_2; }
	inline void set_m_ValidTypeNames_2(StringU5BU5D_t1281789340* value)
	{
		___m_ValidTypeNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidTypeNames_2), value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_Type_3)); }
	inline String_t* get_m_Type_3() const { return ___m_Type_3; }
	inline String_t** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(String_t* value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_3), value);
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEFIELD_T1772682203_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef UNITYEVENT_1_T2729110193_H
#define UNITYEVENT_1_T2729110193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t2729110193  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2729110193, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2729110193_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255368  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-8658990BAD6546E619D8A5C4F90BCF3F089E0953
	U24ArrayTypeU3D16_t3253128245  ___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_0;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-739C505E9F0985CE1E08892BC46BE5E839FF061A
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_1;
	// <PrivateImplementationDetails>/$ArrayType=48 <PrivateImplementationDetails>::$field-35FDBB6669F521B572D4AD71DD77E77F43C1B71B
	U24ArrayTypeU3D48_t1336283963  ___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_2;

public:
	inline static int32_t get_offset_of_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_0)); }
	inline U24ArrayTypeU3D16_t3253128245  get_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_0() const { return ___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_0; }
	inline U24ArrayTypeU3D16_t3253128245 * get_address_of_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_0() { return &___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_0; }
	inline void set_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_0(U24ArrayTypeU3D16_t3253128245  value)
	{
		___U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_1)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_1() const { return ___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_1; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_1() { return &___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_1; }
	inline void set_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_1(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_2)); }
	inline U24ArrayTypeU3D48_t1336283963  get_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_2() const { return ___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_2; }
	inline U24ArrayTypeU3D48_t1336283963 * get_address_of_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_2() { return &___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_2; }
	inline void set_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_2(U24ArrayTypeU3D48_t1336283963  value)
	{
		___U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifndef CONNECTIONPROTOCOL_T2586603950_H
#define CONNECTIONPROTOCOL_T2586603950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.ConnectionProtocol
struct  ConnectionProtocol_t2586603950 
{
public:
	// System.Byte ExitGames.Client.Photon.ConnectionProtocol::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConnectionProtocol_t2586603950, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONPROTOCOL_T2586603950_H
#ifndef DEBUGLEVEL_T3671880145_H
#define DEBUGLEVEL_T3671880145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.DebugLevel
struct  DebugLevel_t3671880145 
{
public:
	// System.Byte ExitGames.Client.Photon.DebugLevel::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DebugLevel_t3671880145, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLEVEL_T3671880145_H
#ifndef SERIALIZATIONPROTOCOL_T4091957412_H
#define SERIALIZATIONPROTOCOL_T4091957412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.SerializationProtocol
struct  SerializationProtocol_t4091957412 
{
public:
	// System.Int32 ExitGames.Client.Photon.SerializationProtocol::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SerializationProtocol_t4091957412, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONPROTOCOL_T4091957412_H
#ifndef CHATSTATE_T1558912819_H
#define CHATSTATE_T1558912819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Chat.ChatState
struct  ChatState_t1558912819 
{
public:
	// System.Int32 Photon.Chat.ChatState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ChatState_t1558912819, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATSTATE_T1558912819_H
#ifndef CUSTOMAUTHENTICATIONTYPE_T2653033121_H
#define CUSTOMAUTHENTICATIONTYPE_T2653033121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Chat.CustomAuthenticationType
struct  CustomAuthenticationType_t2653033121 
{
public:
	// System.Byte Photon.Chat.CustomAuthenticationType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CustomAuthenticationType_t2653033121, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMAUTHENTICATIONTYPE_T2653033121_H
#ifndef RPCTARGET_T1710769941_H
#define RPCTARGET_T1710769941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.RpcTarget
struct  RpcTarget_t1710769941 
{
public:
	// System.Int32 Photon.Pun.RpcTarget::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RpcTarget_t1710769941, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RPCTARGET_T1710769941_H
#ifndef ENODETYPE_T548973521_H
#define ENODETYPE_T548973521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.CellTreeNode/ENodeType
struct  ENodeType_t548973521 
{
public:
	// System.Int32 Photon.Pun.UtilityScripts.CellTreeNode/ENodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ENodeType_t548973521, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENODETYPE_T548973521_H
#ifndef INSTANTIATEOPTION_T2742092059_H
#define INSTANTIATEOPTION_T2742092059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.OnClickInstantiate/InstantiateOption
struct  InstantiateOption_t2742092059 
{
public:
	// System.Int32 Photon.Pun.UtilityScripts.OnClickInstantiate/InstantiateOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InstantiateOption_t2742092059, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTIATEOPTION_T2742092059_H
#ifndef U3CCLICKFLASHU3EC__ITERATOR0_T700316031_H
#define U3CCLICKFLASHU3EC__ITERATOR0_T700316031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.OnClickRpc/<ClickFlash>c__Iterator0
struct  U3CClickFlashU3Ec__Iterator0_t700316031  : public RuntimeObject
{
public:
	// System.Boolean Photon.Pun.UtilityScripts.OnClickRpc/<ClickFlash>c__Iterator0::<wasEmissive>__0
	bool ___U3CwasEmissiveU3E__0_0;
	// System.Single Photon.Pun.UtilityScripts.OnClickRpc/<ClickFlash>c__Iterator0::<f>__1
	float ___U3CfU3E__1_1;
	// UnityEngine.Color Photon.Pun.UtilityScripts.OnClickRpc/<ClickFlash>c__Iterator0::<lerped>__2
	Color_t2555686324  ___U3ClerpedU3E__2_2;
	// Photon.Pun.UtilityScripts.OnClickRpc Photon.Pun.UtilityScripts.OnClickRpc/<ClickFlash>c__Iterator0::$this
	OnClickRpc_t2528495255 * ___U24this_3;
	// System.Object Photon.Pun.UtilityScripts.OnClickRpc/<ClickFlash>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Photon.Pun.UtilityScripts.OnClickRpc/<ClickFlash>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Photon.Pun.UtilityScripts.OnClickRpc/<ClickFlash>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CwasEmissiveU3E__0_0() { return static_cast<int32_t>(offsetof(U3CClickFlashU3Ec__Iterator0_t700316031, ___U3CwasEmissiveU3E__0_0)); }
	inline bool get_U3CwasEmissiveU3E__0_0() const { return ___U3CwasEmissiveU3E__0_0; }
	inline bool* get_address_of_U3CwasEmissiveU3E__0_0() { return &___U3CwasEmissiveU3E__0_0; }
	inline void set_U3CwasEmissiveU3E__0_0(bool value)
	{
		___U3CwasEmissiveU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CfU3E__1_1() { return static_cast<int32_t>(offsetof(U3CClickFlashU3Ec__Iterator0_t700316031, ___U3CfU3E__1_1)); }
	inline float get_U3CfU3E__1_1() const { return ___U3CfU3E__1_1; }
	inline float* get_address_of_U3CfU3E__1_1() { return &___U3CfU3E__1_1; }
	inline void set_U3CfU3E__1_1(float value)
	{
		___U3CfU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3ClerpedU3E__2_2() { return static_cast<int32_t>(offsetof(U3CClickFlashU3Ec__Iterator0_t700316031, ___U3ClerpedU3E__2_2)); }
	inline Color_t2555686324  get_U3ClerpedU3E__2_2() const { return ___U3ClerpedU3E__2_2; }
	inline Color_t2555686324 * get_address_of_U3ClerpedU3E__2_2() { return &___U3ClerpedU3E__2_2; }
	inline void set_U3ClerpedU3E__2_2(Color_t2555686324  value)
	{
		___U3ClerpedU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CClickFlashU3Ec__Iterator0_t700316031, ___U24this_3)); }
	inline OnClickRpc_t2528495255 * get_U24this_3() const { return ___U24this_3; }
	inline OnClickRpc_t2528495255 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(OnClickRpc_t2528495255 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CClickFlashU3Ec__Iterator0_t700316031, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CClickFlashU3Ec__Iterator0_t700316031, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CClickFlashU3Ec__Iterator0_t700316031, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLICKFLASHU3EC__ITERATOR0_T700316031_H
#ifndef TEAM_T3101236044_H
#define TEAM_T3101236044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.PunTeams/Team
struct  Team_t3101236044 
{
public:
	// System.Byte Photon.Pun.UtilityScripts.PunTeams/Team::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Team_t3101236044, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEAM_T3101236044_H
#ifndef TABCHANGEEVENT_T3080003849_H
#define TABCHANGEEVENT_T3080003849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.TabViewManager/TabChangeEvent
struct  TabChangeEvent_t3080003849  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABCHANGEEVENT_T3080003849_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef REQUIREMENTTYPE_T3584265503_H
#define REQUIREMENTTYPE_T3584265503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam/RequirementType
struct  RequirementType_t3584265503 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventParam/RequirementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RequirementType_t3584265503, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREMENTTYPE_T3584265503_H
#ifndef TRIGGER_T4199345191_H
#define TRIGGER_T4199345191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker/Trigger
struct  Trigger_t4199345191 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t4199345191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T4199345191_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef PROPERTYTYPE_T4040930247_H
#define PROPERTYTYPE_T4040930247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty/PropertyType
struct  PropertyType_t4040930247 
{
public:
	// System.Int32 UnityEngine.Analytics.ValueProperty/PropertyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyType_t4040930247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPE_T4040930247_H
#ifndef INPUTBUTTON_T3704011348_H
#define INPUTBUTTON_T3704011348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData/InputButton
struct  InputButton_t3704011348 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/InputButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputButton_t3704011348, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBUTTON_T3704011348_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef PHOTONPEER_T1608153861_H
#define PHOTONPEER_T1608153861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitGames.Client.Photon.PhotonPeer
struct  PhotonPeer_t1608153861  : public RuntimeObject
{
public:
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::<CommandBufferSize>k__BackingField
	int32_t ___U3CCommandBufferSizeU3Ek__BackingField_0;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::<LimitOfUnreliableCommands>k__BackingField
	int32_t ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_1;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::WarningSize
	int32_t ___WarningSize_2;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ClientSdkId
	uint8_t ___ClientSdkId_6;
	// System.String ExitGames.Client.Photon.PhotonPeer::clientVersion
	String_t* ___clientVersion_7;
	// ExitGames.Client.Photon.SerializationProtocol ExitGames.Client.Photon.PhotonPeer::<SerializationProtocolType>k__BackingField
	int32_t ___U3CSerializationProtocolTypeU3Ek__BackingField_8;
	// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type> ExitGames.Client.Photon.PhotonPeer::SocketImplementationConfig
	Dictionary_2_t1253839074 * ___SocketImplementationConfig_9;
	// System.Type ExitGames.Client.Photon.PhotonPeer::<SocketImplementation>k__BackingField
	Type_t * ___U3CSocketImplementationU3Ek__BackingField_10;
	// ExitGames.Client.Photon.DebugLevel ExitGames.Client.Photon.PhotonPeer::DebugOut
	uint8_t ___DebugOut_11;
	// ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.PhotonPeer::<Listener>k__BackingField
	RuntimeObject* ___U3CListenerU3Ek__BackingField_12;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<EnableServerTracing>k__BackingField
	bool ___U3CEnableServerTracingU3Ek__BackingField_13;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::quickResendAttempts
	uint8_t ___quickResendAttempts_14;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMinConnections
	int32_t ___RhttpMinConnections_15;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::RhttpMaxConnections
	int32_t ___RhttpMaxConnections_16;
	// System.Byte ExitGames.Client.Photon.PhotonPeer::ChannelCount
	uint8_t ___ChannelCount_17;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::crcEnabled
	bool ___crcEnabled_18;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::SentCountAllowance
	int32_t ___SentCountAllowance_19;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::TimePingInterval
	int32_t ___TimePingInterval_20;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::DisconnectTimeout
	int32_t ___DisconnectTimeout_21;
	// ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PhotonPeer::<TransportProtocol>k__BackingField
	uint8_t ___U3CTransportProtocolU3Ek__BackingField_22;
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::mtu
	int32_t ___mtu_24;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::<IsSendingOnlyAcks>k__BackingField
	bool ___U3CIsSendingOnlyAcksU3Ek__BackingField_25;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::RandomizeSequenceNumbers
	bool ___RandomizeSequenceNumbers_27;
	// System.Byte[] ExitGames.Client.Photon.PhotonPeer::RandomizedSequenceNumbers
	ByteU5BU5D_t4116647657* ___RandomizedSequenceNumbers_28;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsIncoming>k__BackingField
	TrafficStats_t1302902347 * ___U3CTrafficStatsIncomingU3Ek__BackingField_29;
	// ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::<TrafficStatsOutgoing>k__BackingField
	TrafficStats_t1302902347 * ___U3CTrafficStatsOutgoingU3Ek__BackingField_30;
	// ExitGames.Client.Photon.TrafficStatsGameLevel ExitGames.Client.Photon.PhotonPeer::<TrafficStatsGameLevel>k__BackingField
	TrafficStatsGameLevel_t4013908777 * ___U3CTrafficStatsGameLevelU3Ek__BackingField_31;
	// System.Diagnostics.Stopwatch ExitGames.Client.Photon.PhotonPeer::trafficStatsStopwatch
	Stopwatch_t305734070 * ___trafficStatsStopwatch_32;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::trafficStatsEnabled
	bool ___trafficStatsEnabled_33;
	// ExitGames.Client.Photon.PeerBase ExitGames.Client.Photon.PhotonPeer::peerBase
	PeerBase_t2956237011 * ___peerBase_34;
	// System.Object ExitGames.Client.Photon.PhotonPeer::SendOutgoingLockObject
	RuntimeObject * ___SendOutgoingLockObject_35;
	// System.Object ExitGames.Client.Photon.PhotonPeer::DispatchLockObject
	RuntimeObject * ___DispatchLockObject_36;
	// System.Object ExitGames.Client.Photon.PhotonPeer::EnqueueLock
	RuntimeObject * ___EnqueueLock_37;
	// System.Byte[] ExitGames.Client.Photon.PhotonPeer::PayloadEncryptionSecret
	ByteU5BU5D_t4116647657* ___PayloadEncryptionSecret_38;
	// ExitGames.Client.Photon.EncryptorManaged.Encryptor ExitGames.Client.Photon.PhotonPeer::DgramEncryptor
	Encryptor_t200327285 * ___DgramEncryptor_39;
	// ExitGames.Client.Photon.EncryptorManaged.Decryptor ExitGames.Client.Photon.PhotonPeer::DgramDecryptor
	Decryptor_t2116099858 * ___DgramDecryptor_40;

public:
	inline static int32_t get_offset_of_U3CCommandBufferSizeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CCommandBufferSizeU3Ek__BackingField_0)); }
	inline int32_t get_U3CCommandBufferSizeU3Ek__BackingField_0() const { return ___U3CCommandBufferSizeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CCommandBufferSizeU3Ek__BackingField_0() { return &___U3CCommandBufferSizeU3Ek__BackingField_0; }
	inline void set_U3CCommandBufferSizeU3Ek__BackingField_0(int32_t value)
	{
		___U3CCommandBufferSizeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_1)); }
	inline int32_t get_U3CLimitOfUnreliableCommandsU3Ek__BackingField_1() const { return ___U3CLimitOfUnreliableCommandsU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_1() { return &___U3CLimitOfUnreliableCommandsU3Ek__BackingField_1; }
	inline void set_U3CLimitOfUnreliableCommandsU3Ek__BackingField_1(int32_t value)
	{
		___U3CLimitOfUnreliableCommandsU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_WarningSize_2() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___WarningSize_2)); }
	inline int32_t get_WarningSize_2() const { return ___WarningSize_2; }
	inline int32_t* get_address_of_WarningSize_2() { return &___WarningSize_2; }
	inline void set_WarningSize_2(int32_t value)
	{
		___WarningSize_2 = value;
	}

	inline static int32_t get_offset_of_ClientSdkId_6() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___ClientSdkId_6)); }
	inline uint8_t get_ClientSdkId_6() const { return ___ClientSdkId_6; }
	inline uint8_t* get_address_of_ClientSdkId_6() { return &___ClientSdkId_6; }
	inline void set_ClientSdkId_6(uint8_t value)
	{
		___ClientSdkId_6 = value;
	}

	inline static int32_t get_offset_of_clientVersion_7() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___clientVersion_7)); }
	inline String_t* get_clientVersion_7() const { return ___clientVersion_7; }
	inline String_t** get_address_of_clientVersion_7() { return &___clientVersion_7; }
	inline void set_clientVersion_7(String_t* value)
	{
		___clientVersion_7 = value;
		Il2CppCodeGenWriteBarrier((&___clientVersion_7), value);
	}

	inline static int32_t get_offset_of_U3CSerializationProtocolTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CSerializationProtocolTypeU3Ek__BackingField_8)); }
	inline int32_t get_U3CSerializationProtocolTypeU3Ek__BackingField_8() const { return ___U3CSerializationProtocolTypeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CSerializationProtocolTypeU3Ek__BackingField_8() { return &___U3CSerializationProtocolTypeU3Ek__BackingField_8; }
	inline void set_U3CSerializationProtocolTypeU3Ek__BackingField_8(int32_t value)
	{
		___U3CSerializationProtocolTypeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_SocketImplementationConfig_9() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SocketImplementationConfig_9)); }
	inline Dictionary_2_t1253839074 * get_SocketImplementationConfig_9() const { return ___SocketImplementationConfig_9; }
	inline Dictionary_2_t1253839074 ** get_address_of_SocketImplementationConfig_9() { return &___SocketImplementationConfig_9; }
	inline void set_SocketImplementationConfig_9(Dictionary_2_t1253839074 * value)
	{
		___SocketImplementationConfig_9 = value;
		Il2CppCodeGenWriteBarrier((&___SocketImplementationConfig_9), value);
	}

	inline static int32_t get_offset_of_U3CSocketImplementationU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CSocketImplementationU3Ek__BackingField_10)); }
	inline Type_t * get_U3CSocketImplementationU3Ek__BackingField_10() const { return ___U3CSocketImplementationU3Ek__BackingField_10; }
	inline Type_t ** get_address_of_U3CSocketImplementationU3Ek__BackingField_10() { return &___U3CSocketImplementationU3Ek__BackingField_10; }
	inline void set_U3CSocketImplementationU3Ek__BackingField_10(Type_t * value)
	{
		___U3CSocketImplementationU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSocketImplementationU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_DebugOut_11() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DebugOut_11)); }
	inline uint8_t get_DebugOut_11() const { return ___DebugOut_11; }
	inline uint8_t* get_address_of_DebugOut_11() { return &___DebugOut_11; }
	inline void set_DebugOut_11(uint8_t value)
	{
		___DebugOut_11 = value;
	}

	inline static int32_t get_offset_of_U3CListenerU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CListenerU3Ek__BackingField_12)); }
	inline RuntimeObject* get_U3CListenerU3Ek__BackingField_12() const { return ___U3CListenerU3Ek__BackingField_12; }
	inline RuntimeObject** get_address_of_U3CListenerU3Ek__BackingField_12() { return &___U3CListenerU3Ek__BackingField_12; }
	inline void set_U3CListenerU3Ek__BackingField_12(RuntimeObject* value)
	{
		___U3CListenerU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CListenerU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CEnableServerTracingU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CEnableServerTracingU3Ek__BackingField_13)); }
	inline bool get_U3CEnableServerTracingU3Ek__BackingField_13() const { return ___U3CEnableServerTracingU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CEnableServerTracingU3Ek__BackingField_13() { return &___U3CEnableServerTracingU3Ek__BackingField_13; }
	inline void set_U3CEnableServerTracingU3Ek__BackingField_13(bool value)
	{
		___U3CEnableServerTracingU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_quickResendAttempts_14() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___quickResendAttempts_14)); }
	inline uint8_t get_quickResendAttempts_14() const { return ___quickResendAttempts_14; }
	inline uint8_t* get_address_of_quickResendAttempts_14() { return &___quickResendAttempts_14; }
	inline void set_quickResendAttempts_14(uint8_t value)
	{
		___quickResendAttempts_14 = value;
	}

	inline static int32_t get_offset_of_RhttpMinConnections_15() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RhttpMinConnections_15)); }
	inline int32_t get_RhttpMinConnections_15() const { return ___RhttpMinConnections_15; }
	inline int32_t* get_address_of_RhttpMinConnections_15() { return &___RhttpMinConnections_15; }
	inline void set_RhttpMinConnections_15(int32_t value)
	{
		___RhttpMinConnections_15 = value;
	}

	inline static int32_t get_offset_of_RhttpMaxConnections_16() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RhttpMaxConnections_16)); }
	inline int32_t get_RhttpMaxConnections_16() const { return ___RhttpMaxConnections_16; }
	inline int32_t* get_address_of_RhttpMaxConnections_16() { return &___RhttpMaxConnections_16; }
	inline void set_RhttpMaxConnections_16(int32_t value)
	{
		___RhttpMaxConnections_16 = value;
	}

	inline static int32_t get_offset_of_ChannelCount_17() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___ChannelCount_17)); }
	inline uint8_t get_ChannelCount_17() const { return ___ChannelCount_17; }
	inline uint8_t* get_address_of_ChannelCount_17() { return &___ChannelCount_17; }
	inline void set_ChannelCount_17(uint8_t value)
	{
		___ChannelCount_17 = value;
	}

	inline static int32_t get_offset_of_crcEnabled_18() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___crcEnabled_18)); }
	inline bool get_crcEnabled_18() const { return ___crcEnabled_18; }
	inline bool* get_address_of_crcEnabled_18() { return &___crcEnabled_18; }
	inline void set_crcEnabled_18(bool value)
	{
		___crcEnabled_18 = value;
	}

	inline static int32_t get_offset_of_SentCountAllowance_19() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SentCountAllowance_19)); }
	inline int32_t get_SentCountAllowance_19() const { return ___SentCountAllowance_19; }
	inline int32_t* get_address_of_SentCountAllowance_19() { return &___SentCountAllowance_19; }
	inline void set_SentCountAllowance_19(int32_t value)
	{
		___SentCountAllowance_19 = value;
	}

	inline static int32_t get_offset_of_TimePingInterval_20() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___TimePingInterval_20)); }
	inline int32_t get_TimePingInterval_20() const { return ___TimePingInterval_20; }
	inline int32_t* get_address_of_TimePingInterval_20() { return &___TimePingInterval_20; }
	inline void set_TimePingInterval_20(int32_t value)
	{
		___TimePingInterval_20 = value;
	}

	inline static int32_t get_offset_of_DisconnectTimeout_21() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DisconnectTimeout_21)); }
	inline int32_t get_DisconnectTimeout_21() const { return ___DisconnectTimeout_21; }
	inline int32_t* get_address_of_DisconnectTimeout_21() { return &___DisconnectTimeout_21; }
	inline void set_DisconnectTimeout_21(int32_t value)
	{
		___DisconnectTimeout_21 = value;
	}

	inline static int32_t get_offset_of_U3CTransportProtocolU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTransportProtocolU3Ek__BackingField_22)); }
	inline uint8_t get_U3CTransportProtocolU3Ek__BackingField_22() const { return ___U3CTransportProtocolU3Ek__BackingField_22; }
	inline uint8_t* get_address_of_U3CTransportProtocolU3Ek__BackingField_22() { return &___U3CTransportProtocolU3Ek__BackingField_22; }
	inline void set_U3CTransportProtocolU3Ek__BackingField_22(uint8_t value)
	{
		___U3CTransportProtocolU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_mtu_24() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___mtu_24)); }
	inline int32_t get_mtu_24() const { return ___mtu_24; }
	inline int32_t* get_address_of_mtu_24() { return &___mtu_24; }
	inline void set_mtu_24(int32_t value)
	{
		___mtu_24 = value;
	}

	inline static int32_t get_offset_of_U3CIsSendingOnlyAcksU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CIsSendingOnlyAcksU3Ek__BackingField_25)); }
	inline bool get_U3CIsSendingOnlyAcksU3Ek__BackingField_25() const { return ___U3CIsSendingOnlyAcksU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CIsSendingOnlyAcksU3Ek__BackingField_25() { return &___U3CIsSendingOnlyAcksU3Ek__BackingField_25; }
	inline void set_U3CIsSendingOnlyAcksU3Ek__BackingField_25(bool value)
	{
		___U3CIsSendingOnlyAcksU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_RandomizeSequenceNumbers_27() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RandomizeSequenceNumbers_27)); }
	inline bool get_RandomizeSequenceNumbers_27() const { return ___RandomizeSequenceNumbers_27; }
	inline bool* get_address_of_RandomizeSequenceNumbers_27() { return &___RandomizeSequenceNumbers_27; }
	inline void set_RandomizeSequenceNumbers_27(bool value)
	{
		___RandomizeSequenceNumbers_27 = value;
	}

	inline static int32_t get_offset_of_RandomizedSequenceNumbers_28() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___RandomizedSequenceNumbers_28)); }
	inline ByteU5BU5D_t4116647657* get_RandomizedSequenceNumbers_28() const { return ___RandomizedSequenceNumbers_28; }
	inline ByteU5BU5D_t4116647657** get_address_of_RandomizedSequenceNumbers_28() { return &___RandomizedSequenceNumbers_28; }
	inline void set_RandomizedSequenceNumbers_28(ByteU5BU5D_t4116647657* value)
	{
		___RandomizedSequenceNumbers_28 = value;
		Il2CppCodeGenWriteBarrier((&___RandomizedSequenceNumbers_28), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsIncomingU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsIncomingU3Ek__BackingField_29)); }
	inline TrafficStats_t1302902347 * get_U3CTrafficStatsIncomingU3Ek__BackingField_29() const { return ___U3CTrafficStatsIncomingU3Ek__BackingField_29; }
	inline TrafficStats_t1302902347 ** get_address_of_U3CTrafficStatsIncomingU3Ek__BackingField_29() { return &___U3CTrafficStatsIncomingU3Ek__BackingField_29; }
	inline void set_U3CTrafficStatsIncomingU3Ek__BackingField_29(TrafficStats_t1302902347 * value)
	{
		___U3CTrafficStatsIncomingU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsIncomingU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsOutgoingU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsOutgoingU3Ek__BackingField_30)); }
	inline TrafficStats_t1302902347 * get_U3CTrafficStatsOutgoingU3Ek__BackingField_30() const { return ___U3CTrafficStatsOutgoingU3Ek__BackingField_30; }
	inline TrafficStats_t1302902347 ** get_address_of_U3CTrafficStatsOutgoingU3Ek__BackingField_30() { return &___U3CTrafficStatsOutgoingU3Ek__BackingField_30; }
	inline void set_U3CTrafficStatsOutgoingU3Ek__BackingField_30(TrafficStats_t1302902347 * value)
	{
		___U3CTrafficStatsOutgoingU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsOutgoingU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CTrafficStatsGameLevelU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___U3CTrafficStatsGameLevelU3Ek__BackingField_31)); }
	inline TrafficStatsGameLevel_t4013908777 * get_U3CTrafficStatsGameLevelU3Ek__BackingField_31() const { return ___U3CTrafficStatsGameLevelU3Ek__BackingField_31; }
	inline TrafficStatsGameLevel_t4013908777 ** get_address_of_U3CTrafficStatsGameLevelU3Ek__BackingField_31() { return &___U3CTrafficStatsGameLevelU3Ek__BackingField_31; }
	inline void set_U3CTrafficStatsGameLevelU3Ek__BackingField_31(TrafficStatsGameLevel_t4013908777 * value)
	{
		___U3CTrafficStatsGameLevelU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrafficStatsGameLevelU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_trafficStatsStopwatch_32() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___trafficStatsStopwatch_32)); }
	inline Stopwatch_t305734070 * get_trafficStatsStopwatch_32() const { return ___trafficStatsStopwatch_32; }
	inline Stopwatch_t305734070 ** get_address_of_trafficStatsStopwatch_32() { return &___trafficStatsStopwatch_32; }
	inline void set_trafficStatsStopwatch_32(Stopwatch_t305734070 * value)
	{
		___trafficStatsStopwatch_32 = value;
		Il2CppCodeGenWriteBarrier((&___trafficStatsStopwatch_32), value);
	}

	inline static int32_t get_offset_of_trafficStatsEnabled_33() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___trafficStatsEnabled_33)); }
	inline bool get_trafficStatsEnabled_33() const { return ___trafficStatsEnabled_33; }
	inline bool* get_address_of_trafficStatsEnabled_33() { return &___trafficStatsEnabled_33; }
	inline void set_trafficStatsEnabled_33(bool value)
	{
		___trafficStatsEnabled_33 = value;
	}

	inline static int32_t get_offset_of_peerBase_34() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___peerBase_34)); }
	inline PeerBase_t2956237011 * get_peerBase_34() const { return ___peerBase_34; }
	inline PeerBase_t2956237011 ** get_address_of_peerBase_34() { return &___peerBase_34; }
	inline void set_peerBase_34(PeerBase_t2956237011 * value)
	{
		___peerBase_34 = value;
		Il2CppCodeGenWriteBarrier((&___peerBase_34), value);
	}

	inline static int32_t get_offset_of_SendOutgoingLockObject_35() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___SendOutgoingLockObject_35)); }
	inline RuntimeObject * get_SendOutgoingLockObject_35() const { return ___SendOutgoingLockObject_35; }
	inline RuntimeObject ** get_address_of_SendOutgoingLockObject_35() { return &___SendOutgoingLockObject_35; }
	inline void set_SendOutgoingLockObject_35(RuntimeObject * value)
	{
		___SendOutgoingLockObject_35 = value;
		Il2CppCodeGenWriteBarrier((&___SendOutgoingLockObject_35), value);
	}

	inline static int32_t get_offset_of_DispatchLockObject_36() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DispatchLockObject_36)); }
	inline RuntimeObject * get_DispatchLockObject_36() const { return ___DispatchLockObject_36; }
	inline RuntimeObject ** get_address_of_DispatchLockObject_36() { return &___DispatchLockObject_36; }
	inline void set_DispatchLockObject_36(RuntimeObject * value)
	{
		___DispatchLockObject_36 = value;
		Il2CppCodeGenWriteBarrier((&___DispatchLockObject_36), value);
	}

	inline static int32_t get_offset_of_EnqueueLock_37() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___EnqueueLock_37)); }
	inline RuntimeObject * get_EnqueueLock_37() const { return ___EnqueueLock_37; }
	inline RuntimeObject ** get_address_of_EnqueueLock_37() { return &___EnqueueLock_37; }
	inline void set_EnqueueLock_37(RuntimeObject * value)
	{
		___EnqueueLock_37 = value;
		Il2CppCodeGenWriteBarrier((&___EnqueueLock_37), value);
	}

	inline static int32_t get_offset_of_PayloadEncryptionSecret_38() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___PayloadEncryptionSecret_38)); }
	inline ByteU5BU5D_t4116647657* get_PayloadEncryptionSecret_38() const { return ___PayloadEncryptionSecret_38; }
	inline ByteU5BU5D_t4116647657** get_address_of_PayloadEncryptionSecret_38() { return &___PayloadEncryptionSecret_38; }
	inline void set_PayloadEncryptionSecret_38(ByteU5BU5D_t4116647657* value)
	{
		___PayloadEncryptionSecret_38 = value;
		Il2CppCodeGenWriteBarrier((&___PayloadEncryptionSecret_38), value);
	}

	inline static int32_t get_offset_of_DgramEncryptor_39() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DgramEncryptor_39)); }
	inline Encryptor_t200327285 * get_DgramEncryptor_39() const { return ___DgramEncryptor_39; }
	inline Encryptor_t200327285 ** get_address_of_DgramEncryptor_39() { return &___DgramEncryptor_39; }
	inline void set_DgramEncryptor_39(Encryptor_t200327285 * value)
	{
		___DgramEncryptor_39 = value;
		Il2CppCodeGenWriteBarrier((&___DgramEncryptor_39), value);
	}

	inline static int32_t get_offset_of_DgramDecryptor_40() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861, ___DgramDecryptor_40)); }
	inline Decryptor_t2116099858 * get_DgramDecryptor_40() const { return ___DgramDecryptor_40; }
	inline Decryptor_t2116099858 ** get_address_of_DgramDecryptor_40() { return &___DgramDecryptor_40; }
	inline void set_DgramDecryptor_40(Decryptor_t2116099858 * value)
	{
		___DgramDecryptor_40 = value;
		Il2CppCodeGenWriteBarrier((&___DgramDecryptor_40), value);
	}
};

struct PhotonPeer_t1608153861_StaticFields
{
public:
	// System.Int32 ExitGames.Client.Photon.PhotonPeer::OutgoingStreamBufferSize
	int32_t ___OutgoingStreamBufferSize_23;
	// System.Boolean ExitGames.Client.Photon.PhotonPeer::AsyncKeyExchange
	bool ___AsyncKeyExchange_26;

public:
	inline static int32_t get_offset_of_OutgoingStreamBufferSize_23() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861_StaticFields, ___OutgoingStreamBufferSize_23)); }
	inline int32_t get_OutgoingStreamBufferSize_23() const { return ___OutgoingStreamBufferSize_23; }
	inline int32_t* get_address_of_OutgoingStreamBufferSize_23() { return &___OutgoingStreamBufferSize_23; }
	inline void set_OutgoingStreamBufferSize_23(int32_t value)
	{
		___OutgoingStreamBufferSize_23 = value;
	}

	inline static int32_t get_offset_of_AsyncKeyExchange_26() { return static_cast<int32_t>(offsetof(PhotonPeer_t1608153861_StaticFields, ___AsyncKeyExchange_26)); }
	inline bool get_AsyncKeyExchange_26() const { return ___AsyncKeyExchange_26; }
	inline bool* get_address_of_AsyncKeyExchange_26() { return &___AsyncKeyExchange_26; }
	inline void set_AsyncKeyExchange_26(bool value)
	{
		___AsyncKeyExchange_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONPEER_T1608153861_H
#ifndef AUTHENTICATIONVALUES_T563695058_H
#define AUTHENTICATIONVALUES_T563695058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Chat.AuthenticationValues
struct  AuthenticationValues_t563695058  : public RuntimeObject
{
public:
	// Photon.Chat.CustomAuthenticationType Photon.Chat.AuthenticationValues::authType
	uint8_t ___authType_0;
	// System.String Photon.Chat.AuthenticationValues::<AuthGetParameters>k__BackingField
	String_t* ___U3CAuthGetParametersU3Ek__BackingField_1;
	// System.Object Photon.Chat.AuthenticationValues::<AuthPostData>k__BackingField
	RuntimeObject * ___U3CAuthPostDataU3Ek__BackingField_2;
	// System.String Photon.Chat.AuthenticationValues::<Token>k__BackingField
	String_t* ___U3CTokenU3Ek__BackingField_3;
	// System.String Photon.Chat.AuthenticationValues::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_authType_0() { return static_cast<int32_t>(offsetof(AuthenticationValues_t563695058, ___authType_0)); }
	inline uint8_t get_authType_0() const { return ___authType_0; }
	inline uint8_t* get_address_of_authType_0() { return &___authType_0; }
	inline void set_authType_0(uint8_t value)
	{
		___authType_0 = value;
	}

	inline static int32_t get_offset_of_U3CAuthGetParametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AuthenticationValues_t563695058, ___U3CAuthGetParametersU3Ek__BackingField_1)); }
	inline String_t* get_U3CAuthGetParametersU3Ek__BackingField_1() const { return ___U3CAuthGetParametersU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CAuthGetParametersU3Ek__BackingField_1() { return &___U3CAuthGetParametersU3Ek__BackingField_1; }
	inline void set_U3CAuthGetParametersU3Ek__BackingField_1(String_t* value)
	{
		___U3CAuthGetParametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthGetParametersU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CAuthPostDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AuthenticationValues_t563695058, ___U3CAuthPostDataU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3CAuthPostDataU3Ek__BackingField_2() const { return ___U3CAuthPostDataU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3CAuthPostDataU3Ek__BackingField_2() { return &___U3CAuthPostDataU3Ek__BackingField_2; }
	inline void set_U3CAuthPostDataU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3CAuthPostDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthPostDataU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CTokenU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AuthenticationValues_t563695058, ___U3CTokenU3Ek__BackingField_3)); }
	inline String_t* get_U3CTokenU3Ek__BackingField_3() const { return ___U3CTokenU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CTokenU3Ek__BackingField_3() { return &___U3CTokenU3Ek__BackingField_3; }
	inline void set_U3CTokenU3Ek__BackingField_3(String_t* value)
	{
		___U3CTokenU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTokenU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AuthenticationValues_t563695058, ___U3CUserIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_4() const { return ___U3CUserIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_4() { return &___U3CUserIdU3Ek__BackingField_4; }
	inline void set_U3CUserIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserIdU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONVALUES_T563695058_H
#ifndef CELLTREENODE_T3709723763_H
#define CELLTREENODE_T3709723763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.CellTreeNode
struct  CellTreeNode_t3709723763  : public RuntimeObject
{
public:
	// System.Byte Photon.Pun.UtilityScripts.CellTreeNode::Id
	uint8_t ___Id_0;
	// UnityEngine.Vector3 Photon.Pun.UtilityScripts.CellTreeNode::Center
	Vector3_t3722313464  ___Center_1;
	// UnityEngine.Vector3 Photon.Pun.UtilityScripts.CellTreeNode::Size
	Vector3_t3722313464  ___Size_2;
	// UnityEngine.Vector3 Photon.Pun.UtilityScripts.CellTreeNode::TopLeft
	Vector3_t3722313464  ___TopLeft_3;
	// UnityEngine.Vector3 Photon.Pun.UtilityScripts.CellTreeNode::BottomRight
	Vector3_t3722313464  ___BottomRight_4;
	// Photon.Pun.UtilityScripts.CellTreeNode/ENodeType Photon.Pun.UtilityScripts.CellTreeNode::NodeType
	int32_t ___NodeType_5;
	// Photon.Pun.UtilityScripts.CellTreeNode Photon.Pun.UtilityScripts.CellTreeNode::Parent
	CellTreeNode_t3709723763 * ___Parent_6;
	// System.Collections.Generic.List`1<Photon.Pun.UtilityScripts.CellTreeNode> Photon.Pun.UtilityScripts.CellTreeNode::Childs
	List_1_t886831209 * ___Childs_7;
	// System.Single Photon.Pun.UtilityScripts.CellTreeNode::maxDistance
	float ___maxDistance_8;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(CellTreeNode_t3709723763, ___Id_0)); }
	inline uint8_t get_Id_0() const { return ___Id_0; }
	inline uint8_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(uint8_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Center_1() { return static_cast<int32_t>(offsetof(CellTreeNode_t3709723763, ___Center_1)); }
	inline Vector3_t3722313464  get_Center_1() const { return ___Center_1; }
	inline Vector3_t3722313464 * get_address_of_Center_1() { return &___Center_1; }
	inline void set_Center_1(Vector3_t3722313464  value)
	{
		___Center_1 = value;
	}

	inline static int32_t get_offset_of_Size_2() { return static_cast<int32_t>(offsetof(CellTreeNode_t3709723763, ___Size_2)); }
	inline Vector3_t3722313464  get_Size_2() const { return ___Size_2; }
	inline Vector3_t3722313464 * get_address_of_Size_2() { return &___Size_2; }
	inline void set_Size_2(Vector3_t3722313464  value)
	{
		___Size_2 = value;
	}

	inline static int32_t get_offset_of_TopLeft_3() { return static_cast<int32_t>(offsetof(CellTreeNode_t3709723763, ___TopLeft_3)); }
	inline Vector3_t3722313464  get_TopLeft_3() const { return ___TopLeft_3; }
	inline Vector3_t3722313464 * get_address_of_TopLeft_3() { return &___TopLeft_3; }
	inline void set_TopLeft_3(Vector3_t3722313464  value)
	{
		___TopLeft_3 = value;
	}

	inline static int32_t get_offset_of_BottomRight_4() { return static_cast<int32_t>(offsetof(CellTreeNode_t3709723763, ___BottomRight_4)); }
	inline Vector3_t3722313464  get_BottomRight_4() const { return ___BottomRight_4; }
	inline Vector3_t3722313464 * get_address_of_BottomRight_4() { return &___BottomRight_4; }
	inline void set_BottomRight_4(Vector3_t3722313464  value)
	{
		___BottomRight_4 = value;
	}

	inline static int32_t get_offset_of_NodeType_5() { return static_cast<int32_t>(offsetof(CellTreeNode_t3709723763, ___NodeType_5)); }
	inline int32_t get_NodeType_5() const { return ___NodeType_5; }
	inline int32_t* get_address_of_NodeType_5() { return &___NodeType_5; }
	inline void set_NodeType_5(int32_t value)
	{
		___NodeType_5 = value;
	}

	inline static int32_t get_offset_of_Parent_6() { return static_cast<int32_t>(offsetof(CellTreeNode_t3709723763, ___Parent_6)); }
	inline CellTreeNode_t3709723763 * get_Parent_6() const { return ___Parent_6; }
	inline CellTreeNode_t3709723763 ** get_address_of_Parent_6() { return &___Parent_6; }
	inline void set_Parent_6(CellTreeNode_t3709723763 * value)
	{
		___Parent_6 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_6), value);
	}

	inline static int32_t get_offset_of_Childs_7() { return static_cast<int32_t>(offsetof(CellTreeNode_t3709723763, ___Childs_7)); }
	inline List_1_t886831209 * get_Childs_7() const { return ___Childs_7; }
	inline List_1_t886831209 ** get_address_of_Childs_7() { return &___Childs_7; }
	inline void set_Childs_7(List_1_t886831209 * value)
	{
		___Childs_7 = value;
		Il2CppCodeGenWriteBarrier((&___Childs_7), value);
	}

	inline static int32_t get_offset_of_maxDistance_8() { return static_cast<int32_t>(offsetof(CellTreeNode_t3709723763, ___maxDistance_8)); }
	inline float get_maxDistance_8() const { return ___maxDistance_8; }
	inline float* get_address_of_maxDistance_8() { return &___maxDistance_8; }
	inline void set_maxDistance_8(float value)
	{
		___maxDistance_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CELLTREENODE_T3709723763_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef ANALYTICSEVENTPARAM_T2480121928_H
#define ANALYTICSEVENTPARAM_T2480121928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam
struct  AnalyticsEventParam_t2480121928  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventParam/RequirementType UnityEngine.Analytics.AnalyticsEventParam::m_RequirementType
	int32_t ___m_RequirementType_0;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_GroupID
	String_t* ___m_GroupID_1;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Tooltip
	String_t* ___m_Tooltip_2;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Name
	String_t* ___m_Name_3;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.AnalyticsEventParam::m_Value
	ValueProperty_t1868393739 * ___m_Value_4;

public:
	inline static int32_t get_offset_of_m_RequirementType_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_RequirementType_0)); }
	inline int32_t get_m_RequirementType_0() const { return ___m_RequirementType_0; }
	inline int32_t* get_address_of_m_RequirementType_0() { return &___m_RequirementType_0; }
	inline void set_m_RequirementType_0(int32_t value)
	{
		___m_RequirementType_0 = value;
	}

	inline static int32_t get_offset_of_m_GroupID_1() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_GroupID_1)); }
	inline String_t* get_m_GroupID_1() const { return ___m_GroupID_1; }
	inline String_t** get_address_of_m_GroupID_1() { return &___m_GroupID_1; }
	inline void set_m_GroupID_1(String_t* value)
	{
		___m_GroupID_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_GroupID_1), value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tooltip_2), value);
	}

	inline static int32_t get_offset_of_m_Name_3() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Name_3)); }
	inline String_t* get_m_Name_3() const { return ___m_Name_3; }
	inline String_t** get_address_of_m_Name_3() { return &___m_Name_3; }
	inline void set_m_Name_3(String_t* value)
	{
		___m_Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_3), value);
	}

	inline static int32_t get_offset_of_m_Value_4() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Value_4)); }
	inline ValueProperty_t1868393739 * get_m_Value_4() const { return ___m_Value_4; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_4() { return &___m_Value_4; }
	inline void set_m_Value_4(ValueProperty_t1868393739 * value)
	{
		___m_Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAM_T2480121928_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef VALUEPROPERTY_T1868393739_H
#define VALUEPROPERTY_T1868393739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty
struct  ValueProperty_t1868393739  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EditingCustomValue
	bool ___m_EditingCustomValue_0;
	// System.Int32 UnityEngine.Analytics.ValueProperty::m_PopupIndex
	int32_t ___m_PopupIndex_1;
	// System.String UnityEngine.Analytics.ValueProperty::m_CustomValue
	String_t* ___m_CustomValue_2;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_FixedType
	bool ___m_FixedType_3;
	// System.String UnityEngine.Analytics.ValueProperty::m_EnumType
	String_t* ___m_EnumType_4;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EnumTypeIsCustomizable
	bool ___m_EnumTypeIsCustomizable_5;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_CanDisable
	bool ___m_CanDisable_6;
	// UnityEngine.Analytics.ValueProperty/PropertyType UnityEngine.Analytics.ValueProperty::m_PropertyType
	int32_t ___m_PropertyType_7;
	// System.String UnityEngine.Analytics.ValueProperty::m_ValueType
	String_t* ___m_ValueType_8;
	// System.String UnityEngine.Analytics.ValueProperty::m_Value
	String_t* ___m_Value_9;
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.ValueProperty::m_Target
	TrackableField_t1772682203 * ___m_Target_10;

public:
	inline static int32_t get_offset_of_m_EditingCustomValue_0() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EditingCustomValue_0)); }
	inline bool get_m_EditingCustomValue_0() const { return ___m_EditingCustomValue_0; }
	inline bool* get_address_of_m_EditingCustomValue_0() { return &___m_EditingCustomValue_0; }
	inline void set_m_EditingCustomValue_0(bool value)
	{
		___m_EditingCustomValue_0 = value;
	}

	inline static int32_t get_offset_of_m_PopupIndex_1() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PopupIndex_1)); }
	inline int32_t get_m_PopupIndex_1() const { return ___m_PopupIndex_1; }
	inline int32_t* get_address_of_m_PopupIndex_1() { return &___m_PopupIndex_1; }
	inline void set_m_PopupIndex_1(int32_t value)
	{
		___m_PopupIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_CustomValue_2() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CustomValue_2)); }
	inline String_t* get_m_CustomValue_2() const { return ___m_CustomValue_2; }
	inline String_t** get_address_of_m_CustomValue_2() { return &___m_CustomValue_2; }
	inline void set_m_CustomValue_2(String_t* value)
	{
		___m_CustomValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomValue_2), value);
	}

	inline static int32_t get_offset_of_m_FixedType_3() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_FixedType_3)); }
	inline bool get_m_FixedType_3() const { return ___m_FixedType_3; }
	inline bool* get_address_of_m_FixedType_3() { return &___m_FixedType_3; }
	inline void set_m_FixedType_3(bool value)
	{
		___m_FixedType_3 = value;
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}

	inline static int32_t get_offset_of_m_EnumTypeIsCustomizable_5() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumTypeIsCustomizable_5)); }
	inline bool get_m_EnumTypeIsCustomizable_5() const { return ___m_EnumTypeIsCustomizable_5; }
	inline bool* get_address_of_m_EnumTypeIsCustomizable_5() { return &___m_EnumTypeIsCustomizable_5; }
	inline void set_m_EnumTypeIsCustomizable_5(bool value)
	{
		___m_EnumTypeIsCustomizable_5 = value;
	}

	inline static int32_t get_offset_of_m_CanDisable_6() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CanDisable_6)); }
	inline bool get_m_CanDisable_6() const { return ___m_CanDisable_6; }
	inline bool* get_address_of_m_CanDisable_6() { return &___m_CanDisable_6; }
	inline void set_m_CanDisable_6(bool value)
	{
		___m_CanDisable_6 = value;
	}

	inline static int32_t get_offset_of_m_PropertyType_7() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PropertyType_7)); }
	inline int32_t get_m_PropertyType_7() const { return ___m_PropertyType_7; }
	inline int32_t* get_address_of_m_PropertyType_7() { return &___m_PropertyType_7; }
	inline void set_m_PropertyType_7(int32_t value)
	{
		___m_PropertyType_7 = value;
	}

	inline static int32_t get_offset_of_m_ValueType_8() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_ValueType_8)); }
	inline String_t* get_m_ValueType_8() const { return ___m_ValueType_8; }
	inline String_t** get_address_of_m_ValueType_8() { return &___m_ValueType_8; }
	inline void set_m_ValueType_8(String_t* value)
	{
		___m_ValueType_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValueType_8), value);
	}

	inline static int32_t get_offset_of_m_Value_9() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Value_9)); }
	inline String_t* get_m_Value_9() const { return ___m_Value_9; }
	inline String_t** get_address_of_m_Value_9() { return &___m_Value_9; }
	inline void set_m_Value_9(String_t* value)
	{
		___m_Value_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_9), value);
	}

	inline static int32_t get_offset_of_m_Target_10() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Target_10)); }
	inline TrackableField_t1772682203 * get_m_Target_10() const { return ___m_Target_10; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_10() { return &___m_Target_10; }
	inline void set_m_Target_10(TrackableField_t1772682203 * value)
	{
		___m_Target_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEPROPERTY_T1868393739_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef CHATPEER_T3366739368_H
#define CHATPEER_T3366739368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Chat.ChatPeer
struct  ChatPeer_t3366739368  : public PhotonPeer_t1608153861
{
public:

public:
};

struct ChatPeer_t3366739368_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32> Photon.Chat.ChatPeer::ProtocolToNameServerPort
	Dictionary_2_t1720840067 * ___ProtocolToNameServerPort_43;

public:
	inline static int32_t get_offset_of_ProtocolToNameServerPort_43() { return static_cast<int32_t>(offsetof(ChatPeer_t3366739368_StaticFields, ___ProtocolToNameServerPort_43)); }
	inline Dictionary_2_t1720840067 * get_ProtocolToNameServerPort_43() const { return ___ProtocolToNameServerPort_43; }
	inline Dictionary_2_t1720840067 ** get_address_of_ProtocolToNameServerPort_43() { return &___ProtocolToNameServerPort_43; }
	inline void set_ProtocolToNameServerPort_43(Dictionary_2_t1720840067 * value)
	{
		___ProtocolToNameServerPort_43 = value;
		Il2CppCodeGenWriteBarrier((&___ProtocolToNameServerPort_43), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATPEER_T3366739368_H
#ifndef COUNTDOWNTIMERHASEXPIRED_T4234756006_H
#define COUNTDOWNTIMERHASEXPIRED_T4234756006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.CountdownTimer/CountdownTimerHasExpired
struct  CountdownTimerHasExpired_t4234756006  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTDOWNTIMERHASEXPIRED_T4234756006_H
#ifndef PLAYERNUMBERINGCHANGED_T966975091_H
#define PLAYERNUMBERINGCHANGED_T966975091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.PlayerNumbering/PlayerNumberingChanged
struct  PlayerNumberingChanged_t966975091  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERNUMBERINGCHANGED_T966975091_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CHANNELSELECTOR_T3480308131_H
#define CHANNELSELECTOR_T3480308131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChannelSelector
struct  ChannelSelector_t3480308131  : public MonoBehaviour_t3962482529
{
public:
	// System.String ChannelSelector::Channel
	String_t* ___Channel_4;

public:
	inline static int32_t get_offset_of_Channel_4() { return static_cast<int32_t>(offsetof(ChannelSelector_t3480308131, ___Channel_4)); }
	inline String_t* get_Channel_4() const { return ___Channel_4; }
	inline String_t** get_address_of_Channel_4() { return &___Channel_4; }
	inline void set_Channel_4(String_t* value)
	{
		___Channel_4 = value;
		Il2CppCodeGenWriteBarrier((&___Channel_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELSELECTOR_T3480308131_H
#ifndef CHATAPPIDCHECKERUI_T722438586_H
#define CHATAPPIDCHECKERUI_T722438586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatAppIdCheckerUI
struct  ChatAppIdCheckerUI_t722438586  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ChatAppIdCheckerUI::Description
	Text_t1901882714 * ___Description_4;

public:
	inline static int32_t get_offset_of_Description_4() { return static_cast<int32_t>(offsetof(ChatAppIdCheckerUI_t722438586, ___Description_4)); }
	inline Text_t1901882714 * get_Description_4() const { return ___Description_4; }
	inline Text_t1901882714 ** get_address_of_Description_4() { return &___Description_4; }
	inline void set_Description_4(Text_t1901882714 * value)
	{
		___Description_4 = value;
		Il2CppCodeGenWriteBarrier((&___Description_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATAPPIDCHECKERUI_T722438586_H
#ifndef CHATGUI_T3673337520_H
#define CHATGUI_T3673337520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatGui
struct  ChatGui_t3673337520  : public MonoBehaviour_t3962482529
{
public:
	// System.String[] ChatGui::ChannelsToJoinOnConnect
	StringU5BU5D_t1281789340* ___ChannelsToJoinOnConnect_4;
	// System.String[] ChatGui::FriendsList
	StringU5BU5D_t1281789340* ___FriendsList_5;
	// System.Int32 ChatGui::HistoryLengthToFetch
	int32_t ___HistoryLengthToFetch_6;
	// System.String ChatGui::<UserName>k__BackingField
	String_t* ___U3CUserNameU3Ek__BackingField_7;
	// System.String ChatGui::selectedChannelName
	String_t* ___selectedChannelName_8;
	// Photon.Chat.ChatClient ChatGui::chatClient
	ChatClient_t792052210 * ___chatClient_9;
	// Photon.Realtime.AppSettings ChatGui::chatAppSettings
	AppSettings_t3860110421 * ___chatAppSettings_10;
	// UnityEngine.GameObject ChatGui::missingAppIdErrorPanel
	GameObject_t1113636619 * ___missingAppIdErrorPanel_11;
	// UnityEngine.GameObject ChatGui::ConnectingLabel
	GameObject_t1113636619 * ___ConnectingLabel_12;
	// UnityEngine.RectTransform ChatGui::ChatPanel
	RectTransform_t3704657025 * ___ChatPanel_13;
	// UnityEngine.GameObject ChatGui::UserIdFormPanel
	GameObject_t1113636619 * ___UserIdFormPanel_14;
	// UnityEngine.UI.InputField ChatGui::InputFieldChat
	InputField_t3762917431 * ___InputFieldChat_15;
	// UnityEngine.UI.Text ChatGui::CurrentChannelText
	Text_t1901882714 * ___CurrentChannelText_16;
	// UnityEngine.UI.Toggle ChatGui::ChannelToggleToInstantiate
	Toggle_t2735377061 * ___ChannelToggleToInstantiate_17;
	// UnityEngine.GameObject ChatGui::FriendListUiItemtoInstantiate
	GameObject_t1113636619 * ___FriendListUiItemtoInstantiate_18;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UI.Toggle> ChatGui::channelToggles
	Dictionary_2_t2520633360 * ___channelToggles_19;
	// System.Collections.Generic.Dictionary`2<System.String,FriendItem> ChatGui::friendListItemLUT
	Dictionary_2_t2701966392 * ___friendListItemLUT_20;
	// System.Boolean ChatGui::ShowState
	bool ___ShowState_21;
	// UnityEngine.GameObject ChatGui::Title
	GameObject_t1113636619 * ___Title_22;
	// UnityEngine.UI.Text ChatGui::StateText
	Text_t1901882714 * ___StateText_23;
	// UnityEngine.UI.Text ChatGui::UserIdText
	Text_t1901882714 * ___UserIdText_24;
	// System.Int32 ChatGui::TestLength
	int32_t ___TestLength_26;
	// System.Byte[] ChatGui::testBytes
	ByteU5BU5D_t4116647657* ___testBytes_27;

public:
	inline static int32_t get_offset_of_ChannelsToJoinOnConnect_4() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___ChannelsToJoinOnConnect_4)); }
	inline StringU5BU5D_t1281789340* get_ChannelsToJoinOnConnect_4() const { return ___ChannelsToJoinOnConnect_4; }
	inline StringU5BU5D_t1281789340** get_address_of_ChannelsToJoinOnConnect_4() { return &___ChannelsToJoinOnConnect_4; }
	inline void set_ChannelsToJoinOnConnect_4(StringU5BU5D_t1281789340* value)
	{
		___ChannelsToJoinOnConnect_4 = value;
		Il2CppCodeGenWriteBarrier((&___ChannelsToJoinOnConnect_4), value);
	}

	inline static int32_t get_offset_of_FriendsList_5() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___FriendsList_5)); }
	inline StringU5BU5D_t1281789340* get_FriendsList_5() const { return ___FriendsList_5; }
	inline StringU5BU5D_t1281789340** get_address_of_FriendsList_5() { return &___FriendsList_5; }
	inline void set_FriendsList_5(StringU5BU5D_t1281789340* value)
	{
		___FriendsList_5 = value;
		Il2CppCodeGenWriteBarrier((&___FriendsList_5), value);
	}

	inline static int32_t get_offset_of_HistoryLengthToFetch_6() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___HistoryLengthToFetch_6)); }
	inline int32_t get_HistoryLengthToFetch_6() const { return ___HistoryLengthToFetch_6; }
	inline int32_t* get_address_of_HistoryLengthToFetch_6() { return &___HistoryLengthToFetch_6; }
	inline void set_HistoryLengthToFetch_6(int32_t value)
	{
		___HistoryLengthToFetch_6 = value;
	}

	inline static int32_t get_offset_of_U3CUserNameU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___U3CUserNameU3Ek__BackingField_7)); }
	inline String_t* get_U3CUserNameU3Ek__BackingField_7() const { return ___U3CUserNameU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CUserNameU3Ek__BackingField_7() { return &___U3CUserNameU3Ek__BackingField_7; }
	inline void set_U3CUserNameU3Ek__BackingField_7(String_t* value)
	{
		___U3CUserNameU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserNameU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_selectedChannelName_8() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___selectedChannelName_8)); }
	inline String_t* get_selectedChannelName_8() const { return ___selectedChannelName_8; }
	inline String_t** get_address_of_selectedChannelName_8() { return &___selectedChannelName_8; }
	inline void set_selectedChannelName_8(String_t* value)
	{
		___selectedChannelName_8 = value;
		Il2CppCodeGenWriteBarrier((&___selectedChannelName_8), value);
	}

	inline static int32_t get_offset_of_chatClient_9() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___chatClient_9)); }
	inline ChatClient_t792052210 * get_chatClient_9() const { return ___chatClient_9; }
	inline ChatClient_t792052210 ** get_address_of_chatClient_9() { return &___chatClient_9; }
	inline void set_chatClient_9(ChatClient_t792052210 * value)
	{
		___chatClient_9 = value;
		Il2CppCodeGenWriteBarrier((&___chatClient_9), value);
	}

	inline static int32_t get_offset_of_chatAppSettings_10() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___chatAppSettings_10)); }
	inline AppSettings_t3860110421 * get_chatAppSettings_10() const { return ___chatAppSettings_10; }
	inline AppSettings_t3860110421 ** get_address_of_chatAppSettings_10() { return &___chatAppSettings_10; }
	inline void set_chatAppSettings_10(AppSettings_t3860110421 * value)
	{
		___chatAppSettings_10 = value;
		Il2CppCodeGenWriteBarrier((&___chatAppSettings_10), value);
	}

	inline static int32_t get_offset_of_missingAppIdErrorPanel_11() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___missingAppIdErrorPanel_11)); }
	inline GameObject_t1113636619 * get_missingAppIdErrorPanel_11() const { return ___missingAppIdErrorPanel_11; }
	inline GameObject_t1113636619 ** get_address_of_missingAppIdErrorPanel_11() { return &___missingAppIdErrorPanel_11; }
	inline void set_missingAppIdErrorPanel_11(GameObject_t1113636619 * value)
	{
		___missingAppIdErrorPanel_11 = value;
		Il2CppCodeGenWriteBarrier((&___missingAppIdErrorPanel_11), value);
	}

	inline static int32_t get_offset_of_ConnectingLabel_12() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___ConnectingLabel_12)); }
	inline GameObject_t1113636619 * get_ConnectingLabel_12() const { return ___ConnectingLabel_12; }
	inline GameObject_t1113636619 ** get_address_of_ConnectingLabel_12() { return &___ConnectingLabel_12; }
	inline void set_ConnectingLabel_12(GameObject_t1113636619 * value)
	{
		___ConnectingLabel_12 = value;
		Il2CppCodeGenWriteBarrier((&___ConnectingLabel_12), value);
	}

	inline static int32_t get_offset_of_ChatPanel_13() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___ChatPanel_13)); }
	inline RectTransform_t3704657025 * get_ChatPanel_13() const { return ___ChatPanel_13; }
	inline RectTransform_t3704657025 ** get_address_of_ChatPanel_13() { return &___ChatPanel_13; }
	inline void set_ChatPanel_13(RectTransform_t3704657025 * value)
	{
		___ChatPanel_13 = value;
		Il2CppCodeGenWriteBarrier((&___ChatPanel_13), value);
	}

	inline static int32_t get_offset_of_UserIdFormPanel_14() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___UserIdFormPanel_14)); }
	inline GameObject_t1113636619 * get_UserIdFormPanel_14() const { return ___UserIdFormPanel_14; }
	inline GameObject_t1113636619 ** get_address_of_UserIdFormPanel_14() { return &___UserIdFormPanel_14; }
	inline void set_UserIdFormPanel_14(GameObject_t1113636619 * value)
	{
		___UserIdFormPanel_14 = value;
		Il2CppCodeGenWriteBarrier((&___UserIdFormPanel_14), value);
	}

	inline static int32_t get_offset_of_InputFieldChat_15() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___InputFieldChat_15)); }
	inline InputField_t3762917431 * get_InputFieldChat_15() const { return ___InputFieldChat_15; }
	inline InputField_t3762917431 ** get_address_of_InputFieldChat_15() { return &___InputFieldChat_15; }
	inline void set_InputFieldChat_15(InputField_t3762917431 * value)
	{
		___InputFieldChat_15 = value;
		Il2CppCodeGenWriteBarrier((&___InputFieldChat_15), value);
	}

	inline static int32_t get_offset_of_CurrentChannelText_16() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___CurrentChannelText_16)); }
	inline Text_t1901882714 * get_CurrentChannelText_16() const { return ___CurrentChannelText_16; }
	inline Text_t1901882714 ** get_address_of_CurrentChannelText_16() { return &___CurrentChannelText_16; }
	inline void set_CurrentChannelText_16(Text_t1901882714 * value)
	{
		___CurrentChannelText_16 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentChannelText_16), value);
	}

	inline static int32_t get_offset_of_ChannelToggleToInstantiate_17() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___ChannelToggleToInstantiate_17)); }
	inline Toggle_t2735377061 * get_ChannelToggleToInstantiate_17() const { return ___ChannelToggleToInstantiate_17; }
	inline Toggle_t2735377061 ** get_address_of_ChannelToggleToInstantiate_17() { return &___ChannelToggleToInstantiate_17; }
	inline void set_ChannelToggleToInstantiate_17(Toggle_t2735377061 * value)
	{
		___ChannelToggleToInstantiate_17 = value;
		Il2CppCodeGenWriteBarrier((&___ChannelToggleToInstantiate_17), value);
	}

	inline static int32_t get_offset_of_FriendListUiItemtoInstantiate_18() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___FriendListUiItemtoInstantiate_18)); }
	inline GameObject_t1113636619 * get_FriendListUiItemtoInstantiate_18() const { return ___FriendListUiItemtoInstantiate_18; }
	inline GameObject_t1113636619 ** get_address_of_FriendListUiItemtoInstantiate_18() { return &___FriendListUiItemtoInstantiate_18; }
	inline void set_FriendListUiItemtoInstantiate_18(GameObject_t1113636619 * value)
	{
		___FriendListUiItemtoInstantiate_18 = value;
		Il2CppCodeGenWriteBarrier((&___FriendListUiItemtoInstantiate_18), value);
	}

	inline static int32_t get_offset_of_channelToggles_19() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___channelToggles_19)); }
	inline Dictionary_2_t2520633360 * get_channelToggles_19() const { return ___channelToggles_19; }
	inline Dictionary_2_t2520633360 ** get_address_of_channelToggles_19() { return &___channelToggles_19; }
	inline void set_channelToggles_19(Dictionary_2_t2520633360 * value)
	{
		___channelToggles_19 = value;
		Il2CppCodeGenWriteBarrier((&___channelToggles_19), value);
	}

	inline static int32_t get_offset_of_friendListItemLUT_20() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___friendListItemLUT_20)); }
	inline Dictionary_2_t2701966392 * get_friendListItemLUT_20() const { return ___friendListItemLUT_20; }
	inline Dictionary_2_t2701966392 ** get_address_of_friendListItemLUT_20() { return &___friendListItemLUT_20; }
	inline void set_friendListItemLUT_20(Dictionary_2_t2701966392 * value)
	{
		___friendListItemLUT_20 = value;
		Il2CppCodeGenWriteBarrier((&___friendListItemLUT_20), value);
	}

	inline static int32_t get_offset_of_ShowState_21() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___ShowState_21)); }
	inline bool get_ShowState_21() const { return ___ShowState_21; }
	inline bool* get_address_of_ShowState_21() { return &___ShowState_21; }
	inline void set_ShowState_21(bool value)
	{
		___ShowState_21 = value;
	}

	inline static int32_t get_offset_of_Title_22() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___Title_22)); }
	inline GameObject_t1113636619 * get_Title_22() const { return ___Title_22; }
	inline GameObject_t1113636619 ** get_address_of_Title_22() { return &___Title_22; }
	inline void set_Title_22(GameObject_t1113636619 * value)
	{
		___Title_22 = value;
		Il2CppCodeGenWriteBarrier((&___Title_22), value);
	}

	inline static int32_t get_offset_of_StateText_23() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___StateText_23)); }
	inline Text_t1901882714 * get_StateText_23() const { return ___StateText_23; }
	inline Text_t1901882714 ** get_address_of_StateText_23() { return &___StateText_23; }
	inline void set_StateText_23(Text_t1901882714 * value)
	{
		___StateText_23 = value;
		Il2CppCodeGenWriteBarrier((&___StateText_23), value);
	}

	inline static int32_t get_offset_of_UserIdText_24() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___UserIdText_24)); }
	inline Text_t1901882714 * get_UserIdText_24() const { return ___UserIdText_24; }
	inline Text_t1901882714 ** get_address_of_UserIdText_24() { return &___UserIdText_24; }
	inline void set_UserIdText_24(Text_t1901882714 * value)
	{
		___UserIdText_24 = value;
		Il2CppCodeGenWriteBarrier((&___UserIdText_24), value);
	}

	inline static int32_t get_offset_of_TestLength_26() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___TestLength_26)); }
	inline int32_t get_TestLength_26() const { return ___TestLength_26; }
	inline int32_t* get_address_of_TestLength_26() { return &___TestLength_26; }
	inline void set_TestLength_26(int32_t value)
	{
		___TestLength_26 = value;
	}

	inline static int32_t get_offset_of_testBytes_27() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520, ___testBytes_27)); }
	inline ByteU5BU5D_t4116647657* get_testBytes_27() const { return ___testBytes_27; }
	inline ByteU5BU5D_t4116647657** get_address_of_testBytes_27() { return &___testBytes_27; }
	inline void set_testBytes_27(ByteU5BU5D_t4116647657* value)
	{
		___testBytes_27 = value;
		Il2CppCodeGenWriteBarrier((&___testBytes_27), value);
	}
};

struct ChatGui_t3673337520_StaticFields
{
public:
	// System.String ChatGui::HelpText
	String_t* ___HelpText_25;

public:
	inline static int32_t get_offset_of_HelpText_25() { return static_cast<int32_t>(offsetof(ChatGui_t3673337520_StaticFields, ___HelpText_25)); }
	inline String_t* get_HelpText_25() const { return ___HelpText_25; }
	inline String_t** get_address_of_HelpText_25() { return &___HelpText_25; }
	inline void set_HelpText_25(String_t* value)
	{
		___HelpText_25 = value;
		Il2CppCodeGenWriteBarrier((&___HelpText_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATGUI_T3673337520_H
#ifndef FRIENDITEM_T2916710093_H
#define FRIENDITEM_T2916710093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendItem
struct  FriendItem_t2916710093  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text FriendItem::NameLabel
	Text_t1901882714 * ___NameLabel_4;
	// UnityEngine.UI.Text FriendItem::StatusLabel
	Text_t1901882714 * ___StatusLabel_5;
	// UnityEngine.UI.Text FriendItem::Health
	Text_t1901882714 * ___Health_6;

public:
	inline static int32_t get_offset_of_NameLabel_4() { return static_cast<int32_t>(offsetof(FriendItem_t2916710093, ___NameLabel_4)); }
	inline Text_t1901882714 * get_NameLabel_4() const { return ___NameLabel_4; }
	inline Text_t1901882714 ** get_address_of_NameLabel_4() { return &___NameLabel_4; }
	inline void set_NameLabel_4(Text_t1901882714 * value)
	{
		___NameLabel_4 = value;
		Il2CppCodeGenWriteBarrier((&___NameLabel_4), value);
	}

	inline static int32_t get_offset_of_StatusLabel_5() { return static_cast<int32_t>(offsetof(FriendItem_t2916710093, ___StatusLabel_5)); }
	inline Text_t1901882714 * get_StatusLabel_5() const { return ___StatusLabel_5; }
	inline Text_t1901882714 ** get_address_of_StatusLabel_5() { return &___StatusLabel_5; }
	inline void set_StatusLabel_5(Text_t1901882714 * value)
	{
		___StatusLabel_5 = value;
		Il2CppCodeGenWriteBarrier((&___StatusLabel_5), value);
	}

	inline static int32_t get_offset_of_Health_6() { return static_cast<int32_t>(offsetof(FriendItem_t2916710093, ___Health_6)); }
	inline Text_t1901882714 * get_Health_6() const { return ___Health_6; }
	inline Text_t1901882714 ** get_address_of_Health_6() { return &___Health_6; }
	inline void set_Health_6(Text_t1901882714 * value)
	{
		___Health_6 = value;
		Il2CppCodeGenWriteBarrier((&___Health_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRIENDITEM_T2916710093_H
#ifndef IGNOREUIRAYCASTWHENINACTIVE_T3624510311_H
#define IGNOREUIRAYCASTWHENINACTIVE_T3624510311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IgnoreUiRaycastWhenInactive
struct  IgnoreUiRaycastWhenInactive_t3624510311  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGNOREUIRAYCASTWHENINACTIVE_T3624510311_H
#ifndef NAMEPICKGUI_T2955268898_H
#define NAMEPICKGUI_T2955268898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NamePickGui
struct  NamePickGui_t2955268898  : public MonoBehaviour_t3962482529
{
public:
	// ChatGui NamePickGui::chatNewComponent
	ChatGui_t3673337520 * ___chatNewComponent_5;
	// UnityEngine.UI.InputField NamePickGui::idInput
	InputField_t3762917431 * ___idInput_6;

public:
	inline static int32_t get_offset_of_chatNewComponent_5() { return static_cast<int32_t>(offsetof(NamePickGui_t2955268898, ___chatNewComponent_5)); }
	inline ChatGui_t3673337520 * get_chatNewComponent_5() const { return ___chatNewComponent_5; }
	inline ChatGui_t3673337520 ** get_address_of_chatNewComponent_5() { return &___chatNewComponent_5; }
	inline void set_chatNewComponent_5(ChatGui_t3673337520 * value)
	{
		___chatNewComponent_5 = value;
		Il2CppCodeGenWriteBarrier((&___chatNewComponent_5), value);
	}

	inline static int32_t get_offset_of_idInput_6() { return static_cast<int32_t>(offsetof(NamePickGui_t2955268898, ___idInput_6)); }
	inline InputField_t3762917431 * get_idInput_6() const { return ___idInput_6; }
	inline InputField_t3762917431 ** get_address_of_idInput_6() { return &___idInput_6; }
	inline void set_idInput_6(InputField_t3762917431 * value)
	{
		___idInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___idInput_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEPICKGUI_T2955268898_H
#ifndef EVENTSYSTEMSPAWNER_T790404666_H
#define EVENTSYSTEMSPAWNER_T790404666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Chat.UtilityScripts.EventSystemSpawner
struct  EventSystemSpawner_t790404666  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEMSPAWNER_T790404666_H
#ifndef ONSTARTDELETE_T2848433495_H
#define ONSTARTDELETE_T2848433495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Chat.UtilityScripts.OnStartDelete
struct  OnStartDelete_t2848433495  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSTARTDELETE_T2848433495_H
#ifndef TEXTBUTTONTRANSITION_T7860473_H
#define TEXTBUTTONTRANSITION_T7860473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Chat.UtilityScripts.TextButtonTransition
struct  TextButtonTransition_t7860473  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text Photon.Chat.UtilityScripts.TextButtonTransition::_text
	Text_t1901882714 * ____text_4;
	// UnityEngine.UI.Selectable Photon.Chat.UtilityScripts.TextButtonTransition::Selectable
	Selectable_t3250028441 * ___Selectable_5;
	// UnityEngine.Color Photon.Chat.UtilityScripts.TextButtonTransition::NormalColor
	Color_t2555686324  ___NormalColor_6;
	// UnityEngine.Color Photon.Chat.UtilityScripts.TextButtonTransition::HoverColor
	Color_t2555686324  ___HoverColor_7;

public:
	inline static int32_t get_offset_of__text_4() { return static_cast<int32_t>(offsetof(TextButtonTransition_t7860473, ____text_4)); }
	inline Text_t1901882714 * get__text_4() const { return ____text_4; }
	inline Text_t1901882714 ** get_address_of__text_4() { return &____text_4; }
	inline void set__text_4(Text_t1901882714 * value)
	{
		____text_4 = value;
		Il2CppCodeGenWriteBarrier((&____text_4), value);
	}

	inline static int32_t get_offset_of_Selectable_5() { return static_cast<int32_t>(offsetof(TextButtonTransition_t7860473, ___Selectable_5)); }
	inline Selectable_t3250028441 * get_Selectable_5() const { return ___Selectable_5; }
	inline Selectable_t3250028441 ** get_address_of_Selectable_5() { return &___Selectable_5; }
	inline void set_Selectable_5(Selectable_t3250028441 * value)
	{
		___Selectable_5 = value;
		Il2CppCodeGenWriteBarrier((&___Selectable_5), value);
	}

	inline static int32_t get_offset_of_NormalColor_6() { return static_cast<int32_t>(offsetof(TextButtonTransition_t7860473, ___NormalColor_6)); }
	inline Color_t2555686324  get_NormalColor_6() const { return ___NormalColor_6; }
	inline Color_t2555686324 * get_address_of_NormalColor_6() { return &___NormalColor_6; }
	inline void set_NormalColor_6(Color_t2555686324  value)
	{
		___NormalColor_6 = value;
	}

	inline static int32_t get_offset_of_HoverColor_7() { return static_cast<int32_t>(offsetof(TextButtonTransition_t7860473, ___HoverColor_7)); }
	inline Color_t2555686324  get_HoverColor_7() const { return ___HoverColor_7; }
	inline Color_t2555686324 * get_address_of_HoverColor_7() { return &___HoverColor_7; }
	inline void set_HoverColor_7(Color_t2555686324  value)
	{
		___HoverColor_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTBUTTONTRANSITION_T7860473_H
#ifndef TEXTTOGGLEISONTRANSITION_T2348424493_H
#define TEXTTOGGLEISONTRANSITION_T2348424493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Chat.UtilityScripts.TextToggleIsOnTransition
struct  TextToggleIsOnTransition_t2348424493  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Toggle Photon.Chat.UtilityScripts.TextToggleIsOnTransition::toggle
	Toggle_t2735377061 * ___toggle_4;
	// UnityEngine.UI.Text Photon.Chat.UtilityScripts.TextToggleIsOnTransition::_text
	Text_t1901882714 * ____text_5;
	// UnityEngine.Color Photon.Chat.UtilityScripts.TextToggleIsOnTransition::NormalOnColor
	Color_t2555686324  ___NormalOnColor_6;
	// UnityEngine.Color Photon.Chat.UtilityScripts.TextToggleIsOnTransition::NormalOffColor
	Color_t2555686324  ___NormalOffColor_7;
	// UnityEngine.Color Photon.Chat.UtilityScripts.TextToggleIsOnTransition::HoverOnColor
	Color_t2555686324  ___HoverOnColor_8;
	// UnityEngine.Color Photon.Chat.UtilityScripts.TextToggleIsOnTransition::HoverOffColor
	Color_t2555686324  ___HoverOffColor_9;
	// System.Boolean Photon.Chat.UtilityScripts.TextToggleIsOnTransition::isHover
	bool ___isHover_10;

public:
	inline static int32_t get_offset_of_toggle_4() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t2348424493, ___toggle_4)); }
	inline Toggle_t2735377061 * get_toggle_4() const { return ___toggle_4; }
	inline Toggle_t2735377061 ** get_address_of_toggle_4() { return &___toggle_4; }
	inline void set_toggle_4(Toggle_t2735377061 * value)
	{
		___toggle_4 = value;
		Il2CppCodeGenWriteBarrier((&___toggle_4), value);
	}

	inline static int32_t get_offset_of__text_5() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t2348424493, ____text_5)); }
	inline Text_t1901882714 * get__text_5() const { return ____text_5; }
	inline Text_t1901882714 ** get_address_of__text_5() { return &____text_5; }
	inline void set__text_5(Text_t1901882714 * value)
	{
		____text_5 = value;
		Il2CppCodeGenWriteBarrier((&____text_5), value);
	}

	inline static int32_t get_offset_of_NormalOnColor_6() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t2348424493, ___NormalOnColor_6)); }
	inline Color_t2555686324  get_NormalOnColor_6() const { return ___NormalOnColor_6; }
	inline Color_t2555686324 * get_address_of_NormalOnColor_6() { return &___NormalOnColor_6; }
	inline void set_NormalOnColor_6(Color_t2555686324  value)
	{
		___NormalOnColor_6 = value;
	}

	inline static int32_t get_offset_of_NormalOffColor_7() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t2348424493, ___NormalOffColor_7)); }
	inline Color_t2555686324  get_NormalOffColor_7() const { return ___NormalOffColor_7; }
	inline Color_t2555686324 * get_address_of_NormalOffColor_7() { return &___NormalOffColor_7; }
	inline void set_NormalOffColor_7(Color_t2555686324  value)
	{
		___NormalOffColor_7 = value;
	}

	inline static int32_t get_offset_of_HoverOnColor_8() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t2348424493, ___HoverOnColor_8)); }
	inline Color_t2555686324  get_HoverOnColor_8() const { return ___HoverOnColor_8; }
	inline Color_t2555686324 * get_address_of_HoverOnColor_8() { return &___HoverOnColor_8; }
	inline void set_HoverOnColor_8(Color_t2555686324  value)
	{
		___HoverOnColor_8 = value;
	}

	inline static int32_t get_offset_of_HoverOffColor_9() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t2348424493, ___HoverOffColor_9)); }
	inline Color_t2555686324  get_HoverOffColor_9() const { return ___HoverOffColor_9; }
	inline Color_t2555686324 * get_address_of_HoverOffColor_9() { return &___HoverOffColor_9; }
	inline void set_HoverOffColor_9(Color_t2555686324  value)
	{
		___HoverOffColor_9 = value;
	}

	inline static int32_t get_offset_of_isHover_10() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t2348424493, ___isHover_10)); }
	inline bool get_isHover_10() const { return ___isHover_10; }
	inline bool* get_address_of_isHover_10() { return &___isHover_10; }
	inline void set_isHover_10(bool value)
	{
		___isHover_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTTOGGLEISONTRANSITION_T2348424493_H
#ifndef MONOBEHAVIOURPUN_T1682334777_H
#define MONOBEHAVIOURPUN_T1682334777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.MonoBehaviourPun
struct  MonoBehaviourPun_t1682334777  : public MonoBehaviour_t3962482529
{
public:
	// Photon.Pun.PhotonView Photon.Pun.MonoBehaviourPun::pvCache
	PhotonView_t3684715584 * ___pvCache_4;

public:
	inline static int32_t get_offset_of_pvCache_4() { return static_cast<int32_t>(offsetof(MonoBehaviourPun_t1682334777, ___pvCache_4)); }
	inline PhotonView_t3684715584 * get_pvCache_4() const { return ___pvCache_4; }
	inline PhotonView_t3684715584 ** get_address_of_pvCache_4() { return &___pvCache_4; }
	inline void set_pvCache_4(PhotonView_t3684715584 * value)
	{
		___pvCache_4 = value;
		Il2CppCodeGenWriteBarrier((&___pvCache_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOURPUN_T1682334777_H
#ifndef BUTTONINSIDESCROLLLIST_T2732141882_H
#define BUTTONINSIDESCROLLLIST_T2732141882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.ButtonInsideScrollList
struct  ButtonInsideScrollList_t2732141882  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.ScrollRect Photon.Pun.UtilityScripts.ButtonInsideScrollList::scrollRect
	ScrollRect_t4137855814 * ___scrollRect_4;

public:
	inline static int32_t get_offset_of_scrollRect_4() { return static_cast<int32_t>(offsetof(ButtonInsideScrollList_t2732141882, ___scrollRect_4)); }
	inline ScrollRect_t4137855814 * get_scrollRect_4() const { return ___scrollRect_4; }
	inline ScrollRect_t4137855814 ** get_address_of_scrollRect_4() { return &___scrollRect_4; }
	inline void set_scrollRect_4(ScrollRect_t4137855814 * value)
	{
		___scrollRect_4 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONINSIDESCROLLLIST_T2732141882_H
#ifndef CULLAREA_T635391622_H
#define CULLAREA_T635391622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.CullArea
struct  CullArea_t635391622  : public MonoBehaviour_t3962482529
{
public:
	// System.Byte Photon.Pun.UtilityScripts.CullArea::FIRST_GROUP_ID
	uint8_t ___FIRST_GROUP_ID_6;
	// System.Int32[] Photon.Pun.UtilityScripts.CullArea::SUBDIVISION_FIRST_LEVEL_ORDER
	Int32U5BU5D_t385246372* ___SUBDIVISION_FIRST_LEVEL_ORDER_7;
	// System.Int32[] Photon.Pun.UtilityScripts.CullArea::SUBDIVISION_SECOND_LEVEL_ORDER
	Int32U5BU5D_t385246372* ___SUBDIVISION_SECOND_LEVEL_ORDER_8;
	// System.Int32[] Photon.Pun.UtilityScripts.CullArea::SUBDIVISION_THIRD_LEVEL_ORDER
	Int32U5BU5D_t385246372* ___SUBDIVISION_THIRD_LEVEL_ORDER_9;
	// UnityEngine.Vector2 Photon.Pun.UtilityScripts.CullArea::Center
	Vector2_t2156229523  ___Center_10;
	// UnityEngine.Vector2 Photon.Pun.UtilityScripts.CullArea::Size
	Vector2_t2156229523  ___Size_11;
	// UnityEngine.Vector2[] Photon.Pun.UtilityScripts.CullArea::Subdivisions
	Vector2U5BU5D_t1457185986* ___Subdivisions_12;
	// System.Int32 Photon.Pun.UtilityScripts.CullArea::NumberOfSubdivisions
	int32_t ___NumberOfSubdivisions_13;
	// System.Int32 Photon.Pun.UtilityScripts.CullArea::<CellCount>k__BackingField
	int32_t ___U3CCellCountU3Ek__BackingField_14;
	// Photon.Pun.UtilityScripts.CellTree Photon.Pun.UtilityScripts.CullArea::<CellTree>k__BackingField
	CellTree_t656254725 * ___U3CCellTreeU3Ek__BackingField_15;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject> Photon.Pun.UtilityScripts.CullArea::<Map>k__BackingField
	Dictionary_2_t2349950 * ___U3CMapU3Ek__BackingField_16;
	// System.Boolean Photon.Pun.UtilityScripts.CullArea::YIsUpAxis
	bool ___YIsUpAxis_17;
	// System.Boolean Photon.Pun.UtilityScripts.CullArea::RecreateCellHierarchy
	bool ___RecreateCellHierarchy_18;
	// System.Byte Photon.Pun.UtilityScripts.CullArea::idCounter
	uint8_t ___idCounter_19;

public:
	inline static int32_t get_offset_of_FIRST_GROUP_ID_6() { return static_cast<int32_t>(offsetof(CullArea_t635391622, ___FIRST_GROUP_ID_6)); }
	inline uint8_t get_FIRST_GROUP_ID_6() const { return ___FIRST_GROUP_ID_6; }
	inline uint8_t* get_address_of_FIRST_GROUP_ID_6() { return &___FIRST_GROUP_ID_6; }
	inline void set_FIRST_GROUP_ID_6(uint8_t value)
	{
		___FIRST_GROUP_ID_6 = value;
	}

	inline static int32_t get_offset_of_SUBDIVISION_FIRST_LEVEL_ORDER_7() { return static_cast<int32_t>(offsetof(CullArea_t635391622, ___SUBDIVISION_FIRST_LEVEL_ORDER_7)); }
	inline Int32U5BU5D_t385246372* get_SUBDIVISION_FIRST_LEVEL_ORDER_7() const { return ___SUBDIVISION_FIRST_LEVEL_ORDER_7; }
	inline Int32U5BU5D_t385246372** get_address_of_SUBDIVISION_FIRST_LEVEL_ORDER_7() { return &___SUBDIVISION_FIRST_LEVEL_ORDER_7; }
	inline void set_SUBDIVISION_FIRST_LEVEL_ORDER_7(Int32U5BU5D_t385246372* value)
	{
		___SUBDIVISION_FIRST_LEVEL_ORDER_7 = value;
		Il2CppCodeGenWriteBarrier((&___SUBDIVISION_FIRST_LEVEL_ORDER_7), value);
	}

	inline static int32_t get_offset_of_SUBDIVISION_SECOND_LEVEL_ORDER_8() { return static_cast<int32_t>(offsetof(CullArea_t635391622, ___SUBDIVISION_SECOND_LEVEL_ORDER_8)); }
	inline Int32U5BU5D_t385246372* get_SUBDIVISION_SECOND_LEVEL_ORDER_8() const { return ___SUBDIVISION_SECOND_LEVEL_ORDER_8; }
	inline Int32U5BU5D_t385246372** get_address_of_SUBDIVISION_SECOND_LEVEL_ORDER_8() { return &___SUBDIVISION_SECOND_LEVEL_ORDER_8; }
	inline void set_SUBDIVISION_SECOND_LEVEL_ORDER_8(Int32U5BU5D_t385246372* value)
	{
		___SUBDIVISION_SECOND_LEVEL_ORDER_8 = value;
		Il2CppCodeGenWriteBarrier((&___SUBDIVISION_SECOND_LEVEL_ORDER_8), value);
	}

	inline static int32_t get_offset_of_SUBDIVISION_THIRD_LEVEL_ORDER_9() { return static_cast<int32_t>(offsetof(CullArea_t635391622, ___SUBDIVISION_THIRD_LEVEL_ORDER_9)); }
	inline Int32U5BU5D_t385246372* get_SUBDIVISION_THIRD_LEVEL_ORDER_9() const { return ___SUBDIVISION_THIRD_LEVEL_ORDER_9; }
	inline Int32U5BU5D_t385246372** get_address_of_SUBDIVISION_THIRD_LEVEL_ORDER_9() { return &___SUBDIVISION_THIRD_LEVEL_ORDER_9; }
	inline void set_SUBDIVISION_THIRD_LEVEL_ORDER_9(Int32U5BU5D_t385246372* value)
	{
		___SUBDIVISION_THIRD_LEVEL_ORDER_9 = value;
		Il2CppCodeGenWriteBarrier((&___SUBDIVISION_THIRD_LEVEL_ORDER_9), value);
	}

	inline static int32_t get_offset_of_Center_10() { return static_cast<int32_t>(offsetof(CullArea_t635391622, ___Center_10)); }
	inline Vector2_t2156229523  get_Center_10() const { return ___Center_10; }
	inline Vector2_t2156229523 * get_address_of_Center_10() { return &___Center_10; }
	inline void set_Center_10(Vector2_t2156229523  value)
	{
		___Center_10 = value;
	}

	inline static int32_t get_offset_of_Size_11() { return static_cast<int32_t>(offsetof(CullArea_t635391622, ___Size_11)); }
	inline Vector2_t2156229523  get_Size_11() const { return ___Size_11; }
	inline Vector2_t2156229523 * get_address_of_Size_11() { return &___Size_11; }
	inline void set_Size_11(Vector2_t2156229523  value)
	{
		___Size_11 = value;
	}

	inline static int32_t get_offset_of_Subdivisions_12() { return static_cast<int32_t>(offsetof(CullArea_t635391622, ___Subdivisions_12)); }
	inline Vector2U5BU5D_t1457185986* get_Subdivisions_12() const { return ___Subdivisions_12; }
	inline Vector2U5BU5D_t1457185986** get_address_of_Subdivisions_12() { return &___Subdivisions_12; }
	inline void set_Subdivisions_12(Vector2U5BU5D_t1457185986* value)
	{
		___Subdivisions_12 = value;
		Il2CppCodeGenWriteBarrier((&___Subdivisions_12), value);
	}

	inline static int32_t get_offset_of_NumberOfSubdivisions_13() { return static_cast<int32_t>(offsetof(CullArea_t635391622, ___NumberOfSubdivisions_13)); }
	inline int32_t get_NumberOfSubdivisions_13() const { return ___NumberOfSubdivisions_13; }
	inline int32_t* get_address_of_NumberOfSubdivisions_13() { return &___NumberOfSubdivisions_13; }
	inline void set_NumberOfSubdivisions_13(int32_t value)
	{
		___NumberOfSubdivisions_13 = value;
	}

	inline static int32_t get_offset_of_U3CCellCountU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CullArea_t635391622, ___U3CCellCountU3Ek__BackingField_14)); }
	inline int32_t get_U3CCellCountU3Ek__BackingField_14() const { return ___U3CCellCountU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CCellCountU3Ek__BackingField_14() { return &___U3CCellCountU3Ek__BackingField_14; }
	inline void set_U3CCellCountU3Ek__BackingField_14(int32_t value)
	{
		___U3CCellCountU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CCellTreeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(CullArea_t635391622, ___U3CCellTreeU3Ek__BackingField_15)); }
	inline CellTree_t656254725 * get_U3CCellTreeU3Ek__BackingField_15() const { return ___U3CCellTreeU3Ek__BackingField_15; }
	inline CellTree_t656254725 ** get_address_of_U3CCellTreeU3Ek__BackingField_15() { return &___U3CCellTreeU3Ek__BackingField_15; }
	inline void set_U3CCellTreeU3Ek__BackingField_15(CellTree_t656254725 * value)
	{
		___U3CCellTreeU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCellTreeU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CMapU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(CullArea_t635391622, ___U3CMapU3Ek__BackingField_16)); }
	inline Dictionary_2_t2349950 * get_U3CMapU3Ek__BackingField_16() const { return ___U3CMapU3Ek__BackingField_16; }
	inline Dictionary_2_t2349950 ** get_address_of_U3CMapU3Ek__BackingField_16() { return &___U3CMapU3Ek__BackingField_16; }
	inline void set_U3CMapU3Ek__BackingField_16(Dictionary_2_t2349950 * value)
	{
		___U3CMapU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMapU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_YIsUpAxis_17() { return static_cast<int32_t>(offsetof(CullArea_t635391622, ___YIsUpAxis_17)); }
	inline bool get_YIsUpAxis_17() const { return ___YIsUpAxis_17; }
	inline bool* get_address_of_YIsUpAxis_17() { return &___YIsUpAxis_17; }
	inline void set_YIsUpAxis_17(bool value)
	{
		___YIsUpAxis_17 = value;
	}

	inline static int32_t get_offset_of_RecreateCellHierarchy_18() { return static_cast<int32_t>(offsetof(CullArea_t635391622, ___RecreateCellHierarchy_18)); }
	inline bool get_RecreateCellHierarchy_18() const { return ___RecreateCellHierarchy_18; }
	inline bool* get_address_of_RecreateCellHierarchy_18() { return &___RecreateCellHierarchy_18; }
	inline void set_RecreateCellHierarchy_18(bool value)
	{
		___RecreateCellHierarchy_18 = value;
	}

	inline static int32_t get_offset_of_idCounter_19() { return static_cast<int32_t>(offsetof(CullArea_t635391622, ___idCounter_19)); }
	inline uint8_t get_idCounter_19() const { return ___idCounter_19; }
	inline uint8_t* get_address_of_idCounter_19() { return &___idCounter_19; }
	inline void set_idCounter_19(uint8_t value)
	{
		___idCounter_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULLAREA_T635391622_H
#ifndef CULLINGHANDLER_T4282308608_H
#define CULLINGHANDLER_T4282308608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.CullingHandler
struct  CullingHandler_t4282308608  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 Photon.Pun.UtilityScripts.CullingHandler::orderIndex
	int32_t ___orderIndex_4;
	// Photon.Pun.UtilityScripts.CullArea Photon.Pun.UtilityScripts.CullingHandler::cullArea
	CullArea_t635391622 * ___cullArea_5;
	// System.Collections.Generic.List`1<System.Byte> Photon.Pun.UtilityScripts.CullingHandler::previousActiveCells
	List_1_t2606371118 * ___previousActiveCells_6;
	// System.Collections.Generic.List`1<System.Byte> Photon.Pun.UtilityScripts.CullingHandler::activeCells
	List_1_t2606371118 * ___activeCells_7;
	// Photon.Pun.PhotonView Photon.Pun.UtilityScripts.CullingHandler::pView
	PhotonView_t3684715584 * ___pView_8;
	// UnityEngine.Vector3 Photon.Pun.UtilityScripts.CullingHandler::lastPosition
	Vector3_t3722313464  ___lastPosition_9;
	// UnityEngine.Vector3 Photon.Pun.UtilityScripts.CullingHandler::currentPosition
	Vector3_t3722313464  ___currentPosition_10;

public:
	inline static int32_t get_offset_of_orderIndex_4() { return static_cast<int32_t>(offsetof(CullingHandler_t4282308608, ___orderIndex_4)); }
	inline int32_t get_orderIndex_4() const { return ___orderIndex_4; }
	inline int32_t* get_address_of_orderIndex_4() { return &___orderIndex_4; }
	inline void set_orderIndex_4(int32_t value)
	{
		___orderIndex_4 = value;
	}

	inline static int32_t get_offset_of_cullArea_5() { return static_cast<int32_t>(offsetof(CullingHandler_t4282308608, ___cullArea_5)); }
	inline CullArea_t635391622 * get_cullArea_5() const { return ___cullArea_5; }
	inline CullArea_t635391622 ** get_address_of_cullArea_5() { return &___cullArea_5; }
	inline void set_cullArea_5(CullArea_t635391622 * value)
	{
		___cullArea_5 = value;
		Il2CppCodeGenWriteBarrier((&___cullArea_5), value);
	}

	inline static int32_t get_offset_of_previousActiveCells_6() { return static_cast<int32_t>(offsetof(CullingHandler_t4282308608, ___previousActiveCells_6)); }
	inline List_1_t2606371118 * get_previousActiveCells_6() const { return ___previousActiveCells_6; }
	inline List_1_t2606371118 ** get_address_of_previousActiveCells_6() { return &___previousActiveCells_6; }
	inline void set_previousActiveCells_6(List_1_t2606371118 * value)
	{
		___previousActiveCells_6 = value;
		Il2CppCodeGenWriteBarrier((&___previousActiveCells_6), value);
	}

	inline static int32_t get_offset_of_activeCells_7() { return static_cast<int32_t>(offsetof(CullingHandler_t4282308608, ___activeCells_7)); }
	inline List_1_t2606371118 * get_activeCells_7() const { return ___activeCells_7; }
	inline List_1_t2606371118 ** get_address_of_activeCells_7() { return &___activeCells_7; }
	inline void set_activeCells_7(List_1_t2606371118 * value)
	{
		___activeCells_7 = value;
		Il2CppCodeGenWriteBarrier((&___activeCells_7), value);
	}

	inline static int32_t get_offset_of_pView_8() { return static_cast<int32_t>(offsetof(CullingHandler_t4282308608, ___pView_8)); }
	inline PhotonView_t3684715584 * get_pView_8() const { return ___pView_8; }
	inline PhotonView_t3684715584 ** get_address_of_pView_8() { return &___pView_8; }
	inline void set_pView_8(PhotonView_t3684715584 * value)
	{
		___pView_8 = value;
		Il2CppCodeGenWriteBarrier((&___pView_8), value);
	}

	inline static int32_t get_offset_of_lastPosition_9() { return static_cast<int32_t>(offsetof(CullingHandler_t4282308608, ___lastPosition_9)); }
	inline Vector3_t3722313464  get_lastPosition_9() const { return ___lastPosition_9; }
	inline Vector3_t3722313464 * get_address_of_lastPosition_9() { return &___lastPosition_9; }
	inline void set_lastPosition_9(Vector3_t3722313464  value)
	{
		___lastPosition_9 = value;
	}

	inline static int32_t get_offset_of_currentPosition_10() { return static_cast<int32_t>(offsetof(CullingHandler_t4282308608, ___currentPosition_10)); }
	inline Vector3_t3722313464  get_currentPosition_10() const { return ___currentPosition_10; }
	inline Vector3_t3722313464 * get_address_of_currentPosition_10() { return &___currentPosition_10; }
	inline void set_currentPosition_10(Vector3_t3722313464  value)
	{
		___currentPosition_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULLINGHANDLER_T4282308608_H
#ifndef EVENTSYSTEMSPAWNER_T2883568726_H
#define EVENTSYSTEMSPAWNER_T2883568726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.EventSystemSpawner
struct  EventSystemSpawner_t2883568726  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEMSPAWNER_T2883568726_H
#ifndef GRAPHICTOGGLEISONTRANSITION_T3135858402_H
#define GRAPHICTOGGLEISONTRANSITION_T3135858402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.GraphicToggleIsOnTransition
struct  GraphicToggleIsOnTransition_t3135858402  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Toggle Photon.Pun.UtilityScripts.GraphicToggleIsOnTransition::toggle
	Toggle_t2735377061 * ___toggle_4;
	// UnityEngine.UI.Graphic Photon.Pun.UtilityScripts.GraphicToggleIsOnTransition::_graphic
	Graphic_t1660335611 * ____graphic_5;
	// UnityEngine.Color Photon.Pun.UtilityScripts.GraphicToggleIsOnTransition::NormalOnColor
	Color_t2555686324  ___NormalOnColor_6;
	// UnityEngine.Color Photon.Pun.UtilityScripts.GraphicToggleIsOnTransition::NormalOffColor
	Color_t2555686324  ___NormalOffColor_7;
	// UnityEngine.Color Photon.Pun.UtilityScripts.GraphicToggleIsOnTransition::HoverOnColor
	Color_t2555686324  ___HoverOnColor_8;
	// UnityEngine.Color Photon.Pun.UtilityScripts.GraphicToggleIsOnTransition::HoverOffColor
	Color_t2555686324  ___HoverOffColor_9;
	// System.Boolean Photon.Pun.UtilityScripts.GraphicToggleIsOnTransition::isHover
	bool ___isHover_10;

public:
	inline static int32_t get_offset_of_toggle_4() { return static_cast<int32_t>(offsetof(GraphicToggleIsOnTransition_t3135858402, ___toggle_4)); }
	inline Toggle_t2735377061 * get_toggle_4() const { return ___toggle_4; }
	inline Toggle_t2735377061 ** get_address_of_toggle_4() { return &___toggle_4; }
	inline void set_toggle_4(Toggle_t2735377061 * value)
	{
		___toggle_4 = value;
		Il2CppCodeGenWriteBarrier((&___toggle_4), value);
	}

	inline static int32_t get_offset_of__graphic_5() { return static_cast<int32_t>(offsetof(GraphicToggleIsOnTransition_t3135858402, ____graphic_5)); }
	inline Graphic_t1660335611 * get__graphic_5() const { return ____graphic_5; }
	inline Graphic_t1660335611 ** get_address_of__graphic_5() { return &____graphic_5; }
	inline void set__graphic_5(Graphic_t1660335611 * value)
	{
		____graphic_5 = value;
		Il2CppCodeGenWriteBarrier((&____graphic_5), value);
	}

	inline static int32_t get_offset_of_NormalOnColor_6() { return static_cast<int32_t>(offsetof(GraphicToggleIsOnTransition_t3135858402, ___NormalOnColor_6)); }
	inline Color_t2555686324  get_NormalOnColor_6() const { return ___NormalOnColor_6; }
	inline Color_t2555686324 * get_address_of_NormalOnColor_6() { return &___NormalOnColor_6; }
	inline void set_NormalOnColor_6(Color_t2555686324  value)
	{
		___NormalOnColor_6 = value;
	}

	inline static int32_t get_offset_of_NormalOffColor_7() { return static_cast<int32_t>(offsetof(GraphicToggleIsOnTransition_t3135858402, ___NormalOffColor_7)); }
	inline Color_t2555686324  get_NormalOffColor_7() const { return ___NormalOffColor_7; }
	inline Color_t2555686324 * get_address_of_NormalOffColor_7() { return &___NormalOffColor_7; }
	inline void set_NormalOffColor_7(Color_t2555686324  value)
	{
		___NormalOffColor_7 = value;
	}

	inline static int32_t get_offset_of_HoverOnColor_8() { return static_cast<int32_t>(offsetof(GraphicToggleIsOnTransition_t3135858402, ___HoverOnColor_8)); }
	inline Color_t2555686324  get_HoverOnColor_8() const { return ___HoverOnColor_8; }
	inline Color_t2555686324 * get_address_of_HoverOnColor_8() { return &___HoverOnColor_8; }
	inline void set_HoverOnColor_8(Color_t2555686324  value)
	{
		___HoverOnColor_8 = value;
	}

	inline static int32_t get_offset_of_HoverOffColor_9() { return static_cast<int32_t>(offsetof(GraphicToggleIsOnTransition_t3135858402, ___HoverOffColor_9)); }
	inline Color_t2555686324  get_HoverOffColor_9() const { return ___HoverOffColor_9; }
	inline Color_t2555686324 * get_address_of_HoverOffColor_9() { return &___HoverOffColor_9; }
	inline void set_HoverOffColor_9(Color_t2555686324  value)
	{
		___HoverOffColor_9 = value;
	}

	inline static int32_t get_offset_of_isHover_10() { return static_cast<int32_t>(offsetof(GraphicToggleIsOnTransition_t3135858402, ___isHover_10)); }
	inline bool get_isHover_10() const { return ___isHover_10; }
	inline bool* get_address_of_isHover_10() { return &___isHover_10; }
	inline void set_isHover_10(bool value)
	{
		___isHover_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHICTOGGLEISONTRANSITION_T3135858402_H
#ifndef ONCLICKINSTANTIATE_T3820097070_H
#define ONCLICKINSTANTIATE_T3820097070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.OnClickInstantiate
struct  OnClickInstantiate_t3820097070  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.EventSystems.PointerEventData/InputButton Photon.Pun.UtilityScripts.OnClickInstantiate::Button
	int32_t ___Button_4;
	// UnityEngine.KeyCode Photon.Pun.UtilityScripts.OnClickInstantiate::ModifierKey
	int32_t ___ModifierKey_5;
	// UnityEngine.GameObject Photon.Pun.UtilityScripts.OnClickInstantiate::Prefab
	GameObject_t1113636619 * ___Prefab_6;
	// Photon.Pun.UtilityScripts.OnClickInstantiate/InstantiateOption Photon.Pun.UtilityScripts.OnClickInstantiate::InstantiateType
	int32_t ___InstantiateType_7;

public:
	inline static int32_t get_offset_of_Button_4() { return static_cast<int32_t>(offsetof(OnClickInstantiate_t3820097070, ___Button_4)); }
	inline int32_t get_Button_4() const { return ___Button_4; }
	inline int32_t* get_address_of_Button_4() { return &___Button_4; }
	inline void set_Button_4(int32_t value)
	{
		___Button_4 = value;
	}

	inline static int32_t get_offset_of_ModifierKey_5() { return static_cast<int32_t>(offsetof(OnClickInstantiate_t3820097070, ___ModifierKey_5)); }
	inline int32_t get_ModifierKey_5() const { return ___ModifierKey_5; }
	inline int32_t* get_address_of_ModifierKey_5() { return &___ModifierKey_5; }
	inline void set_ModifierKey_5(int32_t value)
	{
		___ModifierKey_5 = value;
	}

	inline static int32_t get_offset_of_Prefab_6() { return static_cast<int32_t>(offsetof(OnClickInstantiate_t3820097070, ___Prefab_6)); }
	inline GameObject_t1113636619 * get_Prefab_6() const { return ___Prefab_6; }
	inline GameObject_t1113636619 ** get_address_of_Prefab_6() { return &___Prefab_6; }
	inline void set_Prefab_6(GameObject_t1113636619 * value)
	{
		___Prefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_6), value);
	}

	inline static int32_t get_offset_of_InstantiateType_7() { return static_cast<int32_t>(offsetof(OnClickInstantiate_t3820097070, ___InstantiateType_7)); }
	inline int32_t get_InstantiateType_7() const { return ___InstantiateType_7; }
	inline int32_t* get_address_of_InstantiateType_7() { return &___InstantiateType_7; }
	inline void set_InstantiateType_7(int32_t value)
	{
		___InstantiateType_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLICKINSTANTIATE_T3820097070_H
#ifndef ONESCAPEQUIT_T589738653_H
#define ONESCAPEQUIT_T589738653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.OnEscapeQuit
struct  OnEscapeQuit_t589738653  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONESCAPEQUIT_T589738653_H
#ifndef ONJOINEDINSTANTIATE_T361656573_H
#define ONJOINEDINSTANTIATE_T361656573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.OnJoinedInstantiate
struct  OnJoinedInstantiate_t361656573  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform Photon.Pun.UtilityScripts.OnJoinedInstantiate::SpawnPosition
	Transform_t3600365921 * ___SpawnPosition_4;
	// System.Single Photon.Pun.UtilityScripts.OnJoinedInstantiate::PositionOffset
	float ___PositionOffset_5;
	// UnityEngine.GameObject[] Photon.Pun.UtilityScripts.OnJoinedInstantiate::PrefabsToInstantiate
	GameObjectU5BU5D_t3328599146* ___PrefabsToInstantiate_6;

public:
	inline static int32_t get_offset_of_SpawnPosition_4() { return static_cast<int32_t>(offsetof(OnJoinedInstantiate_t361656573, ___SpawnPosition_4)); }
	inline Transform_t3600365921 * get_SpawnPosition_4() const { return ___SpawnPosition_4; }
	inline Transform_t3600365921 ** get_address_of_SpawnPosition_4() { return &___SpawnPosition_4; }
	inline void set_SpawnPosition_4(Transform_t3600365921 * value)
	{
		___SpawnPosition_4 = value;
		Il2CppCodeGenWriteBarrier((&___SpawnPosition_4), value);
	}

	inline static int32_t get_offset_of_PositionOffset_5() { return static_cast<int32_t>(offsetof(OnJoinedInstantiate_t361656573, ___PositionOffset_5)); }
	inline float get_PositionOffset_5() const { return ___PositionOffset_5; }
	inline float* get_address_of_PositionOffset_5() { return &___PositionOffset_5; }
	inline void set_PositionOffset_5(float value)
	{
		___PositionOffset_5 = value;
	}

	inline static int32_t get_offset_of_PrefabsToInstantiate_6() { return static_cast<int32_t>(offsetof(OnJoinedInstantiate_t361656573, ___PrefabsToInstantiate_6)); }
	inline GameObjectU5BU5D_t3328599146* get_PrefabsToInstantiate_6() const { return ___PrefabsToInstantiate_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_PrefabsToInstantiate_6() { return &___PrefabsToInstantiate_6; }
	inline void set_PrefabsToInstantiate_6(GameObjectU5BU5D_t3328599146* value)
	{
		___PrefabsToInstantiate_6 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabsToInstantiate_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONJOINEDINSTANTIATE_T361656573_H
#ifndef ONPOINTEROVERTOOLTIP_T3690126031_H
#define ONPOINTEROVERTOOLTIP_T3690126031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.OnPointerOverTooltip
struct  OnPointerOverTooltip_t3690126031  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPOINTEROVERTOOLTIP_T3690126031_H
#ifndef ONSTARTDELETE_T2541883597_H
#define ONSTARTDELETE_T2541883597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.OnStartDelete
struct  OnStartDelete_t2541883597  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSTARTDELETE_T2541883597_H
#ifndef PHOTONLAGSIMULATIONGUI_T145554164_H
#define PHOTONLAGSIMULATIONGUI_T145554164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.PhotonLagSimulationGui
struct  PhotonLagSimulationGui_t145554164  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rect Photon.Pun.UtilityScripts.PhotonLagSimulationGui::WindowRect
	Rect_t2360479859  ___WindowRect_4;
	// System.Int32 Photon.Pun.UtilityScripts.PhotonLagSimulationGui::WindowId
	int32_t ___WindowId_5;
	// System.Boolean Photon.Pun.UtilityScripts.PhotonLagSimulationGui::Visible
	bool ___Visible_6;
	// ExitGames.Client.Photon.PhotonPeer Photon.Pun.UtilityScripts.PhotonLagSimulationGui::<Peer>k__BackingField
	PhotonPeer_t1608153861 * ___U3CPeerU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_WindowRect_4() { return static_cast<int32_t>(offsetof(PhotonLagSimulationGui_t145554164, ___WindowRect_4)); }
	inline Rect_t2360479859  get_WindowRect_4() const { return ___WindowRect_4; }
	inline Rect_t2360479859 * get_address_of_WindowRect_4() { return &___WindowRect_4; }
	inline void set_WindowRect_4(Rect_t2360479859  value)
	{
		___WindowRect_4 = value;
	}

	inline static int32_t get_offset_of_WindowId_5() { return static_cast<int32_t>(offsetof(PhotonLagSimulationGui_t145554164, ___WindowId_5)); }
	inline int32_t get_WindowId_5() const { return ___WindowId_5; }
	inline int32_t* get_address_of_WindowId_5() { return &___WindowId_5; }
	inline void set_WindowId_5(int32_t value)
	{
		___WindowId_5 = value;
	}

	inline static int32_t get_offset_of_Visible_6() { return static_cast<int32_t>(offsetof(PhotonLagSimulationGui_t145554164, ___Visible_6)); }
	inline bool get_Visible_6() const { return ___Visible_6; }
	inline bool* get_address_of_Visible_6() { return &___Visible_6; }
	inline void set_Visible_6(bool value)
	{
		___Visible_6 = value;
	}

	inline static int32_t get_offset_of_U3CPeerU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PhotonLagSimulationGui_t145554164, ___U3CPeerU3Ek__BackingField_7)); }
	inline PhotonPeer_t1608153861 * get_U3CPeerU3Ek__BackingField_7() const { return ___U3CPeerU3Ek__BackingField_7; }
	inline PhotonPeer_t1608153861 ** get_address_of_U3CPeerU3Ek__BackingField_7() { return &___U3CPeerU3Ek__BackingField_7; }
	inline void set_U3CPeerU3Ek__BackingField_7(PhotonPeer_t1608153861 * value)
	{
		___U3CPeerU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPeerU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONLAGSIMULATIONGUI_T145554164_H
#ifndef PHOTONSTATSGUI_T2125249956_H
#define PHOTONSTATSGUI_T2125249956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.PhotonStatsGui
struct  PhotonStatsGui_t2125249956  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Photon.Pun.UtilityScripts.PhotonStatsGui::statsWindowOn
	bool ___statsWindowOn_4;
	// System.Boolean Photon.Pun.UtilityScripts.PhotonStatsGui::statsOn
	bool ___statsOn_5;
	// System.Boolean Photon.Pun.UtilityScripts.PhotonStatsGui::healthStatsVisible
	bool ___healthStatsVisible_6;
	// System.Boolean Photon.Pun.UtilityScripts.PhotonStatsGui::trafficStatsOn
	bool ___trafficStatsOn_7;
	// System.Boolean Photon.Pun.UtilityScripts.PhotonStatsGui::buttonsOn
	bool ___buttonsOn_8;
	// UnityEngine.Rect Photon.Pun.UtilityScripts.PhotonStatsGui::statsRect
	Rect_t2360479859  ___statsRect_9;
	// System.Int32 Photon.Pun.UtilityScripts.PhotonStatsGui::WindowId
	int32_t ___WindowId_10;

public:
	inline static int32_t get_offset_of_statsWindowOn_4() { return static_cast<int32_t>(offsetof(PhotonStatsGui_t2125249956, ___statsWindowOn_4)); }
	inline bool get_statsWindowOn_4() const { return ___statsWindowOn_4; }
	inline bool* get_address_of_statsWindowOn_4() { return &___statsWindowOn_4; }
	inline void set_statsWindowOn_4(bool value)
	{
		___statsWindowOn_4 = value;
	}

	inline static int32_t get_offset_of_statsOn_5() { return static_cast<int32_t>(offsetof(PhotonStatsGui_t2125249956, ___statsOn_5)); }
	inline bool get_statsOn_5() const { return ___statsOn_5; }
	inline bool* get_address_of_statsOn_5() { return &___statsOn_5; }
	inline void set_statsOn_5(bool value)
	{
		___statsOn_5 = value;
	}

	inline static int32_t get_offset_of_healthStatsVisible_6() { return static_cast<int32_t>(offsetof(PhotonStatsGui_t2125249956, ___healthStatsVisible_6)); }
	inline bool get_healthStatsVisible_6() const { return ___healthStatsVisible_6; }
	inline bool* get_address_of_healthStatsVisible_6() { return &___healthStatsVisible_6; }
	inline void set_healthStatsVisible_6(bool value)
	{
		___healthStatsVisible_6 = value;
	}

	inline static int32_t get_offset_of_trafficStatsOn_7() { return static_cast<int32_t>(offsetof(PhotonStatsGui_t2125249956, ___trafficStatsOn_7)); }
	inline bool get_trafficStatsOn_7() const { return ___trafficStatsOn_7; }
	inline bool* get_address_of_trafficStatsOn_7() { return &___trafficStatsOn_7; }
	inline void set_trafficStatsOn_7(bool value)
	{
		___trafficStatsOn_7 = value;
	}

	inline static int32_t get_offset_of_buttonsOn_8() { return static_cast<int32_t>(offsetof(PhotonStatsGui_t2125249956, ___buttonsOn_8)); }
	inline bool get_buttonsOn_8() const { return ___buttonsOn_8; }
	inline bool* get_address_of_buttonsOn_8() { return &___buttonsOn_8; }
	inline void set_buttonsOn_8(bool value)
	{
		___buttonsOn_8 = value;
	}

	inline static int32_t get_offset_of_statsRect_9() { return static_cast<int32_t>(offsetof(PhotonStatsGui_t2125249956, ___statsRect_9)); }
	inline Rect_t2360479859  get_statsRect_9() const { return ___statsRect_9; }
	inline Rect_t2360479859 * get_address_of_statsRect_9() { return &___statsRect_9; }
	inline void set_statsRect_9(Rect_t2360479859  value)
	{
		___statsRect_9 = value;
	}

	inline static int32_t get_offset_of_WindowId_10() { return static_cast<int32_t>(offsetof(PhotonStatsGui_t2125249956, ___WindowId_10)); }
	inline int32_t get_WindowId_10() const { return ___WindowId_10; }
	inline int32_t* get_address_of_WindowId_10() { return &___WindowId_10; }
	inline void set_WindowId_10(int32_t value)
	{
		___WindowId_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTONSTATSGUI_T2125249956_H
#ifndef POINTEDATGAMEOBJECTINFO_T425461813_H
#define POINTEDATGAMEOBJECTINFO_T425461813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.PointedAtGameObjectInfo
struct  PointedAtGameObjectInfo_t425461813  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text Photon.Pun.UtilityScripts.PointedAtGameObjectInfo::text
	Text_t1901882714 * ___text_5;
	// UnityEngine.Transform Photon.Pun.UtilityScripts.PointedAtGameObjectInfo::focus
	Transform_t3600365921 * ___focus_6;

public:
	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(PointedAtGameObjectInfo_t425461813, ___text_5)); }
	inline Text_t1901882714 * get_text_5() const { return ___text_5; }
	inline Text_t1901882714 ** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(Text_t1901882714 * value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier((&___text_5), value);
	}

	inline static int32_t get_offset_of_focus_6() { return static_cast<int32_t>(offsetof(PointedAtGameObjectInfo_t425461813, ___focus_6)); }
	inline Transform_t3600365921 * get_focus_6() const { return ___focus_6; }
	inline Transform_t3600365921 ** get_address_of_focus_6() { return &___focus_6; }
	inline void set_focus_6(Transform_t3600365921 * value)
	{
		___focus_6 = value;
		Il2CppCodeGenWriteBarrier((&___focus_6), value);
	}
};

struct PointedAtGameObjectInfo_t425461813_StaticFields
{
public:
	// Photon.Pun.UtilityScripts.PointedAtGameObjectInfo Photon.Pun.UtilityScripts.PointedAtGameObjectInfo::Instance
	PointedAtGameObjectInfo_t425461813 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(PointedAtGameObjectInfo_t425461813_StaticFields, ___Instance_4)); }
	inline PointedAtGameObjectInfo_t425461813 * get_Instance_4() const { return ___Instance_4; }
	inline PointedAtGameObjectInfo_t425461813 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(PointedAtGameObjectInfo_t425461813 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEDATGAMEOBJECTINFO_T425461813_H
#ifndef PUNPLAYERSCORES_T3603890024_H
#define PUNPLAYERSCORES_T3603890024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.PunPlayerScores
struct  PunPlayerScores_t3603890024  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNPLAYERSCORES_T3603890024_H
#ifndef STATESGUI_T4032328020_H
#define STATESGUI_T4032328020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.StatesGui
struct  StatesGui_t4032328020  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rect Photon.Pun.UtilityScripts.StatesGui::GuiOffset
	Rect_t2360479859  ___GuiOffset_4;
	// System.Boolean Photon.Pun.UtilityScripts.StatesGui::DontDestroy
	bool ___DontDestroy_5;
	// System.Boolean Photon.Pun.UtilityScripts.StatesGui::ServerTimestamp
	bool ___ServerTimestamp_6;
	// System.Boolean Photon.Pun.UtilityScripts.StatesGui::DetailedConnection
	bool ___DetailedConnection_7;
	// System.Boolean Photon.Pun.UtilityScripts.StatesGui::Server
	bool ___Server_8;
	// System.Boolean Photon.Pun.UtilityScripts.StatesGui::AppVersion
	bool ___AppVersion_9;
	// System.Boolean Photon.Pun.UtilityScripts.StatesGui::UserId
	bool ___UserId_10;
	// System.Boolean Photon.Pun.UtilityScripts.StatesGui::Room
	bool ___Room_11;
	// System.Boolean Photon.Pun.UtilityScripts.StatesGui::RoomProps
	bool ___RoomProps_12;
	// System.Boolean Photon.Pun.UtilityScripts.StatesGui::LocalPlayer
	bool ___LocalPlayer_13;
	// System.Boolean Photon.Pun.UtilityScripts.StatesGui::PlayerProps
	bool ___PlayerProps_14;
	// System.Boolean Photon.Pun.UtilityScripts.StatesGui::Others
	bool ___Others_15;
	// System.Boolean Photon.Pun.UtilityScripts.StatesGui::Buttons
	bool ___Buttons_16;
	// System.Boolean Photon.Pun.UtilityScripts.StatesGui::ExpectedUsers
	bool ___ExpectedUsers_17;
	// UnityEngine.Rect Photon.Pun.UtilityScripts.StatesGui::GuiRect
	Rect_t2360479859  ___GuiRect_18;

public:
	inline static int32_t get_offset_of_GuiOffset_4() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020, ___GuiOffset_4)); }
	inline Rect_t2360479859  get_GuiOffset_4() const { return ___GuiOffset_4; }
	inline Rect_t2360479859 * get_address_of_GuiOffset_4() { return &___GuiOffset_4; }
	inline void set_GuiOffset_4(Rect_t2360479859  value)
	{
		___GuiOffset_4 = value;
	}

	inline static int32_t get_offset_of_DontDestroy_5() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020, ___DontDestroy_5)); }
	inline bool get_DontDestroy_5() const { return ___DontDestroy_5; }
	inline bool* get_address_of_DontDestroy_5() { return &___DontDestroy_5; }
	inline void set_DontDestroy_5(bool value)
	{
		___DontDestroy_5 = value;
	}

	inline static int32_t get_offset_of_ServerTimestamp_6() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020, ___ServerTimestamp_6)); }
	inline bool get_ServerTimestamp_6() const { return ___ServerTimestamp_6; }
	inline bool* get_address_of_ServerTimestamp_6() { return &___ServerTimestamp_6; }
	inline void set_ServerTimestamp_6(bool value)
	{
		___ServerTimestamp_6 = value;
	}

	inline static int32_t get_offset_of_DetailedConnection_7() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020, ___DetailedConnection_7)); }
	inline bool get_DetailedConnection_7() const { return ___DetailedConnection_7; }
	inline bool* get_address_of_DetailedConnection_7() { return &___DetailedConnection_7; }
	inline void set_DetailedConnection_7(bool value)
	{
		___DetailedConnection_7 = value;
	}

	inline static int32_t get_offset_of_Server_8() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020, ___Server_8)); }
	inline bool get_Server_8() const { return ___Server_8; }
	inline bool* get_address_of_Server_8() { return &___Server_8; }
	inline void set_Server_8(bool value)
	{
		___Server_8 = value;
	}

	inline static int32_t get_offset_of_AppVersion_9() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020, ___AppVersion_9)); }
	inline bool get_AppVersion_9() const { return ___AppVersion_9; }
	inline bool* get_address_of_AppVersion_9() { return &___AppVersion_9; }
	inline void set_AppVersion_9(bool value)
	{
		___AppVersion_9 = value;
	}

	inline static int32_t get_offset_of_UserId_10() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020, ___UserId_10)); }
	inline bool get_UserId_10() const { return ___UserId_10; }
	inline bool* get_address_of_UserId_10() { return &___UserId_10; }
	inline void set_UserId_10(bool value)
	{
		___UserId_10 = value;
	}

	inline static int32_t get_offset_of_Room_11() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020, ___Room_11)); }
	inline bool get_Room_11() const { return ___Room_11; }
	inline bool* get_address_of_Room_11() { return &___Room_11; }
	inline void set_Room_11(bool value)
	{
		___Room_11 = value;
	}

	inline static int32_t get_offset_of_RoomProps_12() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020, ___RoomProps_12)); }
	inline bool get_RoomProps_12() const { return ___RoomProps_12; }
	inline bool* get_address_of_RoomProps_12() { return &___RoomProps_12; }
	inline void set_RoomProps_12(bool value)
	{
		___RoomProps_12 = value;
	}

	inline static int32_t get_offset_of_LocalPlayer_13() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020, ___LocalPlayer_13)); }
	inline bool get_LocalPlayer_13() const { return ___LocalPlayer_13; }
	inline bool* get_address_of_LocalPlayer_13() { return &___LocalPlayer_13; }
	inline void set_LocalPlayer_13(bool value)
	{
		___LocalPlayer_13 = value;
	}

	inline static int32_t get_offset_of_PlayerProps_14() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020, ___PlayerProps_14)); }
	inline bool get_PlayerProps_14() const { return ___PlayerProps_14; }
	inline bool* get_address_of_PlayerProps_14() { return &___PlayerProps_14; }
	inline void set_PlayerProps_14(bool value)
	{
		___PlayerProps_14 = value;
	}

	inline static int32_t get_offset_of_Others_15() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020, ___Others_15)); }
	inline bool get_Others_15() const { return ___Others_15; }
	inline bool* get_address_of_Others_15() { return &___Others_15; }
	inline void set_Others_15(bool value)
	{
		___Others_15 = value;
	}

	inline static int32_t get_offset_of_Buttons_16() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020, ___Buttons_16)); }
	inline bool get_Buttons_16() const { return ___Buttons_16; }
	inline bool* get_address_of_Buttons_16() { return &___Buttons_16; }
	inline void set_Buttons_16(bool value)
	{
		___Buttons_16 = value;
	}

	inline static int32_t get_offset_of_ExpectedUsers_17() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020, ___ExpectedUsers_17)); }
	inline bool get_ExpectedUsers_17() const { return ___ExpectedUsers_17; }
	inline bool* get_address_of_ExpectedUsers_17() { return &___ExpectedUsers_17; }
	inline void set_ExpectedUsers_17(bool value)
	{
		___ExpectedUsers_17 = value;
	}

	inline static int32_t get_offset_of_GuiRect_18() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020, ___GuiRect_18)); }
	inline Rect_t2360479859  get_GuiRect_18() const { return ___GuiRect_18; }
	inline Rect_t2360479859 * get_address_of_GuiRect_18() { return &___GuiRect_18; }
	inline void set_GuiRect_18(Rect_t2360479859  value)
	{
		___GuiRect_18 = value;
	}
};

struct StatesGui_t4032328020_StaticFields
{
public:
	// Photon.Pun.UtilityScripts.StatesGui Photon.Pun.UtilityScripts.StatesGui::Instance
	StatesGui_t4032328020 * ___Instance_19;

public:
	inline static int32_t get_offset_of_Instance_19() { return static_cast<int32_t>(offsetof(StatesGui_t4032328020_StaticFields, ___Instance_19)); }
	inline StatesGui_t4032328020 * get_Instance_19() const { return ___Instance_19; }
	inline StatesGui_t4032328020 ** get_address_of_Instance_19() { return &___Instance_19; }
	inline void set_Instance_19(StatesGui_t4032328020 * value)
	{
		___Instance_19 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATESGUI_T4032328020_H
#ifndef TABVIEWMANAGER_T3686055887_H
#define TABVIEWMANAGER_T3686055887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.TabViewManager
struct  TabViewManager_t3686055887  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.ToggleGroup Photon.Pun.UtilityScripts.TabViewManager::ToggleGroup
	ToggleGroup_t123837990 * ___ToggleGroup_4;
	// Photon.Pun.UtilityScripts.TabViewManager/Tab[] Photon.Pun.UtilityScripts.TabViewManager::Tabs
	TabU5BU5D_t533311896* ___Tabs_5;
	// Photon.Pun.UtilityScripts.TabViewManager/TabChangeEvent Photon.Pun.UtilityScripts.TabViewManager::OnTabChanged
	TabChangeEvent_t3080003849 * ___OnTabChanged_6;
	// Photon.Pun.UtilityScripts.TabViewManager/Tab Photon.Pun.UtilityScripts.TabViewManager::CurrentTab
	Tab_t117203701 * ___CurrentTab_7;
	// System.Collections.Generic.Dictionary`2<UnityEngine.UI.Toggle,Photon.Pun.UtilityScripts.TabViewManager/Tab> Photon.Pun.UtilityScripts.TabViewManager::Tab_lut
	Dictionary_2_t1152131052 * ___Tab_lut_8;

public:
	inline static int32_t get_offset_of_ToggleGroup_4() { return static_cast<int32_t>(offsetof(TabViewManager_t3686055887, ___ToggleGroup_4)); }
	inline ToggleGroup_t123837990 * get_ToggleGroup_4() const { return ___ToggleGroup_4; }
	inline ToggleGroup_t123837990 ** get_address_of_ToggleGroup_4() { return &___ToggleGroup_4; }
	inline void set_ToggleGroup_4(ToggleGroup_t123837990 * value)
	{
		___ToggleGroup_4 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleGroup_4), value);
	}

	inline static int32_t get_offset_of_Tabs_5() { return static_cast<int32_t>(offsetof(TabViewManager_t3686055887, ___Tabs_5)); }
	inline TabU5BU5D_t533311896* get_Tabs_5() const { return ___Tabs_5; }
	inline TabU5BU5D_t533311896** get_address_of_Tabs_5() { return &___Tabs_5; }
	inline void set_Tabs_5(TabU5BU5D_t533311896* value)
	{
		___Tabs_5 = value;
		Il2CppCodeGenWriteBarrier((&___Tabs_5), value);
	}

	inline static int32_t get_offset_of_OnTabChanged_6() { return static_cast<int32_t>(offsetof(TabViewManager_t3686055887, ___OnTabChanged_6)); }
	inline TabChangeEvent_t3080003849 * get_OnTabChanged_6() const { return ___OnTabChanged_6; }
	inline TabChangeEvent_t3080003849 ** get_address_of_OnTabChanged_6() { return &___OnTabChanged_6; }
	inline void set_OnTabChanged_6(TabChangeEvent_t3080003849 * value)
	{
		___OnTabChanged_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnTabChanged_6), value);
	}

	inline static int32_t get_offset_of_CurrentTab_7() { return static_cast<int32_t>(offsetof(TabViewManager_t3686055887, ___CurrentTab_7)); }
	inline Tab_t117203701 * get_CurrentTab_7() const { return ___CurrentTab_7; }
	inline Tab_t117203701 ** get_address_of_CurrentTab_7() { return &___CurrentTab_7; }
	inline void set_CurrentTab_7(Tab_t117203701 * value)
	{
		___CurrentTab_7 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentTab_7), value);
	}

	inline static int32_t get_offset_of_Tab_lut_8() { return static_cast<int32_t>(offsetof(TabViewManager_t3686055887, ___Tab_lut_8)); }
	inline Dictionary_2_t1152131052 * get_Tab_lut_8() const { return ___Tab_lut_8; }
	inline Dictionary_2_t1152131052 ** get_address_of_Tab_lut_8() { return &___Tab_lut_8; }
	inline void set_Tab_lut_8(Dictionary_2_t1152131052 * value)
	{
		___Tab_lut_8 = value;
		Il2CppCodeGenWriteBarrier((&___Tab_lut_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABVIEWMANAGER_T3686055887_H
#ifndef TEXTBUTTONTRANSITION_T2307109358_H
#define TEXTBUTTONTRANSITION_T2307109358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.TextButtonTransition
struct  TextButtonTransition_t2307109358  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text Photon.Pun.UtilityScripts.TextButtonTransition::_text
	Text_t1901882714 * ____text_4;
	// UnityEngine.UI.Selectable Photon.Pun.UtilityScripts.TextButtonTransition::Selectable
	Selectable_t3250028441 * ___Selectable_5;
	// UnityEngine.Color Photon.Pun.UtilityScripts.TextButtonTransition::NormalColor
	Color_t2555686324  ___NormalColor_6;
	// UnityEngine.Color Photon.Pun.UtilityScripts.TextButtonTransition::HoverColor
	Color_t2555686324  ___HoverColor_7;

public:
	inline static int32_t get_offset_of__text_4() { return static_cast<int32_t>(offsetof(TextButtonTransition_t2307109358, ____text_4)); }
	inline Text_t1901882714 * get__text_4() const { return ____text_4; }
	inline Text_t1901882714 ** get_address_of__text_4() { return &____text_4; }
	inline void set__text_4(Text_t1901882714 * value)
	{
		____text_4 = value;
		Il2CppCodeGenWriteBarrier((&____text_4), value);
	}

	inline static int32_t get_offset_of_Selectable_5() { return static_cast<int32_t>(offsetof(TextButtonTransition_t2307109358, ___Selectable_5)); }
	inline Selectable_t3250028441 * get_Selectable_5() const { return ___Selectable_5; }
	inline Selectable_t3250028441 ** get_address_of_Selectable_5() { return &___Selectable_5; }
	inline void set_Selectable_5(Selectable_t3250028441 * value)
	{
		___Selectable_5 = value;
		Il2CppCodeGenWriteBarrier((&___Selectable_5), value);
	}

	inline static int32_t get_offset_of_NormalColor_6() { return static_cast<int32_t>(offsetof(TextButtonTransition_t2307109358, ___NormalColor_6)); }
	inline Color_t2555686324  get_NormalColor_6() const { return ___NormalColor_6; }
	inline Color_t2555686324 * get_address_of_NormalColor_6() { return &___NormalColor_6; }
	inline void set_NormalColor_6(Color_t2555686324  value)
	{
		___NormalColor_6 = value;
	}

	inline static int32_t get_offset_of_HoverColor_7() { return static_cast<int32_t>(offsetof(TextButtonTransition_t2307109358, ___HoverColor_7)); }
	inline Color_t2555686324  get_HoverColor_7() const { return ___HoverColor_7; }
	inline Color_t2555686324 * get_address_of_HoverColor_7() { return &___HoverColor_7; }
	inline void set_HoverColor_7(Color_t2555686324  value)
	{
		___HoverColor_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTBUTTONTRANSITION_T2307109358_H
#ifndef TEXTTOGGLEISONTRANSITION_T4243955300_H
#define TEXTTOGGLEISONTRANSITION_T4243955300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.TextToggleIsOnTransition
struct  TextToggleIsOnTransition_t4243955300  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Toggle Photon.Pun.UtilityScripts.TextToggleIsOnTransition::toggle
	Toggle_t2735377061 * ___toggle_4;
	// UnityEngine.UI.Text Photon.Pun.UtilityScripts.TextToggleIsOnTransition::_text
	Text_t1901882714 * ____text_5;
	// UnityEngine.Color Photon.Pun.UtilityScripts.TextToggleIsOnTransition::NormalOnColor
	Color_t2555686324  ___NormalOnColor_6;
	// UnityEngine.Color Photon.Pun.UtilityScripts.TextToggleIsOnTransition::NormalOffColor
	Color_t2555686324  ___NormalOffColor_7;
	// UnityEngine.Color Photon.Pun.UtilityScripts.TextToggleIsOnTransition::HoverOnColor
	Color_t2555686324  ___HoverOnColor_8;
	// UnityEngine.Color Photon.Pun.UtilityScripts.TextToggleIsOnTransition::HoverOffColor
	Color_t2555686324  ___HoverOffColor_9;
	// System.Boolean Photon.Pun.UtilityScripts.TextToggleIsOnTransition::isHover
	bool ___isHover_10;

public:
	inline static int32_t get_offset_of_toggle_4() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t4243955300, ___toggle_4)); }
	inline Toggle_t2735377061 * get_toggle_4() const { return ___toggle_4; }
	inline Toggle_t2735377061 ** get_address_of_toggle_4() { return &___toggle_4; }
	inline void set_toggle_4(Toggle_t2735377061 * value)
	{
		___toggle_4 = value;
		Il2CppCodeGenWriteBarrier((&___toggle_4), value);
	}

	inline static int32_t get_offset_of__text_5() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t4243955300, ____text_5)); }
	inline Text_t1901882714 * get__text_5() const { return ____text_5; }
	inline Text_t1901882714 ** get_address_of__text_5() { return &____text_5; }
	inline void set__text_5(Text_t1901882714 * value)
	{
		____text_5 = value;
		Il2CppCodeGenWriteBarrier((&____text_5), value);
	}

	inline static int32_t get_offset_of_NormalOnColor_6() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t4243955300, ___NormalOnColor_6)); }
	inline Color_t2555686324  get_NormalOnColor_6() const { return ___NormalOnColor_6; }
	inline Color_t2555686324 * get_address_of_NormalOnColor_6() { return &___NormalOnColor_6; }
	inline void set_NormalOnColor_6(Color_t2555686324  value)
	{
		___NormalOnColor_6 = value;
	}

	inline static int32_t get_offset_of_NormalOffColor_7() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t4243955300, ___NormalOffColor_7)); }
	inline Color_t2555686324  get_NormalOffColor_7() const { return ___NormalOffColor_7; }
	inline Color_t2555686324 * get_address_of_NormalOffColor_7() { return &___NormalOffColor_7; }
	inline void set_NormalOffColor_7(Color_t2555686324  value)
	{
		___NormalOffColor_7 = value;
	}

	inline static int32_t get_offset_of_HoverOnColor_8() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t4243955300, ___HoverOnColor_8)); }
	inline Color_t2555686324  get_HoverOnColor_8() const { return ___HoverOnColor_8; }
	inline Color_t2555686324 * get_address_of_HoverOnColor_8() { return &___HoverOnColor_8; }
	inline void set_HoverOnColor_8(Color_t2555686324  value)
	{
		___HoverOnColor_8 = value;
	}

	inline static int32_t get_offset_of_HoverOffColor_9() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t4243955300, ___HoverOffColor_9)); }
	inline Color_t2555686324  get_HoverOffColor_9() const { return ___HoverOffColor_9; }
	inline Color_t2555686324 * get_address_of_HoverOffColor_9() { return &___HoverOffColor_9; }
	inline void set_HoverOffColor_9(Color_t2555686324  value)
	{
		___HoverOffColor_9 = value;
	}

	inline static int32_t get_offset_of_isHover_10() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t4243955300, ___isHover_10)); }
	inline bool get_isHover_10() const { return ___isHover_10; }
	inline bool* get_address_of_isHover_10() { return &___isHover_10; }
	inline void set_isHover_10(bool value)
	{
		___isHover_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTTOGGLEISONTRANSITION_T4243955300_H
#ifndef CONNECTANDJOINRANDOMLB_T3563937443_H
#define CONNECTANDJOINRANDOMLB_T3563937443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Realtime.Demo.ConnectAndJoinRandomLb
struct  ConnectAndJoinRandomLb_t3563937443  : public MonoBehaviour_t3962482529
{
public:
	// System.String Photon.Realtime.Demo.ConnectAndJoinRandomLb::AppId
	String_t* ___AppId_4;
	// Photon.Realtime.LoadBalancingClient Photon.Realtime.Demo.ConnectAndJoinRandomLb::lbc
	LoadBalancingClient_t609581828 * ___lbc_5;
	// Photon.Realtime.ConnectionHandler Photon.Realtime.Demo.ConnectAndJoinRandomLb::ch
	ConnectionHandler_t3678426725 * ___ch_6;
	// UnityEngine.UI.Text Photon.Realtime.Demo.ConnectAndJoinRandomLb::StateUiText
	Text_t1901882714 * ___StateUiText_7;

public:
	inline static int32_t get_offset_of_AppId_4() { return static_cast<int32_t>(offsetof(ConnectAndJoinRandomLb_t3563937443, ___AppId_4)); }
	inline String_t* get_AppId_4() const { return ___AppId_4; }
	inline String_t** get_address_of_AppId_4() { return &___AppId_4; }
	inline void set_AppId_4(String_t* value)
	{
		___AppId_4 = value;
		Il2CppCodeGenWriteBarrier((&___AppId_4), value);
	}

	inline static int32_t get_offset_of_lbc_5() { return static_cast<int32_t>(offsetof(ConnectAndJoinRandomLb_t3563937443, ___lbc_5)); }
	inline LoadBalancingClient_t609581828 * get_lbc_5() const { return ___lbc_5; }
	inline LoadBalancingClient_t609581828 ** get_address_of_lbc_5() { return &___lbc_5; }
	inline void set_lbc_5(LoadBalancingClient_t609581828 * value)
	{
		___lbc_5 = value;
		Il2CppCodeGenWriteBarrier((&___lbc_5), value);
	}

	inline static int32_t get_offset_of_ch_6() { return static_cast<int32_t>(offsetof(ConnectAndJoinRandomLb_t3563937443, ___ch_6)); }
	inline ConnectionHandler_t3678426725 * get_ch_6() const { return ___ch_6; }
	inline ConnectionHandler_t3678426725 ** get_address_of_ch_6() { return &___ch_6; }
	inline void set_ch_6(ConnectionHandler_t3678426725 * value)
	{
		___ch_6 = value;
		Il2CppCodeGenWriteBarrier((&___ch_6), value);
	}

	inline static int32_t get_offset_of_StateUiText_7() { return static_cast<int32_t>(offsetof(ConnectAndJoinRandomLb_t3563937443, ___StateUiText_7)); }
	inline Text_t1901882714 * get_StateUiText_7() const { return ___StateUiText_7; }
	inline Text_t1901882714 ** get_address_of_StateUiText_7() { return &___StateUiText_7; }
	inline void set_StateUiText_7(Text_t1901882714 * value)
	{
		___StateUiText_7 = value;
		Il2CppCodeGenWriteBarrier((&___StateUiText_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTANDJOINRANDOMLB_T3563937443_H
#ifndef ANALYTICSEVENTTRACKER_T2285229262_H
#define ANALYTICSEVENTTRACKER_T2285229262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker
struct  AnalyticsEventTracker_t2285229262  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Analytics.EventTrigger UnityEngine.Analytics.AnalyticsEventTracker::m_Trigger
	EventTrigger_t2527451695 * ___m_Trigger_4;
	// UnityEngine.Analytics.StandardEventPayload UnityEngine.Analytics.AnalyticsEventTracker::m_EventPayload
	StandardEventPayload_t1629891255 * ___m_EventPayload_5;

public:
	inline static int32_t get_offset_of_m_Trigger_4() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_Trigger_4)); }
	inline EventTrigger_t2527451695 * get_m_Trigger_4() const { return ___m_Trigger_4; }
	inline EventTrigger_t2527451695 ** get_address_of_m_Trigger_4() { return &___m_Trigger_4; }
	inline void set_m_Trigger_4(EventTrigger_t2527451695 * value)
	{
		___m_Trigger_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Trigger_4), value);
	}

	inline static int32_t get_offset_of_m_EventPayload_5() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_EventPayload_5)); }
	inline StandardEventPayload_t1629891255 * get_m_EventPayload_5() const { return ___m_EventPayload_5; }
	inline StandardEventPayload_t1629891255 ** get_address_of_m_EventPayload_5() { return &___m_EventPayload_5; }
	inline void set_m_EventPayload_5(StandardEventPayload_t1629891255 * value)
	{
		___m_EventPayload_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventPayload_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKER_T2285229262_H
#ifndef ANALYTICSTRACKER_T731021378_H
#define ANALYTICSTRACKER_T731021378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker
struct  AnalyticsTracker_t731021378  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityEngine.Analytics.AnalyticsTracker::m_EventName
	String_t* ___m_EventName_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsTracker::m_Dict
	Dictionary_2_t2865362463 * ___m_Dict_5;
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker::m_PrevDictHash
	int32_t ___m_PrevDictHash_6;
	// UnityEngine.Analytics.TrackableProperty UnityEngine.Analytics.AnalyticsTracker::m_TrackableProperty
	TrackableProperty_t3943537984 * ___m_TrackableProperty_7;
	// UnityEngine.Analytics.AnalyticsTracker/Trigger UnityEngine.Analytics.AnalyticsTracker::m_Trigger
	int32_t ___m_Trigger_8;

public:
	inline static int32_t get_offset_of_m_EventName_4() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_EventName_4)); }
	inline String_t* get_m_EventName_4() const { return ___m_EventName_4; }
	inline String_t** get_address_of_m_EventName_4() { return &___m_EventName_4; }
	inline void set_m_EventName_4(String_t* value)
	{
		___m_EventName_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_4), value);
	}

	inline static int32_t get_offset_of_m_Dict_5() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Dict_5)); }
	inline Dictionary_2_t2865362463 * get_m_Dict_5() const { return ___m_Dict_5; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_Dict_5() { return &___m_Dict_5; }
	inline void set_m_Dict_5(Dictionary_2_t2865362463 * value)
	{
		___m_Dict_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_5), value);
	}

	inline static int32_t get_offset_of_m_PrevDictHash_6() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_PrevDictHash_6)); }
	inline int32_t get_m_PrevDictHash_6() const { return ___m_PrevDictHash_6; }
	inline int32_t* get_address_of_m_PrevDictHash_6() { return &___m_PrevDictHash_6; }
	inline void set_m_PrevDictHash_6(int32_t value)
	{
		___m_PrevDictHash_6 = value;
	}

	inline static int32_t get_offset_of_m_TrackableProperty_7() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_TrackableProperty_7)); }
	inline TrackableProperty_t3943537984 * get_m_TrackableProperty_7() const { return ___m_TrackableProperty_7; }
	inline TrackableProperty_t3943537984 ** get_address_of_m_TrackableProperty_7() { return &___m_TrackableProperty_7; }
	inline void set_m_TrackableProperty_7(TrackableProperty_t3943537984 * value)
	{
		___m_TrackableProperty_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableProperty_7), value);
	}

	inline static int32_t get_offset_of_m_Trigger_8() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Trigger_8)); }
	inline int32_t get_m_Trigger_8() const { return ___m_Trigger_8; }
	inline int32_t* get_address_of_m_Trigger_8() { return &___m_Trigger_8; }
	inline void set_m_Trigger_8(int32_t value)
	{
		___m_Trigger_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSTRACKER_T731021378_H
#ifndef MONOBEHAVIOURPUNCALLBACKS_T1810614660_H
#define MONOBEHAVIOURPUNCALLBACKS_T1810614660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.MonoBehaviourPunCallbacks
struct  MonoBehaviourPunCallbacks_t1810614660  : public MonoBehaviourPun_t1682334777
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOURPUNCALLBACKS_T1810614660_H
#ifndef MOVEBYKEYS_T979471368_H
#define MOVEBYKEYS_T979471368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.MoveByKeys
struct  MoveByKeys_t979471368  : public MonoBehaviourPun_t1682334777
{
public:
	// System.Single Photon.Pun.UtilityScripts.MoveByKeys::Speed
	float ___Speed_5;
	// System.Single Photon.Pun.UtilityScripts.MoveByKeys::JumpForce
	float ___JumpForce_6;
	// System.Single Photon.Pun.UtilityScripts.MoveByKeys::JumpTimeout
	float ___JumpTimeout_7;
	// System.Boolean Photon.Pun.UtilityScripts.MoveByKeys::isSprite
	bool ___isSprite_8;
	// System.Single Photon.Pun.UtilityScripts.MoveByKeys::jumpingTime
	float ___jumpingTime_9;
	// UnityEngine.Rigidbody Photon.Pun.UtilityScripts.MoveByKeys::body
	Rigidbody_t3916780224 * ___body_10;
	// UnityEngine.Rigidbody2D Photon.Pun.UtilityScripts.MoveByKeys::body2d
	Rigidbody2D_t939494601 * ___body2d_11;

public:
	inline static int32_t get_offset_of_Speed_5() { return static_cast<int32_t>(offsetof(MoveByKeys_t979471368, ___Speed_5)); }
	inline float get_Speed_5() const { return ___Speed_5; }
	inline float* get_address_of_Speed_5() { return &___Speed_5; }
	inline void set_Speed_5(float value)
	{
		___Speed_5 = value;
	}

	inline static int32_t get_offset_of_JumpForce_6() { return static_cast<int32_t>(offsetof(MoveByKeys_t979471368, ___JumpForce_6)); }
	inline float get_JumpForce_6() const { return ___JumpForce_6; }
	inline float* get_address_of_JumpForce_6() { return &___JumpForce_6; }
	inline void set_JumpForce_6(float value)
	{
		___JumpForce_6 = value;
	}

	inline static int32_t get_offset_of_JumpTimeout_7() { return static_cast<int32_t>(offsetof(MoveByKeys_t979471368, ___JumpTimeout_7)); }
	inline float get_JumpTimeout_7() const { return ___JumpTimeout_7; }
	inline float* get_address_of_JumpTimeout_7() { return &___JumpTimeout_7; }
	inline void set_JumpTimeout_7(float value)
	{
		___JumpTimeout_7 = value;
	}

	inline static int32_t get_offset_of_isSprite_8() { return static_cast<int32_t>(offsetof(MoveByKeys_t979471368, ___isSprite_8)); }
	inline bool get_isSprite_8() const { return ___isSprite_8; }
	inline bool* get_address_of_isSprite_8() { return &___isSprite_8; }
	inline void set_isSprite_8(bool value)
	{
		___isSprite_8 = value;
	}

	inline static int32_t get_offset_of_jumpingTime_9() { return static_cast<int32_t>(offsetof(MoveByKeys_t979471368, ___jumpingTime_9)); }
	inline float get_jumpingTime_9() const { return ___jumpingTime_9; }
	inline float* get_address_of_jumpingTime_9() { return &___jumpingTime_9; }
	inline void set_jumpingTime_9(float value)
	{
		___jumpingTime_9 = value;
	}

	inline static int32_t get_offset_of_body_10() { return static_cast<int32_t>(offsetof(MoveByKeys_t979471368, ___body_10)); }
	inline Rigidbody_t3916780224 * get_body_10() const { return ___body_10; }
	inline Rigidbody_t3916780224 ** get_address_of_body_10() { return &___body_10; }
	inline void set_body_10(Rigidbody_t3916780224 * value)
	{
		___body_10 = value;
		Il2CppCodeGenWriteBarrier((&___body_10), value);
	}

	inline static int32_t get_offset_of_body2d_11() { return static_cast<int32_t>(offsetof(MoveByKeys_t979471368, ___body2d_11)); }
	inline Rigidbody2D_t939494601 * get_body2d_11() const { return ___body2d_11; }
	inline Rigidbody2D_t939494601 ** get_address_of_body2d_11() { return &___body2d_11; }
	inline void set_body2d_11(Rigidbody2D_t939494601 * value)
	{
		___body2d_11 = value;
		Il2CppCodeGenWriteBarrier((&___body2d_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEBYKEYS_T979471368_H
#ifndef ONCLICKDESTROY_T3019334366_H
#define ONCLICKDESTROY_T3019334366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.OnClickDestroy
struct  OnClickDestroy_t3019334366  : public MonoBehaviourPun_t1682334777
{
public:
	// UnityEngine.EventSystems.PointerEventData/InputButton Photon.Pun.UtilityScripts.OnClickDestroy::Button
	int32_t ___Button_5;
	// UnityEngine.KeyCode Photon.Pun.UtilityScripts.OnClickDestroy::ModifierKey
	int32_t ___ModifierKey_6;
	// System.Boolean Photon.Pun.UtilityScripts.OnClickDestroy::DestroyByRpc
	bool ___DestroyByRpc_7;

public:
	inline static int32_t get_offset_of_Button_5() { return static_cast<int32_t>(offsetof(OnClickDestroy_t3019334366, ___Button_5)); }
	inline int32_t get_Button_5() const { return ___Button_5; }
	inline int32_t* get_address_of_Button_5() { return &___Button_5; }
	inline void set_Button_5(int32_t value)
	{
		___Button_5 = value;
	}

	inline static int32_t get_offset_of_ModifierKey_6() { return static_cast<int32_t>(offsetof(OnClickDestroy_t3019334366, ___ModifierKey_6)); }
	inline int32_t get_ModifierKey_6() const { return ___ModifierKey_6; }
	inline int32_t* get_address_of_ModifierKey_6() { return &___ModifierKey_6; }
	inline void set_ModifierKey_6(int32_t value)
	{
		___ModifierKey_6 = value;
	}

	inline static int32_t get_offset_of_DestroyByRpc_7() { return static_cast<int32_t>(offsetof(OnClickDestroy_t3019334366, ___DestroyByRpc_7)); }
	inline bool get_DestroyByRpc_7() const { return ___DestroyByRpc_7; }
	inline bool* get_address_of_DestroyByRpc_7() { return &___DestroyByRpc_7; }
	inline void set_DestroyByRpc_7(bool value)
	{
		___DestroyByRpc_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLICKDESTROY_T3019334366_H
#ifndef ONCLICKRPC_T2528495255_H
#define ONCLICKRPC_T2528495255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.OnClickRpc
struct  OnClickRpc_t2528495255  : public MonoBehaviourPun_t1682334777
{
public:
	// UnityEngine.EventSystems.PointerEventData/InputButton Photon.Pun.UtilityScripts.OnClickRpc::Button
	int32_t ___Button_5;
	// UnityEngine.KeyCode Photon.Pun.UtilityScripts.OnClickRpc::ModifierKey
	int32_t ___ModifierKey_6;
	// Photon.Pun.RpcTarget Photon.Pun.UtilityScripts.OnClickRpc::Target
	int32_t ___Target_7;
	// UnityEngine.Material Photon.Pun.UtilityScripts.OnClickRpc::originalMaterial
	Material_t340375123 * ___originalMaterial_8;
	// UnityEngine.Color Photon.Pun.UtilityScripts.OnClickRpc::originalColor
	Color_t2555686324  ___originalColor_9;
	// System.Boolean Photon.Pun.UtilityScripts.OnClickRpc::isFlashing
	bool ___isFlashing_10;

public:
	inline static int32_t get_offset_of_Button_5() { return static_cast<int32_t>(offsetof(OnClickRpc_t2528495255, ___Button_5)); }
	inline int32_t get_Button_5() const { return ___Button_5; }
	inline int32_t* get_address_of_Button_5() { return &___Button_5; }
	inline void set_Button_5(int32_t value)
	{
		___Button_5 = value;
	}

	inline static int32_t get_offset_of_ModifierKey_6() { return static_cast<int32_t>(offsetof(OnClickRpc_t2528495255, ___ModifierKey_6)); }
	inline int32_t get_ModifierKey_6() const { return ___ModifierKey_6; }
	inline int32_t* get_address_of_ModifierKey_6() { return &___ModifierKey_6; }
	inline void set_ModifierKey_6(int32_t value)
	{
		___ModifierKey_6 = value;
	}

	inline static int32_t get_offset_of_Target_7() { return static_cast<int32_t>(offsetof(OnClickRpc_t2528495255, ___Target_7)); }
	inline int32_t get_Target_7() const { return ___Target_7; }
	inline int32_t* get_address_of_Target_7() { return &___Target_7; }
	inline void set_Target_7(int32_t value)
	{
		___Target_7 = value;
	}

	inline static int32_t get_offset_of_originalMaterial_8() { return static_cast<int32_t>(offsetof(OnClickRpc_t2528495255, ___originalMaterial_8)); }
	inline Material_t340375123 * get_originalMaterial_8() const { return ___originalMaterial_8; }
	inline Material_t340375123 ** get_address_of_originalMaterial_8() { return &___originalMaterial_8; }
	inline void set_originalMaterial_8(Material_t340375123 * value)
	{
		___originalMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___originalMaterial_8), value);
	}

	inline static int32_t get_offset_of_originalColor_9() { return static_cast<int32_t>(offsetof(OnClickRpc_t2528495255, ___originalColor_9)); }
	inline Color_t2555686324  get_originalColor_9() const { return ___originalColor_9; }
	inline Color_t2555686324 * get_address_of_originalColor_9() { return &___originalColor_9; }
	inline void set_originalColor_9(Color_t2555686324  value)
	{
		___originalColor_9 = value;
	}

	inline static int32_t get_offset_of_isFlashing_10() { return static_cast<int32_t>(offsetof(OnClickRpc_t2528495255, ___isFlashing_10)); }
	inline bool get_isFlashing_10() const { return ___isFlashing_10; }
	inline bool* get_address_of_isFlashing_10() { return &___isFlashing_10; }
	inline void set_isFlashing_10(bool value)
	{
		___isFlashing_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLICKRPC_T2528495255_H
#ifndef SMOOTHSYNCMOVEMENT_T663920301_H
#define SMOOTHSYNCMOVEMENT_T663920301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.SmoothSyncMovement
struct  SmoothSyncMovement_t663920301  : public MonoBehaviourPun_t1682334777
{
public:
	// System.Single Photon.Pun.UtilityScripts.SmoothSyncMovement::SmoothingDelay
	float ___SmoothingDelay_5;
	// UnityEngine.Vector3 Photon.Pun.UtilityScripts.SmoothSyncMovement::correctPlayerPos
	Vector3_t3722313464  ___correctPlayerPos_6;
	// UnityEngine.Quaternion Photon.Pun.UtilityScripts.SmoothSyncMovement::correctPlayerRot
	Quaternion_t2301928331  ___correctPlayerRot_7;

public:
	inline static int32_t get_offset_of_SmoothingDelay_5() { return static_cast<int32_t>(offsetof(SmoothSyncMovement_t663920301, ___SmoothingDelay_5)); }
	inline float get_SmoothingDelay_5() const { return ___SmoothingDelay_5; }
	inline float* get_address_of_SmoothingDelay_5() { return &___SmoothingDelay_5; }
	inline void set_SmoothingDelay_5(float value)
	{
		___SmoothingDelay_5 = value;
	}

	inline static int32_t get_offset_of_correctPlayerPos_6() { return static_cast<int32_t>(offsetof(SmoothSyncMovement_t663920301, ___correctPlayerPos_6)); }
	inline Vector3_t3722313464  get_correctPlayerPos_6() const { return ___correctPlayerPos_6; }
	inline Vector3_t3722313464 * get_address_of_correctPlayerPos_6() { return &___correctPlayerPos_6; }
	inline void set_correctPlayerPos_6(Vector3_t3722313464  value)
	{
		___correctPlayerPos_6 = value;
	}

	inline static int32_t get_offset_of_correctPlayerRot_7() { return static_cast<int32_t>(offsetof(SmoothSyncMovement_t663920301, ___correctPlayerRot_7)); }
	inline Quaternion_t2301928331  get_correctPlayerRot_7() const { return ___correctPlayerRot_7; }
	inline Quaternion_t2301928331 * get_address_of_correctPlayerRot_7() { return &___correctPlayerRot_7; }
	inline void set_correctPlayerRot_7(Quaternion_t2301928331  value)
	{
		___correctPlayerRot_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHSYNCMOVEMENT_T663920301_H
#ifndef CONNECTANDJOINRANDOM_T1495527429_H
#define CONNECTANDJOINRANDOM_T1495527429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.ConnectAndJoinRandom
struct  ConnectAndJoinRandom_t1495527429  : public MonoBehaviourPunCallbacks_t1810614660
{
public:
	// System.Boolean Photon.Pun.UtilityScripts.ConnectAndJoinRandom::AutoConnect
	bool ___AutoConnect_5;
	// System.Byte Photon.Pun.UtilityScripts.ConnectAndJoinRandom::Version
	uint8_t ___Version_6;

public:
	inline static int32_t get_offset_of_AutoConnect_5() { return static_cast<int32_t>(offsetof(ConnectAndJoinRandom_t1495527429, ___AutoConnect_5)); }
	inline bool get_AutoConnect_5() const { return ___AutoConnect_5; }
	inline bool* get_address_of_AutoConnect_5() { return &___AutoConnect_5; }
	inline void set_AutoConnect_5(bool value)
	{
		___AutoConnect_5 = value;
	}

	inline static int32_t get_offset_of_Version_6() { return static_cast<int32_t>(offsetof(ConnectAndJoinRandom_t1495527429, ___Version_6)); }
	inline uint8_t get_Version_6() const { return ___Version_6; }
	inline uint8_t* get_address_of_Version_6() { return &___Version_6; }
	inline void set_Version_6(uint8_t value)
	{
		___Version_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTANDJOINRANDOM_T1495527429_H
#ifndef COUNTDOWNTIMER_T636337431_H
#define COUNTDOWNTIMER_T636337431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.CountdownTimer
struct  CountdownTimer_t636337431  : public MonoBehaviourPunCallbacks_t1810614660
{
public:
	// System.Boolean Photon.Pun.UtilityScripts.CountdownTimer::isTimerRunning
	bool ___isTimerRunning_7;
	// System.Single Photon.Pun.UtilityScripts.CountdownTimer::startTime
	float ___startTime_8;
	// UnityEngine.UI.Text Photon.Pun.UtilityScripts.CountdownTimer::Text
	Text_t1901882714 * ___Text_9;
	// System.Single Photon.Pun.UtilityScripts.CountdownTimer::Countdown
	float ___Countdown_10;

public:
	inline static int32_t get_offset_of_isTimerRunning_7() { return static_cast<int32_t>(offsetof(CountdownTimer_t636337431, ___isTimerRunning_7)); }
	inline bool get_isTimerRunning_7() const { return ___isTimerRunning_7; }
	inline bool* get_address_of_isTimerRunning_7() { return &___isTimerRunning_7; }
	inline void set_isTimerRunning_7(bool value)
	{
		___isTimerRunning_7 = value;
	}

	inline static int32_t get_offset_of_startTime_8() { return static_cast<int32_t>(offsetof(CountdownTimer_t636337431, ___startTime_8)); }
	inline float get_startTime_8() const { return ___startTime_8; }
	inline float* get_address_of_startTime_8() { return &___startTime_8; }
	inline void set_startTime_8(float value)
	{
		___startTime_8 = value;
	}

	inline static int32_t get_offset_of_Text_9() { return static_cast<int32_t>(offsetof(CountdownTimer_t636337431, ___Text_9)); }
	inline Text_t1901882714 * get_Text_9() const { return ___Text_9; }
	inline Text_t1901882714 ** get_address_of_Text_9() { return &___Text_9; }
	inline void set_Text_9(Text_t1901882714 * value)
	{
		___Text_9 = value;
		Il2CppCodeGenWriteBarrier((&___Text_9), value);
	}

	inline static int32_t get_offset_of_Countdown_10() { return static_cast<int32_t>(offsetof(CountdownTimer_t636337431, ___Countdown_10)); }
	inline float get_Countdown_10() const { return ___Countdown_10; }
	inline float* get_address_of_Countdown_10() { return &___Countdown_10; }
	inline void set_Countdown_10(float value)
	{
		___Countdown_10 = value;
	}
};

struct CountdownTimer_t636337431_StaticFields
{
public:
	// Photon.Pun.UtilityScripts.CountdownTimer/CountdownTimerHasExpired Photon.Pun.UtilityScripts.CountdownTimer::OnCountdownTimerHasExpired
	CountdownTimerHasExpired_t4234756006 * ___OnCountdownTimerHasExpired_6;

public:
	inline static int32_t get_offset_of_OnCountdownTimerHasExpired_6() { return static_cast<int32_t>(offsetof(CountdownTimer_t636337431_StaticFields, ___OnCountdownTimerHasExpired_6)); }
	inline CountdownTimerHasExpired_t4234756006 * get_OnCountdownTimerHasExpired_6() const { return ___OnCountdownTimerHasExpired_6; }
	inline CountdownTimerHasExpired_t4234756006 ** get_address_of_OnCountdownTimerHasExpired_6() { return &___OnCountdownTimerHasExpired_6; }
	inline void set_OnCountdownTimerHasExpired_6(CountdownTimerHasExpired_t4234756006 * value)
	{
		___OnCountdownTimerHasExpired_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnCountdownTimerHasExpired_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTDOWNTIMER_T636337431_H
#ifndef PLAYERNUMBERING_T1896744609_H
#define PLAYERNUMBERING_T1896744609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.PlayerNumbering
struct  PlayerNumbering_t1896744609  : public MonoBehaviourPunCallbacks_t1810614660
{
public:
	// System.Boolean Photon.Pun.UtilityScripts.PlayerNumbering::dontDestroyOnLoad
	bool ___dontDestroyOnLoad_9;

public:
	inline static int32_t get_offset_of_dontDestroyOnLoad_9() { return static_cast<int32_t>(offsetof(PlayerNumbering_t1896744609, ___dontDestroyOnLoad_9)); }
	inline bool get_dontDestroyOnLoad_9() const { return ___dontDestroyOnLoad_9; }
	inline bool* get_address_of_dontDestroyOnLoad_9() { return &___dontDestroyOnLoad_9; }
	inline void set_dontDestroyOnLoad_9(bool value)
	{
		___dontDestroyOnLoad_9 = value;
	}
};

struct PlayerNumbering_t1896744609_StaticFields
{
public:
	// Photon.Pun.UtilityScripts.PlayerNumbering Photon.Pun.UtilityScripts.PlayerNumbering::instance
	PlayerNumbering_t1896744609 * ___instance_5;
	// Photon.Realtime.Player[] Photon.Pun.UtilityScripts.PlayerNumbering::SortedPlayers
	PlayerU5BU5D_t3651776216* ___SortedPlayers_6;
	// Photon.Pun.UtilityScripts.PlayerNumbering/PlayerNumberingChanged Photon.Pun.UtilityScripts.PlayerNumbering::OnPlayerNumberingChanged
	PlayerNumberingChanged_t966975091 * ___OnPlayerNumberingChanged_7;
	// System.Func`2<Photon.Realtime.Player,System.Int32> Photon.Pun.UtilityScripts.PlayerNumbering::<>f__mg$cache0
	Func_2_t3125806854 * ___U3CU3Ef__mgU24cache0_10;
	// System.Func`2<Photon.Realtime.Player,System.Int32> Photon.Pun.UtilityScripts.PlayerNumbering::<>f__mg$cache1
	Func_2_t3125806854 * ___U3CU3Ef__mgU24cache1_11;
	// System.Func`2<Photon.Realtime.Player,System.Int32> Photon.Pun.UtilityScripts.PlayerNumbering::<>f__am$cache0
	Func_2_t3125806854 * ___U3CU3Ef__amU24cache0_12;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(PlayerNumbering_t1896744609_StaticFields, ___instance_5)); }
	inline PlayerNumbering_t1896744609 * get_instance_5() const { return ___instance_5; }
	inline PlayerNumbering_t1896744609 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(PlayerNumbering_t1896744609 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}

	inline static int32_t get_offset_of_SortedPlayers_6() { return static_cast<int32_t>(offsetof(PlayerNumbering_t1896744609_StaticFields, ___SortedPlayers_6)); }
	inline PlayerU5BU5D_t3651776216* get_SortedPlayers_6() const { return ___SortedPlayers_6; }
	inline PlayerU5BU5D_t3651776216** get_address_of_SortedPlayers_6() { return &___SortedPlayers_6; }
	inline void set_SortedPlayers_6(PlayerU5BU5D_t3651776216* value)
	{
		___SortedPlayers_6 = value;
		Il2CppCodeGenWriteBarrier((&___SortedPlayers_6), value);
	}

	inline static int32_t get_offset_of_OnPlayerNumberingChanged_7() { return static_cast<int32_t>(offsetof(PlayerNumbering_t1896744609_StaticFields, ___OnPlayerNumberingChanged_7)); }
	inline PlayerNumberingChanged_t966975091 * get_OnPlayerNumberingChanged_7() const { return ___OnPlayerNumberingChanged_7; }
	inline PlayerNumberingChanged_t966975091 ** get_address_of_OnPlayerNumberingChanged_7() { return &___OnPlayerNumberingChanged_7; }
	inline void set_OnPlayerNumberingChanged_7(PlayerNumberingChanged_t966975091 * value)
	{
		___OnPlayerNumberingChanged_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnPlayerNumberingChanged_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_10() { return static_cast<int32_t>(offsetof(PlayerNumbering_t1896744609_StaticFields, ___U3CU3Ef__mgU24cache0_10)); }
	inline Func_2_t3125806854 * get_U3CU3Ef__mgU24cache0_10() const { return ___U3CU3Ef__mgU24cache0_10; }
	inline Func_2_t3125806854 ** get_address_of_U3CU3Ef__mgU24cache0_10() { return &___U3CU3Ef__mgU24cache0_10; }
	inline void set_U3CU3Ef__mgU24cache0_10(Func_2_t3125806854 * value)
	{
		___U3CU3Ef__mgU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_11() { return static_cast<int32_t>(offsetof(PlayerNumbering_t1896744609_StaticFields, ___U3CU3Ef__mgU24cache1_11)); }
	inline Func_2_t3125806854 * get_U3CU3Ef__mgU24cache1_11() const { return ___U3CU3Ef__mgU24cache1_11; }
	inline Func_2_t3125806854 ** get_address_of_U3CU3Ef__mgU24cache1_11() { return &___U3CU3Ef__mgU24cache1_11; }
	inline void set_U3CU3Ef__mgU24cache1_11(Func_2_t3125806854 * value)
	{
		___U3CU3Ef__mgU24cache1_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(PlayerNumbering_t1896744609_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Func_2_t3125806854 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Func_2_t3125806854 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Func_2_t3125806854 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERNUMBERING_T1896744609_H
#ifndef PUNTEAMS_T4131221827_H
#define PUNTEAMS_T4131221827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.PunTeams
struct  PunTeams_t4131221827  : public MonoBehaviourPunCallbacks_t1810614660
{
public:

public:
};

struct PunTeams_t4131221827_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<Photon.Pun.UtilityScripts.PunTeams/Team,System.Collections.Generic.List`1<Photon.Realtime.Player>> Photon.Pun.UtilityScripts.PunTeams::PlayersPerTeam
	Dictionary_2_t660995199 * ___PlayersPerTeam_5;

public:
	inline static int32_t get_offset_of_PlayersPerTeam_5() { return static_cast<int32_t>(offsetof(PunTeams_t4131221827_StaticFields, ___PlayersPerTeam_5)); }
	inline Dictionary_2_t660995199 * get_PlayersPerTeam_5() const { return ___PlayersPerTeam_5; }
	inline Dictionary_2_t660995199 ** get_address_of_PlayersPerTeam_5() { return &___PlayersPerTeam_5; }
	inline void set_PlayersPerTeam_5(Dictionary_2_t660995199 * value)
	{
		___PlayersPerTeam_5 = value;
		Il2CppCodeGenWriteBarrier((&___PlayersPerTeam_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNTEAMS_T4131221827_H
#ifndef PUNTURNMANAGER_T13104469_H
#define PUNTURNMANAGER_T13104469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.UtilityScripts.PunTurnManager
struct  PunTurnManager_t13104469  : public MonoBehaviourPunCallbacks_t1810614660
{
public:
	// System.Single Photon.Pun.UtilityScripts.PunTurnManager::TurnDuration
	float ___TurnDuration_5;
	// Photon.Pun.UtilityScripts.IPunTurnManagerCallbacks Photon.Pun.UtilityScripts.PunTurnManager::TurnManagerListener
	RuntimeObject* ___TurnManagerListener_6;
	// System.Collections.Generic.HashSet`1<Photon.Realtime.Player> Photon.Pun.UtilityScripts.PunTurnManager::finishedPlayers
	HashSet_1_t1444519063 * ___finishedPlayers_7;
	// System.Boolean Photon.Pun.UtilityScripts.PunTurnManager::_isOverCallProcessed
	bool ____isOverCallProcessed_11;

public:
	inline static int32_t get_offset_of_TurnDuration_5() { return static_cast<int32_t>(offsetof(PunTurnManager_t13104469, ___TurnDuration_5)); }
	inline float get_TurnDuration_5() const { return ___TurnDuration_5; }
	inline float* get_address_of_TurnDuration_5() { return &___TurnDuration_5; }
	inline void set_TurnDuration_5(float value)
	{
		___TurnDuration_5 = value;
	}

	inline static int32_t get_offset_of_TurnManagerListener_6() { return static_cast<int32_t>(offsetof(PunTurnManager_t13104469, ___TurnManagerListener_6)); }
	inline RuntimeObject* get_TurnManagerListener_6() const { return ___TurnManagerListener_6; }
	inline RuntimeObject** get_address_of_TurnManagerListener_6() { return &___TurnManagerListener_6; }
	inline void set_TurnManagerListener_6(RuntimeObject* value)
	{
		___TurnManagerListener_6 = value;
		Il2CppCodeGenWriteBarrier((&___TurnManagerListener_6), value);
	}

	inline static int32_t get_offset_of_finishedPlayers_7() { return static_cast<int32_t>(offsetof(PunTurnManager_t13104469, ___finishedPlayers_7)); }
	inline HashSet_1_t1444519063 * get_finishedPlayers_7() const { return ___finishedPlayers_7; }
	inline HashSet_1_t1444519063 ** get_address_of_finishedPlayers_7() { return &___finishedPlayers_7; }
	inline void set_finishedPlayers_7(HashSet_1_t1444519063 * value)
	{
		___finishedPlayers_7 = value;
		Il2CppCodeGenWriteBarrier((&___finishedPlayers_7), value);
	}

	inline static int32_t get_offset_of__isOverCallProcessed_11() { return static_cast<int32_t>(offsetof(PunTurnManager_t13104469, ____isOverCallProcessed_11)); }
	inline bool get__isOverCallProcessed_11() const { return ____isOverCallProcessed_11; }
	inline bool* get_address_of__isOverCallProcessed_11() { return &____isOverCallProcessed_11; }
	inline void set__isOverCallProcessed_11(bool value)
	{
		____isOverCallProcessed_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNTURNMANAGER_T13104469_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (ChatOperationCode_t2913553512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (ChatParameterCode_t3986735616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (ChatPeer_t3366739368), -1, sizeof(ChatPeer_t3366739368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2502[3] = 
{
	0,
	0,
	ChatPeer_t3366739368_StaticFields::get_offset_of_ProtocolToNameServerPort_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (CustomAuthenticationType_t2653033121)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2503[8] = 
{
	CustomAuthenticationType_t2653033121::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (AuthenticationValues_t563695058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[5] = 
{
	AuthenticationValues_t563695058::get_offset_of_authType_0(),
	AuthenticationValues_t563695058::get_offset_of_U3CAuthGetParametersU3Ek__BackingField_1(),
	AuthenticationValues_t563695058::get_offset_of_U3CAuthPostDataU3Ek__BackingField_2(),
	AuthenticationValues_t563695058::get_offset_of_U3CTokenU3Ek__BackingField_3(),
	AuthenticationValues_t563695058::get_offset_of_U3CUserIdU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (ParameterCode_t52387945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (ErrorCode_t424885541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (ChatState_t1558912819)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2507[13] = 
{
	ChatState_t1558912819::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (ChatUserStatus_t4200752496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (U3CModuleU3E_t692745548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (CullArea_t635391622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[16] = 
{
	0,
	0,
	CullArea_t635391622::get_offset_of_FIRST_GROUP_ID_6(),
	CullArea_t635391622::get_offset_of_SUBDIVISION_FIRST_LEVEL_ORDER_7(),
	CullArea_t635391622::get_offset_of_SUBDIVISION_SECOND_LEVEL_ORDER_8(),
	CullArea_t635391622::get_offset_of_SUBDIVISION_THIRD_LEVEL_ORDER_9(),
	CullArea_t635391622::get_offset_of_Center_10(),
	CullArea_t635391622::get_offset_of_Size_11(),
	CullArea_t635391622::get_offset_of_Subdivisions_12(),
	CullArea_t635391622::get_offset_of_NumberOfSubdivisions_13(),
	CullArea_t635391622::get_offset_of_U3CCellCountU3Ek__BackingField_14(),
	CullArea_t635391622::get_offset_of_U3CCellTreeU3Ek__BackingField_15(),
	CullArea_t635391622::get_offset_of_U3CMapU3Ek__BackingField_16(),
	CullArea_t635391622::get_offset_of_YIsUpAxis_17(),
	CullArea_t635391622::get_offset_of_RecreateCellHierarchy_18(),
	CullArea_t635391622::get_offset_of_idCounter_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (CellTree_t656254725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2512[1] = 
{
	CellTree_t656254725::get_offset_of_U3CRootNodeU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (CellTreeNode_t3709723763), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[9] = 
{
	CellTreeNode_t3709723763::get_offset_of_Id_0(),
	CellTreeNode_t3709723763::get_offset_of_Center_1(),
	CellTreeNode_t3709723763::get_offset_of_Size_2(),
	CellTreeNode_t3709723763::get_offset_of_TopLeft_3(),
	CellTreeNode_t3709723763::get_offset_of_BottomRight_4(),
	CellTreeNode_t3709723763::get_offset_of_NodeType_5(),
	CellTreeNode_t3709723763::get_offset_of_Parent_6(),
	CellTreeNode_t3709723763::get_offset_of_Childs_7(),
	CellTreeNode_t3709723763::get_offset_of_maxDistance_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (ENodeType_t548973521)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2514[4] = 
{
	ENodeType_t548973521::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (CullingHandler_t4282308608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[7] = 
{
	CullingHandler_t4282308608::get_offset_of_orderIndex_4(),
	CullingHandler_t4282308608::get_offset_of_cullArea_5(),
	CullingHandler_t4282308608::get_offset_of_previousActiveCells_6(),
	CullingHandler_t4282308608::get_offset_of_activeCells_7(),
	CullingHandler_t4282308608::get_offset_of_pView_8(),
	CullingHandler_t4282308608::get_offset_of_lastPosition_9(),
	CullingHandler_t4282308608::get_offset_of_currentPosition_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (PhotonLagSimulationGui_t145554164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[4] = 
{
	PhotonLagSimulationGui_t145554164::get_offset_of_WindowRect_4(),
	PhotonLagSimulationGui_t145554164::get_offset_of_WindowId_5(),
	PhotonLagSimulationGui_t145554164::get_offset_of_Visible_6(),
	PhotonLagSimulationGui_t145554164::get_offset_of_U3CPeerU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (PhotonStatsGui_t2125249956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[7] = 
{
	PhotonStatsGui_t2125249956::get_offset_of_statsWindowOn_4(),
	PhotonStatsGui_t2125249956::get_offset_of_statsOn_5(),
	PhotonStatsGui_t2125249956::get_offset_of_healthStatsVisible_6(),
	PhotonStatsGui_t2125249956::get_offset_of_trafficStatsOn_7(),
	PhotonStatsGui_t2125249956::get_offset_of_buttonsOn_8(),
	PhotonStatsGui_t2125249956::get_offset_of_statsRect_9(),
	PhotonStatsGui_t2125249956::get_offset_of_WindowId_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (PointedAtGameObjectInfo_t425461813), -1, sizeof(PointedAtGameObjectInfo_t425461813_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2518[3] = 
{
	PointedAtGameObjectInfo_t425461813_StaticFields::get_offset_of_Instance_4(),
	PointedAtGameObjectInfo_t425461813::get_offset_of_text_5(),
	PointedAtGameObjectInfo_t425461813::get_offset_of_focus_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (StatesGui_t4032328020), -1, sizeof(StatesGui_t4032328020_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2519[16] = 
{
	StatesGui_t4032328020::get_offset_of_GuiOffset_4(),
	StatesGui_t4032328020::get_offset_of_DontDestroy_5(),
	StatesGui_t4032328020::get_offset_of_ServerTimestamp_6(),
	StatesGui_t4032328020::get_offset_of_DetailedConnection_7(),
	StatesGui_t4032328020::get_offset_of_Server_8(),
	StatesGui_t4032328020::get_offset_of_AppVersion_9(),
	StatesGui_t4032328020::get_offset_of_UserId_10(),
	StatesGui_t4032328020::get_offset_of_Room_11(),
	StatesGui_t4032328020::get_offset_of_RoomProps_12(),
	StatesGui_t4032328020::get_offset_of_LocalPlayer_13(),
	StatesGui_t4032328020::get_offset_of_PlayerProps_14(),
	StatesGui_t4032328020::get_offset_of_Others_15(),
	StatesGui_t4032328020::get_offset_of_Buttons_16(),
	StatesGui_t4032328020::get_offset_of_ExpectedUsers_17(),
	StatesGui_t4032328020::get_offset_of_GuiRect_18(),
	StatesGui_t4032328020_StaticFields::get_offset_of_Instance_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (PlayerNumbering_t1896744609), -1, sizeof(PlayerNumbering_t1896744609_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2520[8] = 
{
	PlayerNumbering_t1896744609_StaticFields::get_offset_of_instance_5(),
	PlayerNumbering_t1896744609_StaticFields::get_offset_of_SortedPlayers_6(),
	PlayerNumbering_t1896744609_StaticFields::get_offset_of_OnPlayerNumberingChanged_7(),
	0,
	PlayerNumbering_t1896744609::get_offset_of_dontDestroyOnLoad_9(),
	PlayerNumbering_t1896744609_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_10(),
	PlayerNumbering_t1896744609_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_11(),
	PlayerNumbering_t1896744609_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (PlayerNumberingChanged_t966975091), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (PlayerNumberingExtensions_t2795551351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (PunPlayerScores_t3603890024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (ScoreExtensions_t2818287168), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (PunTeams_t4131221827), -1, sizeof(PunTeams_t4131221827_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2525[2] = 
{
	PunTeams_t4131221827_StaticFields::get_offset_of_PlayersPerTeam_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (Team_t3101236044)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2526[4] = 
{
	Team_t3101236044::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (TeamExtensions_t1434078711), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (SmoothSyncMovement_t663920301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2528[3] = 
{
	SmoothSyncMovement_t663920301::get_offset_of_SmoothingDelay_5(),
	SmoothSyncMovement_t663920301::get_offset_of_correctPlayerPos_6(),
	SmoothSyncMovement_t663920301::get_offset_of_correctPlayerRot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (ConnectAndJoinRandom_t1495527429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2529[2] = 
{
	ConnectAndJoinRandom_t1495527429::get_offset_of_AutoConnect_5(),
	ConnectAndJoinRandom_t1495527429::get_offset_of_Version_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (MoveByKeys_t979471368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2530[7] = 
{
	MoveByKeys_t979471368::get_offset_of_Speed_5(),
	MoveByKeys_t979471368::get_offset_of_JumpForce_6(),
	MoveByKeys_t979471368::get_offset_of_JumpTimeout_7(),
	MoveByKeys_t979471368::get_offset_of_isSprite_8(),
	MoveByKeys_t979471368::get_offset_of_jumpingTime_9(),
	MoveByKeys_t979471368::get_offset_of_body_10(),
	MoveByKeys_t979471368::get_offset_of_body2d_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (OnClickDestroy_t3019334366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[3] = 
{
	OnClickDestroy_t3019334366::get_offset_of_Button_5(),
	OnClickDestroy_t3019334366::get_offset_of_ModifierKey_6(),
	OnClickDestroy_t3019334366::get_offset_of_DestroyByRpc_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (U3CDestroyRpcU3Ec__Iterator0_t785359983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2532[4] = 
{
	U3CDestroyRpcU3Ec__Iterator0_t785359983::get_offset_of_U24this_0(),
	U3CDestroyRpcU3Ec__Iterator0_t785359983::get_offset_of_U24current_1(),
	U3CDestroyRpcU3Ec__Iterator0_t785359983::get_offset_of_U24disposing_2(),
	U3CDestroyRpcU3Ec__Iterator0_t785359983::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (OnClickInstantiate_t3820097070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2533[4] = 
{
	OnClickInstantiate_t3820097070::get_offset_of_Button_4(),
	OnClickInstantiate_t3820097070::get_offset_of_ModifierKey_5(),
	OnClickInstantiate_t3820097070::get_offset_of_Prefab_6(),
	OnClickInstantiate_t3820097070::get_offset_of_InstantiateType_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (InstantiateOption_t2742092059)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2534[3] = 
{
	InstantiateOption_t2742092059::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (OnClickRpc_t2528495255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2535[6] = 
{
	OnClickRpc_t2528495255::get_offset_of_Button_5(),
	OnClickRpc_t2528495255::get_offset_of_ModifierKey_6(),
	OnClickRpc_t2528495255::get_offset_of_Target_7(),
	OnClickRpc_t2528495255::get_offset_of_originalMaterial_8(),
	OnClickRpc_t2528495255::get_offset_of_originalColor_9(),
	OnClickRpc_t2528495255::get_offset_of_isFlashing_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (U3CClickFlashU3Ec__Iterator0_t700316031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2536[7] = 
{
	U3CClickFlashU3Ec__Iterator0_t700316031::get_offset_of_U3CwasEmissiveU3E__0_0(),
	U3CClickFlashU3Ec__Iterator0_t700316031::get_offset_of_U3CfU3E__1_1(),
	U3CClickFlashU3Ec__Iterator0_t700316031::get_offset_of_U3ClerpedU3E__2_2(),
	U3CClickFlashU3Ec__Iterator0_t700316031::get_offset_of_U24this_3(),
	U3CClickFlashU3Ec__Iterator0_t700316031::get_offset_of_U24current_4(),
	U3CClickFlashU3Ec__Iterator0_t700316031::get_offset_of_U24disposing_5(),
	U3CClickFlashU3Ec__Iterator0_t700316031::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (OnEscapeQuit_t589738653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (OnJoinedInstantiate_t361656573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2538[3] = 
{
	OnJoinedInstantiate_t361656573::get_offset_of_SpawnPosition_4(),
	OnJoinedInstantiate_t361656573::get_offset_of_PositionOffset_5(),
	OnJoinedInstantiate_t361656573::get_offset_of_PrefabsToInstantiate_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (OnStartDelete_t2541883597), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (CountdownTimer_t636337431), -1, sizeof(CountdownTimer_t636337431_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2540[6] = 
{
	0,
	CountdownTimer_t636337431_StaticFields::get_offset_of_OnCountdownTimerHasExpired_6(),
	CountdownTimer_t636337431::get_offset_of_isTimerRunning_7(),
	CountdownTimer_t636337431::get_offset_of_startTime_8(),
	CountdownTimer_t636337431::get_offset_of_Text_9(),
	CountdownTimer_t636337431::get_offset_of_Countdown_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (CountdownTimerHasExpired_t4234756006), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (PunTurnManager_t13104469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[7] = 
{
	PunTurnManager_t13104469::get_offset_of_TurnDuration_5(),
	PunTurnManager_t13104469::get_offset_of_TurnManagerListener_6(),
	PunTurnManager_t13104469::get_offset_of_finishedPlayers_7(),
	0,
	0,
	0,
	PunTurnManager_t13104469::get_offset_of__isOverCallProcessed_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (TurnExtensions_t575626914), -1, sizeof(TurnExtensions_t575626914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2544[3] = 
{
	TurnExtensions_t575626914_StaticFields::get_offset_of_TurnPropKey_0(),
	TurnExtensions_t575626914_StaticFields::get_offset_of_TurnStartPropKey_1(),
	TurnExtensions_t575626914_StaticFields::get_offset_of_FinishedTurnPropKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (ButtonInsideScrollList_t2732141882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[1] = 
{
	ButtonInsideScrollList_t2732141882::get_offset_of_scrollRect_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (EventSystemSpawner_t2883568726), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (GraphicToggleIsOnTransition_t3135858402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[7] = 
{
	GraphicToggleIsOnTransition_t3135858402::get_offset_of_toggle_4(),
	GraphicToggleIsOnTransition_t3135858402::get_offset_of__graphic_5(),
	GraphicToggleIsOnTransition_t3135858402::get_offset_of_NormalOnColor_6(),
	GraphicToggleIsOnTransition_t3135858402::get_offset_of_NormalOffColor_7(),
	GraphicToggleIsOnTransition_t3135858402::get_offset_of_HoverOnColor_8(),
	GraphicToggleIsOnTransition_t3135858402::get_offset_of_HoverOffColor_9(),
	GraphicToggleIsOnTransition_t3135858402::get_offset_of_isHover_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (OnPointerOverTooltip_t3690126031), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (TabViewManager_t3686055887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2549[5] = 
{
	TabViewManager_t3686055887::get_offset_of_ToggleGroup_4(),
	TabViewManager_t3686055887::get_offset_of_Tabs_5(),
	TabViewManager_t3686055887::get_offset_of_OnTabChanged_6(),
	TabViewManager_t3686055887::get_offset_of_CurrentTab_7(),
	TabViewManager_t3686055887::get_offset_of_Tab_lut_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (TabChangeEvent_t3080003849), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (Tab_t117203701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2551[3] = 
{
	Tab_t117203701::get_offset_of_ID_0(),
	Tab_t117203701::get_offset_of_Toggle_1(),
	Tab_t117203701::get_offset_of_View_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (U3CStartU3Ec__AnonStorey0_t1921436154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2552[2] = 
{
	U3CStartU3Ec__AnonStorey0_t1921436154::get_offset_of__tab_0(),
	U3CStartU3Ec__AnonStorey0_t1921436154::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (TextButtonTransition_t2307109358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2553[4] = 
{
	TextButtonTransition_t2307109358::get_offset_of__text_4(),
	TextButtonTransition_t2307109358::get_offset_of_Selectable_5(),
	TextButtonTransition_t2307109358::get_offset_of_NormalColor_6(),
	TextButtonTransition_t2307109358::get_offset_of_HoverColor_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (TextToggleIsOnTransition_t4243955300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2554[7] = 
{
	TextToggleIsOnTransition_t4243955300::get_offset_of_toggle_4(),
	TextToggleIsOnTransition_t4243955300::get_offset_of__text_5(),
	TextToggleIsOnTransition_t4243955300::get_offset_of_NormalOnColor_6(),
	TextToggleIsOnTransition_t4243955300::get_offset_of_NormalOffColor_7(),
	TextToggleIsOnTransition_t4243955300::get_offset_of_HoverOnColor_8(),
	TextToggleIsOnTransition_t4243955300::get_offset_of_HoverOffColor_9(),
	TextToggleIsOnTransition_t4243955300::get_offset_of_isHover_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255368), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2555[3] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_0(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_1(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (U24ArrayTypeU3D16_t3253128245)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D16_t3253128245 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (U24ArrayTypeU3D32_t3651253610)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D32_t3651253610 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (U24ArrayTypeU3D48_t1336283963)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D48_t1336283963 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (U3CModuleU3E_t692745549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (AnalyticsEvent_t4058973021), -1, sizeof(AnalyticsEvent_t4058973021_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2560[4] = 
{
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_k_SdkVersion_0(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_m_EventData_1(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of__debugMode_2(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_enumRenameTable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (U3CModuleU3E_t692745550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (U3CModuleU3E_t692745551), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (AnalyticsEventTracker_t2285229262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2563[2] = 
{
	AnalyticsEventTracker_t2285229262::get_offset_of_m_Trigger_4(),
	AnalyticsEventTracker_t2285229262::get_offset_of_m_EventPayload_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (U3CTimedTriggerU3Ec__Iterator0_t3813435494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2564[4] = 
{
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24this_0(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24current_1(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24disposing_2(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (AnalyticsEventTrackerSettings_t480422680), -1, sizeof(AnalyticsEventTrackerSettings_t480422680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2565[2] = 
{
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_paramCountMax_0(),
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_triggerRuleCountMax_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (AnalyticsEventParam_t2480121928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2566[5] = 
{
	AnalyticsEventParam_t2480121928::get_offset_of_m_RequirementType_0(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_GroupID_1(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Tooltip_2(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Name_3(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (RequirementType_t3584265503)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2567[4] = 
{
	RequirementType_t3584265503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (AnalyticsEventParamListContainer_t587083383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2568[1] = 
{
	AnalyticsEventParamListContainer_t587083383::get_offset_of_m_Parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (StandardEventPayload_t1629891255), -1, sizeof(StandardEventPayload_t1629891255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2569[6] = 
{
	StandardEventPayload_t1629891255::get_offset_of_m_IsEventExpanded_0(),
	StandardEventPayload_t1629891255::get_offset_of_m_StandardEventType_1(),
	StandardEventPayload_t1629891255::get_offset_of_standardEventType_2(),
	StandardEventPayload_t1629891255::get_offset_of_m_Parameters_3(),
	StandardEventPayload_t1629891255_StaticFields::get_offset_of_m_EventData_4(),
	StandardEventPayload_t1629891255::get_offset_of_m_Name_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (TrackableField_t1772682203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[3] = 
{
	TrackableField_t1772682203::get_offset_of_m_ValidTypeNames_2(),
	TrackableField_t1772682203::get_offset_of_m_Type_3(),
	TrackableField_t1772682203::get_offset_of_m_EnumType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (TrackablePropertyBase_t2121532948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2571[2] = 
{
	TrackablePropertyBase_t2121532948::get_offset_of_m_Target_0(),
	TrackablePropertyBase_t2121532948::get_offset_of_m_Path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (ValueProperty_t1868393739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2572[11] = 
{
	ValueProperty_t1868393739::get_offset_of_m_EditingCustomValue_0(),
	ValueProperty_t1868393739::get_offset_of_m_PopupIndex_1(),
	ValueProperty_t1868393739::get_offset_of_m_CustomValue_2(),
	ValueProperty_t1868393739::get_offset_of_m_FixedType_3(),
	ValueProperty_t1868393739::get_offset_of_m_EnumType_4(),
	ValueProperty_t1868393739::get_offset_of_m_EnumTypeIsCustomizable_5(),
	ValueProperty_t1868393739::get_offset_of_m_CanDisable_6(),
	ValueProperty_t1868393739::get_offset_of_m_PropertyType_7(),
	ValueProperty_t1868393739::get_offset_of_m_ValueType_8(),
	ValueProperty_t1868393739::get_offset_of_m_Value_9(),
	ValueProperty_t1868393739::get_offset_of_m_Target_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (PropertyType_t4040930247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2573[4] = 
{
	PropertyType_t4040930247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (AnalyticsTracker_t731021378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[5] = 
{
	AnalyticsTracker_t731021378::get_offset_of_m_EventName_4(),
	AnalyticsTracker_t731021378::get_offset_of_m_Dict_5(),
	AnalyticsTracker_t731021378::get_offset_of_m_PrevDictHash_6(),
	AnalyticsTracker_t731021378::get_offset_of_m_TrackableProperty_7(),
	AnalyticsTracker_t731021378::get_offset_of_m_Trigger_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (Trigger_t4199345191)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2575[8] = 
{
	Trigger_t4199345191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (TrackableProperty_t3943537984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[2] = 
{
	0,
	TrackableProperty_t3943537984::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (FieldWithTarget_t3058750293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2577[6] = 
{
	FieldWithTarget_t3058750293::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t3058750293::get_offset_of_m_Target_1(),
	FieldWithTarget_t3058750293::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t3058750293::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t3058750293::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t3058750293::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (TriggerBool_t501031542)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2578[4] = 
{
	TriggerBool_t501031542::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (TriggerLifecycleEvent_t3193146760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2579[9] = 
{
	TriggerLifecycleEvent_t3193146760::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (TriggerOperator_t3611898925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2580[9] = 
{
	TriggerOperator_t3611898925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (TriggerType_t105272677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2581[5] = 
{
	TriggerType_t105272677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2587[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (U3CModuleU3E_t692745552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (EventSystemSpawner_t790404666), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (OnStartDelete_t2848433495), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (TextButtonTransition_t7860473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2591[4] = 
{
	TextButtonTransition_t7860473::get_offset_of__text_4(),
	TextButtonTransition_t7860473::get_offset_of_Selectable_5(),
	TextButtonTransition_t7860473::get_offset_of_NormalColor_6(),
	TextButtonTransition_t7860473::get_offset_of_HoverColor_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (TextToggleIsOnTransition_t2348424493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2592[7] = 
{
	TextToggleIsOnTransition_t2348424493::get_offset_of_toggle_4(),
	TextToggleIsOnTransition_t2348424493::get_offset_of__text_5(),
	TextToggleIsOnTransition_t2348424493::get_offset_of_NormalOnColor_6(),
	TextToggleIsOnTransition_t2348424493::get_offset_of_NormalOffColor_7(),
	TextToggleIsOnTransition_t2348424493::get_offset_of_HoverOnColor_8(),
	TextToggleIsOnTransition_t2348424493::get_offset_of_HoverOffColor_9(),
	TextToggleIsOnTransition_t2348424493::get_offset_of_isHover_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (ChannelSelector_t3480308131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2593[1] = 
{
	ChannelSelector_t3480308131::get_offset_of_Channel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (ChatAppIdCheckerUI_t722438586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2594[1] = 
{
	ChatAppIdCheckerUI_t722438586::get_offset_of_Description_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (ChatGui_t3673337520), -1, sizeof(ChatGui_t3673337520_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2595[24] = 
{
	ChatGui_t3673337520::get_offset_of_ChannelsToJoinOnConnect_4(),
	ChatGui_t3673337520::get_offset_of_FriendsList_5(),
	ChatGui_t3673337520::get_offset_of_HistoryLengthToFetch_6(),
	ChatGui_t3673337520::get_offset_of_U3CUserNameU3Ek__BackingField_7(),
	ChatGui_t3673337520::get_offset_of_selectedChannelName_8(),
	ChatGui_t3673337520::get_offset_of_chatClient_9(),
	ChatGui_t3673337520::get_offset_of_chatAppSettings_10(),
	ChatGui_t3673337520::get_offset_of_missingAppIdErrorPanel_11(),
	ChatGui_t3673337520::get_offset_of_ConnectingLabel_12(),
	ChatGui_t3673337520::get_offset_of_ChatPanel_13(),
	ChatGui_t3673337520::get_offset_of_UserIdFormPanel_14(),
	ChatGui_t3673337520::get_offset_of_InputFieldChat_15(),
	ChatGui_t3673337520::get_offset_of_CurrentChannelText_16(),
	ChatGui_t3673337520::get_offset_of_ChannelToggleToInstantiate_17(),
	ChatGui_t3673337520::get_offset_of_FriendListUiItemtoInstantiate_18(),
	ChatGui_t3673337520::get_offset_of_channelToggles_19(),
	ChatGui_t3673337520::get_offset_of_friendListItemLUT_20(),
	ChatGui_t3673337520::get_offset_of_ShowState_21(),
	ChatGui_t3673337520::get_offset_of_Title_22(),
	ChatGui_t3673337520::get_offset_of_StateText_23(),
	ChatGui_t3673337520::get_offset_of_UserIdText_24(),
	ChatGui_t3673337520_StaticFields::get_offset_of_HelpText_25(),
	ChatGui_t3673337520::get_offset_of_TestLength_26(),
	ChatGui_t3673337520::get_offset_of_testBytes_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (FriendItem_t2916710093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2596[3] = 
{
	FriendItem_t2916710093::get_offset_of_NameLabel_4(),
	FriendItem_t2916710093::get_offset_of_StatusLabel_5(),
	FriendItem_t2916710093::get_offset_of_Health_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (IgnoreUiRaycastWhenInactive_t3624510311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (NamePickGui_t2955268898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2598[3] = 
{
	0,
	NamePickGui_t2955268898::get_offset_of_chatNewComponent_5(),
	NamePickGui_t2955268898::get_offset_of_idInput_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (ConnectAndJoinRandomLb_t3563937443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[4] = 
{
	ConnectAndJoinRandomLb_t3563937443::get_offset_of_AppId_4(),
	ConnectAndJoinRandomLb_t3563937443::get_offset_of_lbc_5(),
	ConnectAndJoinRandomLb_t3563937443::get_offset_of_ch_6(),
	ConnectAndJoinRandomLb_t3563937443::get_offset_of_StateUiText_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
