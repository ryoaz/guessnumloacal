﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// EnemyLogScript
struct EnemyLogScript_t943560016;
// EnemyLogTextView
struct EnemyLogTextView_t71717095;
// Enter
struct Enter_t1090004723;
// Photon.Pun.Demo.Cockpit.PropertyListenerBase
struct PropertyListenerBase_t1226442861;
// Photon.Pun.Demo.Cockpit.PunCockpit
struct PunCockpit_t3722300818;
// Photon.Pun.Demo.Cockpit.PunCockpitEmbed
struct PunCockpitEmbed_t1735463960;
// Photon.Pun.Demo.Cockpit.RoomListView
struct RoomListView_t2450403350;
// Photon.Pun.PhotonView
struct PhotonView_t3684715584;
// Photon.Pun.UtilityScripts.ConnectAndJoinRandom
struct ConnectAndJoinRandom_t1495527429;
// ResultManager
struct ResultManager_t2208194647;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<Photon.Pun.Demo.Shared.DocLinks/Languages,System.String>
struct Dictionary_2_t4246196329;
// System.Collections.Generic.Dictionary`2<Photon.Pun.Demo.Shared.DocLinks/Products,System.String>
struct Dictionary_2_t2878688375;
// System.Collections.Generic.Dictionary`2<Photon.Pun.Demo.Shared.DocLinks/Versions,System.String>
struct Dictionary_2_t1561956248;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Void
struct Void_t1185182177;
// TimerScript
struct TimerScript_t1618777791;
// TurnScript
struct TurnScript_t2279615295;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.YieldInstruction
struct YieldInstruction_t403091072;
// YourLogScript
struct YourLogScript_t3397472296;
// YourLogTextView
struct YourLogTextView_t1250600108;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CSTARTU3EC__ITERATOR0_T496713993_H
#define U3CSTARTU3EC__ITERATOR0_T496713993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.PunCockpitEmbed/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t496713993  : public RuntimeObject
{
public:
	// Photon.Pun.Demo.Cockpit.PunCockpitEmbed Photon.Pun.Demo.Cockpit.PunCockpitEmbed/<Start>c__Iterator0::$this
	PunCockpitEmbed_t1735463960 * ___U24this_0;
	// System.Object Photon.Pun.Demo.Cockpit.PunCockpitEmbed/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Photon.Pun.Demo.Cockpit.PunCockpitEmbed/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Photon.Pun.Demo.Cockpit.PunCockpitEmbed/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t496713993, ___U24this_0)); }
	inline PunCockpitEmbed_t1735463960 * get_U24this_0() const { return ___U24this_0; }
	inline PunCockpitEmbed_t1735463960 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(PunCockpitEmbed_t1735463960 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t496713993, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t496713993, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t496713993, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T496713993_H
#ifndef U3CCLEARSTATUSU3EC__ITERATOR0_T675715044_H
#define U3CCLEARSTATUSU3EC__ITERATOR0_T675715044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.RoomListView/<clearStatus>c__Iterator0
struct  U3CclearStatusU3Ec__Iterator0_t675715044  : public RuntimeObject
{
public:
	// Photon.Pun.Demo.Cockpit.RoomListView Photon.Pun.Demo.Cockpit.RoomListView/<clearStatus>c__Iterator0::$this
	RoomListView_t2450403350 * ___U24this_0;
	// System.Object Photon.Pun.Demo.Cockpit.RoomListView/<clearStatus>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Photon.Pun.Demo.Cockpit.RoomListView/<clearStatus>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Photon.Pun.Demo.Cockpit.RoomListView/<clearStatus>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CclearStatusU3Ec__Iterator0_t675715044, ___U24this_0)); }
	inline RoomListView_t2450403350 * get_U24this_0() const { return ___U24this_0; }
	inline RoomListView_t2450403350 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(RoomListView_t2450403350 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CclearStatusU3Ec__Iterator0_t675715044, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CclearStatusU3Ec__Iterator0_t675715044, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CclearStatusU3Ec__Iterator0_t675715044, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLEARSTATUSU3EC__ITERATOR0_T675715044_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U24ARRAYTYPEU3D512_T3839624093_H
#define U24ARRAYTYPEU3D512_T3839624093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=512
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D512_t3839624093 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D512_t3839624093__padding[512];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D512_T3839624093_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255369  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=512 <PrivateImplementationDetails>::$field-E74588860CA220F5327520E16546CBB6016903F4
	U24ArrayTypeU3D512_t3839624093  ___U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_0;

public:
	inline static int32_t get_offset_of_U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_0)); }
	inline U24ArrayTypeU3D512_t3839624093  get_U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_0() const { return ___U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_0; }
	inline U24ArrayTypeU3D512_t3839624093 * get_address_of_U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_0() { return &___U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_0; }
	inline void set_U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_0(U24ArrayTypeU3D512_t3839624093  value)
	{
		___U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#ifndef U3CFADEOUTU3EC__ITERATOR0_T712175311_H
#define U3CFADEOUTU3EC__ITERATOR0_T712175311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.PropertyListenerBase/<FadeOut>c__Iterator0
struct  U3CFadeOutU3Ec__Iterator0_t712175311  : public RuntimeObject
{
public:
	// System.Single Photon.Pun.Demo.Cockpit.PropertyListenerBase/<FadeOut>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_0;
	// UnityEngine.UI.Graphic Photon.Pun.Demo.Cockpit.PropertyListenerBase/<FadeOut>c__Iterator0::image
	Graphic_t1660335611 * ___image_1;
	// UnityEngine.Color Photon.Pun.Demo.Cockpit.PropertyListenerBase/<FadeOut>c__Iterator0::<c>__0
	Color_t2555686324  ___U3CcU3E__0_2;
	// Photon.Pun.Demo.Cockpit.PropertyListenerBase Photon.Pun.Demo.Cockpit.PropertyListenerBase/<FadeOut>c__Iterator0::$this
	PropertyListenerBase_t1226442861 * ___U24this_3;
	// System.Object Photon.Pun.Demo.Cockpit.PropertyListenerBase/<FadeOut>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Photon.Pun.Demo.Cockpit.PropertyListenerBase/<FadeOut>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Photon.Pun.Demo.Cockpit.PropertyListenerBase/<FadeOut>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator0_t712175311, ___U3CelapsedTimeU3E__0_0)); }
	inline float get_U3CelapsedTimeU3E__0_0() const { return ___U3CelapsedTimeU3E__0_0; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_0() { return &___U3CelapsedTimeU3E__0_0; }
	inline void set_U3CelapsedTimeU3E__0_0(float value)
	{
		___U3CelapsedTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_image_1() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator0_t712175311, ___image_1)); }
	inline Graphic_t1660335611 * get_image_1() const { return ___image_1; }
	inline Graphic_t1660335611 ** get_address_of_image_1() { return &___image_1; }
	inline void set_image_1(Graphic_t1660335611 * value)
	{
		___image_1 = value;
		Il2CppCodeGenWriteBarrier((&___image_1), value);
	}

	inline static int32_t get_offset_of_U3CcU3E__0_2() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator0_t712175311, ___U3CcU3E__0_2)); }
	inline Color_t2555686324  get_U3CcU3E__0_2() const { return ___U3CcU3E__0_2; }
	inline Color_t2555686324 * get_address_of_U3CcU3E__0_2() { return &___U3CcU3E__0_2; }
	inline void set_U3CcU3E__0_2(Color_t2555686324  value)
	{
		___U3CcU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator0_t712175311, ___U24this_3)); }
	inline PropertyListenerBase_t1226442861 * get_U24this_3() const { return ___U24this_3; }
	inline PropertyListenerBase_t1226442861 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PropertyListenerBase_t1226442861 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator0_t712175311, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator0_t712175311, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ec__Iterator0_t712175311, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEOUTU3EC__ITERATOR0_T712175311_H
#ifndef DOCTYPES_T77080171_H
#define DOCTYPES_T77080171_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Shared.DocLinks/DocTypes
struct  DocTypes_t77080171 
{
public:
	// System.Int32 Photon.Pun.Demo.Shared.DocLinks/DocTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DocTypes_t77080171, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCTYPES_T77080171_H
#ifndef LANGUAGES_T2208671880_H
#define LANGUAGES_T2208671880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Shared.DocLinks/Languages
struct  Languages_t2208671880 
{
public:
	// System.Int32 Photon.Pun.Demo.Shared.DocLinks/Languages::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Languages_t2208671880, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGES_T2208671880_H
#ifndef PRODUCTS_T1821662994_H
#define PRODUCTS_T1821662994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Shared.DocLinks/Products
struct  Products_t1821662994 
{
public:
	// System.Int32 Photon.Pun.Demo.Shared.DocLinks/Products::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Products_t1821662994, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTS_T1821662994_H
#ifndef VERSIONS_T239389637_H
#define VERSIONS_T239389637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Shared.DocLinks/Versions
struct  Versions_t239389637 
{
public:
	// System.Int32 Photon.Pun.Demo.Shared.DocLinks/Versions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Versions_t239389637, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSIONS_T239389637_H
#ifndef SERVERCONNECTION_T1897300512_H
#define SERVERCONNECTION_T1897300512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Realtime.ServerConnection
struct  ServerConnection_t1897300512 
{
public:
	// System.Int32 Photon.Realtime.ServerConnection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ServerConnection_t1897300512, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERCONNECTION_T1897300512_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DOCLINKS_T1816087656_H
#define DOCLINKS_T1816087656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Shared.DocLinks
struct  DocLinks_t1816087656  : public RuntimeObject
{
public:

public:
};

struct DocLinks_t1816087656_StaticFields
{
public:
	// Photon.Pun.Demo.Shared.DocLinks/Versions Photon.Pun.Demo.Shared.DocLinks::Version
	int32_t ___Version_0;
	// Photon.Pun.Demo.Shared.DocLinks/Languages Photon.Pun.Demo.Shared.DocLinks::Language
	int32_t ___Language_1;
	// Photon.Pun.Demo.Shared.DocLinks/Products Photon.Pun.Demo.Shared.DocLinks::Product
	int32_t ___Product_2;
	// System.String Photon.Pun.Demo.Shared.DocLinks::ApiUrlRoot
	String_t* ___ApiUrlRoot_3;
	// System.String Photon.Pun.Demo.Shared.DocLinks::DocUrlFormat
	String_t* ___DocUrlFormat_4;
	// System.Collections.Generic.Dictionary`2<Photon.Pun.Demo.Shared.DocLinks/Products,System.String> Photon.Pun.Demo.Shared.DocLinks::ProductsFolders
	Dictionary_2_t2878688375 * ___ProductsFolders_5;
	// System.Collections.Generic.Dictionary`2<Photon.Pun.Demo.Shared.DocLinks/Languages,System.String> Photon.Pun.Demo.Shared.DocLinks::ApiLanguagesFolder
	Dictionary_2_t4246196329 * ___ApiLanguagesFolder_6;
	// System.Collections.Generic.Dictionary`2<Photon.Pun.Demo.Shared.DocLinks/Languages,System.String> Photon.Pun.Demo.Shared.DocLinks::DocLanguagesFolder
	Dictionary_2_t4246196329 * ___DocLanguagesFolder_7;
	// System.Collections.Generic.Dictionary`2<Photon.Pun.Demo.Shared.DocLinks/Versions,System.String> Photon.Pun.Demo.Shared.DocLinks::VersionsFolder
	Dictionary_2_t1561956248 * ___VersionsFolder_8;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(DocLinks_t1816087656_StaticFields, ___Version_0)); }
	inline int32_t get_Version_0() const { return ___Version_0; }
	inline int32_t* get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(int32_t value)
	{
		___Version_0 = value;
	}

	inline static int32_t get_offset_of_Language_1() { return static_cast<int32_t>(offsetof(DocLinks_t1816087656_StaticFields, ___Language_1)); }
	inline int32_t get_Language_1() const { return ___Language_1; }
	inline int32_t* get_address_of_Language_1() { return &___Language_1; }
	inline void set_Language_1(int32_t value)
	{
		___Language_1 = value;
	}

	inline static int32_t get_offset_of_Product_2() { return static_cast<int32_t>(offsetof(DocLinks_t1816087656_StaticFields, ___Product_2)); }
	inline int32_t get_Product_2() const { return ___Product_2; }
	inline int32_t* get_address_of_Product_2() { return &___Product_2; }
	inline void set_Product_2(int32_t value)
	{
		___Product_2 = value;
	}

	inline static int32_t get_offset_of_ApiUrlRoot_3() { return static_cast<int32_t>(offsetof(DocLinks_t1816087656_StaticFields, ___ApiUrlRoot_3)); }
	inline String_t* get_ApiUrlRoot_3() const { return ___ApiUrlRoot_3; }
	inline String_t** get_address_of_ApiUrlRoot_3() { return &___ApiUrlRoot_3; }
	inline void set_ApiUrlRoot_3(String_t* value)
	{
		___ApiUrlRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&___ApiUrlRoot_3), value);
	}

	inline static int32_t get_offset_of_DocUrlFormat_4() { return static_cast<int32_t>(offsetof(DocLinks_t1816087656_StaticFields, ___DocUrlFormat_4)); }
	inline String_t* get_DocUrlFormat_4() const { return ___DocUrlFormat_4; }
	inline String_t** get_address_of_DocUrlFormat_4() { return &___DocUrlFormat_4; }
	inline void set_DocUrlFormat_4(String_t* value)
	{
		___DocUrlFormat_4 = value;
		Il2CppCodeGenWriteBarrier((&___DocUrlFormat_4), value);
	}

	inline static int32_t get_offset_of_ProductsFolders_5() { return static_cast<int32_t>(offsetof(DocLinks_t1816087656_StaticFields, ___ProductsFolders_5)); }
	inline Dictionary_2_t2878688375 * get_ProductsFolders_5() const { return ___ProductsFolders_5; }
	inline Dictionary_2_t2878688375 ** get_address_of_ProductsFolders_5() { return &___ProductsFolders_5; }
	inline void set_ProductsFolders_5(Dictionary_2_t2878688375 * value)
	{
		___ProductsFolders_5 = value;
		Il2CppCodeGenWriteBarrier((&___ProductsFolders_5), value);
	}

	inline static int32_t get_offset_of_ApiLanguagesFolder_6() { return static_cast<int32_t>(offsetof(DocLinks_t1816087656_StaticFields, ___ApiLanguagesFolder_6)); }
	inline Dictionary_2_t4246196329 * get_ApiLanguagesFolder_6() const { return ___ApiLanguagesFolder_6; }
	inline Dictionary_2_t4246196329 ** get_address_of_ApiLanguagesFolder_6() { return &___ApiLanguagesFolder_6; }
	inline void set_ApiLanguagesFolder_6(Dictionary_2_t4246196329 * value)
	{
		___ApiLanguagesFolder_6 = value;
		Il2CppCodeGenWriteBarrier((&___ApiLanguagesFolder_6), value);
	}

	inline static int32_t get_offset_of_DocLanguagesFolder_7() { return static_cast<int32_t>(offsetof(DocLinks_t1816087656_StaticFields, ___DocLanguagesFolder_7)); }
	inline Dictionary_2_t4246196329 * get_DocLanguagesFolder_7() const { return ___DocLanguagesFolder_7; }
	inline Dictionary_2_t4246196329 ** get_address_of_DocLanguagesFolder_7() { return &___DocLanguagesFolder_7; }
	inline void set_DocLanguagesFolder_7(Dictionary_2_t4246196329 * value)
	{
		___DocLanguagesFolder_7 = value;
		Il2CppCodeGenWriteBarrier((&___DocLanguagesFolder_7), value);
	}

	inline static int32_t get_offset_of_VersionsFolder_8() { return static_cast<int32_t>(offsetof(DocLinks_t1816087656_StaticFields, ___VersionsFolder_8)); }
	inline Dictionary_2_t1561956248 * get_VersionsFolder_8() const { return ___VersionsFolder_8; }
	inline Dictionary_2_t1561956248 ** get_address_of_VersionsFolder_8() { return &___VersionsFolder_8; }
	inline void set_VersionsFolder_8(Dictionary_2_t1561956248 * value)
	{
		___VersionsFolder_8 = value;
		Il2CppCodeGenWriteBarrier((&___VersionsFolder_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCLINKS_T1816087656_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef FIRSTNUM_T3448397475_H
#define FIRSTNUM_T3448397475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FirstNum
struct  FirstNum_t3448397475  : public MonoBehaviour_t3962482529
{
public:
	// System.String FirstNum::yellowNum
	String_t* ___yellowNum_4;

public:
	inline static int32_t get_offset_of_yellowNum_4() { return static_cast<int32_t>(offsetof(FirstNum_t3448397475, ___yellowNum_4)); }
	inline String_t* get_yellowNum_4() const { return ___yellowNum_4; }
	inline String_t** get_address_of_yellowNum_4() { return &___yellowNum_4; }
	inline void set_yellowNum_4(String_t* value)
	{
		___yellowNum_4 = value;
		Il2CppCodeGenWriteBarrier((&___yellowNum_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIRSTNUM_T3448397475_H
#ifndef HOMESCRIPT_T3660720983_H
#define HOMESCRIPT_T3660720983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeScript
struct  HomeScript_t3660720983  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject HomeScript::resultManager
	GameObject_t1113636619 * ___resultManager_4;
	// ResultManager HomeScript::resultManagerScript
	ResultManager_t2208194647 * ___resultManagerScript_5;

public:
	inline static int32_t get_offset_of_resultManager_4() { return static_cast<int32_t>(offsetof(HomeScript_t3660720983, ___resultManager_4)); }
	inline GameObject_t1113636619 * get_resultManager_4() const { return ___resultManager_4; }
	inline GameObject_t1113636619 ** get_address_of_resultManager_4() { return &___resultManager_4; }
	inline void set_resultManager_4(GameObject_t1113636619 * value)
	{
		___resultManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___resultManager_4), value);
	}

	inline static int32_t get_offset_of_resultManagerScript_5() { return static_cast<int32_t>(offsetof(HomeScript_t3660720983, ___resultManagerScript_5)); }
	inline ResultManager_t2208194647 * get_resultManagerScript_5() const { return ___resultManagerScript_5; }
	inline ResultManager_t2208194647 ** get_address_of_resultManagerScript_5() { return &___resultManagerScript_5; }
	inline void set_resultManagerScript_5(ResultManager_t2208194647 * value)
	{
		___resultManagerScript_5 = value;
		Il2CppCodeGenWriteBarrier((&___resultManagerScript_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOMESCRIPT_T3660720983_H
#ifndef APPVERSIONPROPERTY_T243272796_H
#define APPVERSIONPROPERTY_T243272796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.AppVersionProperty
struct  AppVersionProperty_t243272796  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.AppVersionProperty::Text
	Text_t1901882714 * ___Text_4;
	// System.String Photon.Pun.Demo.Cockpit.AppVersionProperty::_cache
	String_t* ____cache_5;

public:
	inline static int32_t get_offset_of_Text_4() { return static_cast<int32_t>(offsetof(AppVersionProperty_t243272796, ___Text_4)); }
	inline Text_t1901882714 * get_Text_4() const { return ___Text_4; }
	inline Text_t1901882714 ** get_address_of_Text_4() { return &___Text_4; }
	inline void set_Text_4(Text_t1901882714 * value)
	{
		___Text_4 = value;
		Il2CppCodeGenWriteBarrier((&___Text_4), value);
	}

	inline static int32_t get_offset_of__cache_5() { return static_cast<int32_t>(offsetof(AppVersionProperty_t243272796, ____cache_5)); }
	inline String_t* get__cache_5() const { return ____cache_5; }
	inline String_t** get_address_of__cache_5() { return &____cache_5; }
	inline void set__cache_5(String_t* value)
	{
		____cache_5 = value;
		Il2CppCodeGenWriteBarrier((&____cache_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPVERSIONPROPERTY_T243272796_H
#ifndef GAMEVERSIONPROPERTY_T2599490529_H
#define GAMEVERSIONPROPERTY_T2599490529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.GameVersionProperty
struct  GameVersionProperty_t2599490529  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.GameVersionProperty::Text
	Text_t1901882714 * ___Text_4;
	// System.String Photon.Pun.Demo.Cockpit.GameVersionProperty::_cache
	String_t* ____cache_5;

public:
	inline static int32_t get_offset_of_Text_4() { return static_cast<int32_t>(offsetof(GameVersionProperty_t2599490529, ___Text_4)); }
	inline Text_t1901882714 * get_Text_4() const { return ___Text_4; }
	inline Text_t1901882714 ** get_address_of_Text_4() { return &___Text_4; }
	inline void set_Text_4(Text_t1901882714 * value)
	{
		___Text_4 = value;
		Il2CppCodeGenWriteBarrier((&___Text_4), value);
	}

	inline static int32_t get_offset_of__cache_5() { return static_cast<int32_t>(offsetof(GameVersionProperty_t2599490529, ____cache_5)); }
	inline String_t* get__cache_5() const { return ____cache_5; }
	inline String_t** get_address_of__cache_5() { return &____cache_5; }
	inline void set__cache_5(String_t* value)
	{
		____cache_5 = value;
		Il2CppCodeGenWriteBarrier((&____cache_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEVERSIONPROPERTY_T2599490529_H
#ifndef PROPERTYLISTENERBASE_T1226442861_H
#define PROPERTYLISTENERBASE_T1226442861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.PropertyListenerBase
struct  PropertyListenerBase_t1226442861  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Graphic Photon.Pun.Demo.Cockpit.PropertyListenerBase::UpdateIndicator
	Graphic_t1660335611 * ___UpdateIndicator_4;
	// UnityEngine.YieldInstruction Photon.Pun.Demo.Cockpit.PropertyListenerBase::fadeInstruction
	YieldInstruction_t403091072 * ___fadeInstruction_5;
	// System.Single Photon.Pun.Demo.Cockpit.PropertyListenerBase::Duration
	float ___Duration_6;

public:
	inline static int32_t get_offset_of_UpdateIndicator_4() { return static_cast<int32_t>(offsetof(PropertyListenerBase_t1226442861, ___UpdateIndicator_4)); }
	inline Graphic_t1660335611 * get_UpdateIndicator_4() const { return ___UpdateIndicator_4; }
	inline Graphic_t1660335611 ** get_address_of_UpdateIndicator_4() { return &___UpdateIndicator_4; }
	inline void set_UpdateIndicator_4(Graphic_t1660335611 * value)
	{
		___UpdateIndicator_4 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateIndicator_4), value);
	}

	inline static int32_t get_offset_of_fadeInstruction_5() { return static_cast<int32_t>(offsetof(PropertyListenerBase_t1226442861, ___fadeInstruction_5)); }
	inline YieldInstruction_t403091072 * get_fadeInstruction_5() const { return ___fadeInstruction_5; }
	inline YieldInstruction_t403091072 ** get_address_of_fadeInstruction_5() { return &___fadeInstruction_5; }
	inline void set_fadeInstruction_5(YieldInstruction_t403091072 * value)
	{
		___fadeInstruction_5 = value;
		Il2CppCodeGenWriteBarrier((&___fadeInstruction_5), value);
	}

	inline static int32_t get_offset_of_Duration_6() { return static_cast<int32_t>(offsetof(PropertyListenerBase_t1226442861, ___Duration_6)); }
	inline float get_Duration_6() const { return ___Duration_6; }
	inline float* get_address_of_Duration_6() { return &___Duration_6; }
	inline void set_Duration_6(float value)
	{
		___Duration_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYLISTENERBASE_T1226442861_H
#ifndef SERVERADDRESSPROPERTY_T960619058_H
#define SERVERADDRESSPROPERTY_T960619058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.ServerAddressProperty
struct  ServerAddressProperty_t960619058  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.ServerAddressProperty::Text
	Text_t1901882714 * ___Text_4;
	// System.String Photon.Pun.Demo.Cockpit.ServerAddressProperty::_cache
	String_t* ____cache_5;

public:
	inline static int32_t get_offset_of_Text_4() { return static_cast<int32_t>(offsetof(ServerAddressProperty_t960619058, ___Text_4)); }
	inline Text_t1901882714 * get_Text_4() const { return ___Text_4; }
	inline Text_t1901882714 ** get_address_of_Text_4() { return &___Text_4; }
	inline void set_Text_4(Text_t1901882714 * value)
	{
		___Text_4 = value;
		Il2CppCodeGenWriteBarrier((&___Text_4), value);
	}

	inline static int32_t get_offset_of__cache_5() { return static_cast<int32_t>(offsetof(ServerAddressProperty_t960619058, ____cache_5)); }
	inline String_t* get__cache_5() const { return ____cache_5; }
	inline String_t** get_address_of__cache_5() { return &____cache_5; }
	inline void set__cache_5(String_t* value)
	{
		____cache_5 = value;
		Il2CppCodeGenWriteBarrier((&____cache_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERADDRESSPROPERTY_T960619058_H
#ifndef USERIDFIELD_T3654608899_H
#define USERIDFIELD_T3654608899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.UserIdField
struct  UserIdField_t3654608899  : public MonoBehaviour_t3962482529
{
public:
	// Photon.Pun.Demo.Cockpit.PunCockpit Photon.Pun.Demo.Cockpit.UserIdField::Manager
	PunCockpit_t3722300818 * ___Manager_4;
	// UnityEngine.UI.InputField Photon.Pun.Demo.Cockpit.UserIdField::PropertyValueInput
	InputField_t3762917431 * ___PropertyValueInput_5;
	// System.String Photon.Pun.Demo.Cockpit.UserIdField::_cache
	String_t* ____cache_6;
	// System.Boolean Photon.Pun.Demo.Cockpit.UserIdField::registered
	bool ___registered_7;

public:
	inline static int32_t get_offset_of_Manager_4() { return static_cast<int32_t>(offsetof(UserIdField_t3654608899, ___Manager_4)); }
	inline PunCockpit_t3722300818 * get_Manager_4() const { return ___Manager_4; }
	inline PunCockpit_t3722300818 ** get_address_of_Manager_4() { return &___Manager_4; }
	inline void set_Manager_4(PunCockpit_t3722300818 * value)
	{
		___Manager_4 = value;
		Il2CppCodeGenWriteBarrier((&___Manager_4), value);
	}

	inline static int32_t get_offset_of_PropertyValueInput_5() { return static_cast<int32_t>(offsetof(UserIdField_t3654608899, ___PropertyValueInput_5)); }
	inline InputField_t3762917431 * get_PropertyValueInput_5() const { return ___PropertyValueInput_5; }
	inline InputField_t3762917431 ** get_address_of_PropertyValueInput_5() { return &___PropertyValueInput_5; }
	inline void set_PropertyValueInput_5(InputField_t3762917431 * value)
	{
		___PropertyValueInput_5 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyValueInput_5), value);
	}

	inline static int32_t get_offset_of__cache_6() { return static_cast<int32_t>(offsetof(UserIdField_t3654608899, ____cache_6)); }
	inline String_t* get__cache_6() const { return ____cache_6; }
	inline String_t** get_address_of__cache_6() { return &____cache_6; }
	inline void set__cache_6(String_t* value)
	{
		____cache_6 = value;
		Il2CppCodeGenWriteBarrier((&____cache_6), value);
	}

	inline static int32_t get_offset_of_registered_7() { return static_cast<int32_t>(offsetof(UserIdField_t3654608899, ___registered_7)); }
	inline bool get_registered_7() const { return ___registered_7; }
	inline bool* get_address_of_registered_7() { return &___registered_7; }
	inline void set_registered_7(bool value)
	{
		___registered_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERIDFIELD_T3654608899_H
#ifndef MONOBEHAVIOURPUN_T1682334777_H
#define MONOBEHAVIOURPUN_T1682334777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.MonoBehaviourPun
struct  MonoBehaviourPun_t1682334777  : public MonoBehaviour_t3962482529
{
public:
	// Photon.Pun.PhotonView Photon.Pun.MonoBehaviourPun::pvCache
	PhotonView_t3684715584 * ___pvCache_4;

public:
	inline static int32_t get_offset_of_pvCache_4() { return static_cast<int32_t>(offsetof(MonoBehaviourPun_t1682334777, ___pvCache_4)); }
	inline PhotonView_t3684715584 * get_pvCache_4() const { return ___pvCache_4; }
	inline PhotonView_t3684715584 ** get_address_of_pvCache_4() { return &___pvCache_4; }
	inline void set_pvCache_4(PhotonView_t3684715584 * value)
	{
		___pvCache_4 = value;
		Il2CppCodeGenWriteBarrier((&___pvCache_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOURPUN_T1682334777_H
#ifndef RESULTMANAGER_T2208194647_H
#define RESULTMANAGER_T2208194647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultManager
struct  ResultManager_t2208194647  : public MonoBehaviour_t3962482529
{
public:
	// System.String ResultManager::resultCoad
	String_t* ___resultCoad_4;

public:
	inline static int32_t get_offset_of_resultCoad_4() { return static_cast<int32_t>(offsetof(ResultManager_t2208194647, ___resultCoad_4)); }
	inline String_t* get_resultCoad_4() const { return ___resultCoad_4; }
	inline String_t** get_address_of_resultCoad_4() { return &___resultCoad_4; }
	inline void set_resultCoad_4(String_t* value)
	{
		___resultCoad_4 = value;
		Il2CppCodeGenWriteBarrier((&___resultCoad_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTMANAGER_T2208194647_H
#ifndef RESULTSCRIPT_T2333723299_H
#define RESULTSCRIPT_T2333723299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultScript
struct  ResultScript_t2333723299  : public MonoBehaviour_t3962482529
{
public:
	// System.String ResultScript::resultCoad
	String_t* ___resultCoad_4;
	// UnityEngine.UI.Text ResultScript::resultText
	Text_t1901882714 * ___resultText_5;
	// UnityEngine.GameObject ResultScript::resultManager
	GameObject_t1113636619 * ___resultManager_6;
	// ResultManager ResultScript::resultManagerScript
	ResultManager_t2208194647 * ___resultManagerScript_7;

public:
	inline static int32_t get_offset_of_resultCoad_4() { return static_cast<int32_t>(offsetof(ResultScript_t2333723299, ___resultCoad_4)); }
	inline String_t* get_resultCoad_4() const { return ___resultCoad_4; }
	inline String_t** get_address_of_resultCoad_4() { return &___resultCoad_4; }
	inline void set_resultCoad_4(String_t* value)
	{
		___resultCoad_4 = value;
		Il2CppCodeGenWriteBarrier((&___resultCoad_4), value);
	}

	inline static int32_t get_offset_of_resultText_5() { return static_cast<int32_t>(offsetof(ResultScript_t2333723299, ___resultText_5)); }
	inline Text_t1901882714 * get_resultText_5() const { return ___resultText_5; }
	inline Text_t1901882714 ** get_address_of_resultText_5() { return &___resultText_5; }
	inline void set_resultText_5(Text_t1901882714 * value)
	{
		___resultText_5 = value;
		Il2CppCodeGenWriteBarrier((&___resultText_5), value);
	}

	inline static int32_t get_offset_of_resultManager_6() { return static_cast<int32_t>(offsetof(ResultScript_t2333723299, ___resultManager_6)); }
	inline GameObject_t1113636619 * get_resultManager_6() const { return ___resultManager_6; }
	inline GameObject_t1113636619 ** get_address_of_resultManager_6() { return &___resultManager_6; }
	inline void set_resultManager_6(GameObject_t1113636619 * value)
	{
		___resultManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___resultManager_6), value);
	}

	inline static int32_t get_offset_of_resultManagerScript_7() { return static_cast<int32_t>(offsetof(ResultScript_t2333723299, ___resultManagerScript_7)); }
	inline ResultManager_t2208194647 * get_resultManagerScript_7() const { return ___resultManagerScript_7; }
	inline ResultManager_t2208194647 ** get_address_of_resultManagerScript_7() { return &___resultManagerScript_7; }
	inline void set_resultManagerScript_7(ResultManager_t2208194647 * value)
	{
		___resultManagerScript_7 = value;
		Il2CppCodeGenWriteBarrier((&___resultManagerScript_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTSCRIPT_T2333723299_H
#ifndef RETRYSCRIPT_T1070381244_H
#define RETRYSCRIPT_T1070381244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RetryScript
struct  RetryScript_t1070381244  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject RetryScript::resultManager
	GameObject_t1113636619 * ___resultManager_4;
	// ResultManager RetryScript::resultManagerScript
	ResultManager_t2208194647 * ___resultManagerScript_5;

public:
	inline static int32_t get_offset_of_resultManager_4() { return static_cast<int32_t>(offsetof(RetryScript_t1070381244, ___resultManager_4)); }
	inline GameObject_t1113636619 * get_resultManager_4() const { return ___resultManager_4; }
	inline GameObject_t1113636619 ** get_address_of_resultManager_4() { return &___resultManager_4; }
	inline void set_resultManager_4(GameObject_t1113636619 * value)
	{
		___resultManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___resultManager_4), value);
	}

	inline static int32_t get_offset_of_resultManagerScript_5() { return static_cast<int32_t>(offsetof(RetryScript_t1070381244, ___resultManagerScript_5)); }
	inline ResultManager_t2208194647 * get_resultManagerScript_5() const { return ___resultManagerScript_5; }
	inline ResultManager_t2208194647 ** get_address_of_resultManagerScript_5() { return &___resultManagerScript_5; }
	inline void set_resultManagerScript_5(ResultManager_t2208194647 * value)
	{
		___resultManagerScript_5 = value;
		Il2CppCodeGenWriteBarrier((&___resultManagerScript_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETRYSCRIPT_T1070381244_H
#ifndef SCOREHELPER_T1549971229_H
#define SCOREHELPER_T1549971229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreHelper
struct  ScoreHelper_t1549971229  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 ScoreHelper::Score
	int32_t ___Score_4;
	// System.Int32 ScoreHelper::_currentScore
	int32_t ____currentScore_5;

public:
	inline static int32_t get_offset_of_Score_4() { return static_cast<int32_t>(offsetof(ScoreHelper_t1549971229, ___Score_4)); }
	inline int32_t get_Score_4() const { return ___Score_4; }
	inline int32_t* get_address_of_Score_4() { return &___Score_4; }
	inline void set_Score_4(int32_t value)
	{
		___Score_4 = value;
	}

	inline static int32_t get_offset_of__currentScore_5() { return static_cast<int32_t>(offsetof(ScoreHelper_t1549971229, ____currentScore_5)); }
	inline int32_t get__currentScore_5() const { return ____currentScore_5; }
	inline int32_t* get_address_of__currentScore_5() { return &____currentScore_5; }
	inline void set__currentScore_5(int32_t value)
	{
		____currentScore_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOREHELPER_T1549971229_H
#ifndef SECONDNUM_T3374771210_H
#define SECONDNUM_T3374771210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SecondNum
struct  SecondNum_t3374771210  : public MonoBehaviour_t3962482529
{
public:
	// System.String SecondNum::yellowNum
	String_t* ___yellowNum_4;

public:
	inline static int32_t get_offset_of_yellowNum_4() { return static_cast<int32_t>(offsetof(SecondNum_t3374771210, ___yellowNum_4)); }
	inline String_t* get_yellowNum_4() const { return ___yellowNum_4; }
	inline String_t** get_address_of_yellowNum_4() { return &___yellowNum_4; }
	inline void set_yellowNum_4(String_t* value)
	{
		___yellowNum_4 = value;
		Il2CppCodeGenWriteBarrier((&___yellowNum_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECONDNUM_T3374771210_H
#ifndef SOMEBODYSCRIPT_T208421239_H
#define SOMEBODYSCRIPT_T208421239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SomebodyScript
struct  SomebodyScript_t208421239  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOMEBODYSCRIPT_T208421239_H
#ifndef THIRDNUM_T2751324300_H
#define THIRDNUM_T2751324300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdNum
struct  ThirdNum_t2751324300  : public MonoBehaviour_t3962482529
{
public:
	// System.String ThirdNum::yellowNum
	String_t* ___yellowNum_4;

public:
	inline static int32_t get_offset_of_yellowNum_4() { return static_cast<int32_t>(offsetof(ThirdNum_t2751324300, ___yellowNum_4)); }
	inline String_t* get_yellowNum_4() const { return ___yellowNum_4; }
	inline String_t** get_address_of_yellowNum_4() { return &___yellowNum_4; }
	inline void set_yellowNum_4(String_t* value)
	{
		___yellowNum_4 = value;
		Il2CppCodeGenWriteBarrier((&___yellowNum_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDNUM_T2751324300_H
#ifndef TIMERSCRIPT_T1618777791_H
#define TIMERSCRIPT_T1618777791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimerScript
struct  TimerScript_t1618777791  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text TimerScript::timerText
	Text_t1901882714 * ___timerText_4;
	// System.Single TimerScript::timer
	float ___timer_5;
	// System.Boolean TimerScript::isStoped
	bool ___isStoped_6;

public:
	inline static int32_t get_offset_of_timerText_4() { return static_cast<int32_t>(offsetof(TimerScript_t1618777791, ___timerText_4)); }
	inline Text_t1901882714 * get_timerText_4() const { return ___timerText_4; }
	inline Text_t1901882714 ** get_address_of_timerText_4() { return &___timerText_4; }
	inline void set_timerText_4(Text_t1901882714 * value)
	{
		___timerText_4 = value;
		Il2CppCodeGenWriteBarrier((&___timerText_4), value);
	}

	inline static int32_t get_offset_of_timer_5() { return static_cast<int32_t>(offsetof(TimerScript_t1618777791, ___timer_5)); }
	inline float get_timer_5() const { return ___timer_5; }
	inline float* get_address_of_timer_5() { return &___timer_5; }
	inline void set_timer_5(float value)
	{
		___timer_5 = value;
	}

	inline static int32_t get_offset_of_isStoped_6() { return static_cast<int32_t>(offsetof(TimerScript_t1618777791, ___isStoped_6)); }
	inline bool get_isStoped_6() const { return ___isStoped_6; }
	inline bool* get_address_of_isStoped_6() { return &___isStoped_6; }
	inline void set_isStoped_6(bool value)
	{
		___isStoped_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERSCRIPT_T1618777791_H
#ifndef TURNSCRIPT_T2279615295_H
#define TURNSCRIPT_T2279615295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TurnScript
struct  TurnScript_t2279615295  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TurnScript::turnCount
	int32_t ___turnCount_4;
	// UnityEngine.UI.Text TurnScript::turnText
	Text_t1901882714 * ___turnText_5;

public:
	inline static int32_t get_offset_of_turnCount_4() { return static_cast<int32_t>(offsetof(TurnScript_t2279615295, ___turnCount_4)); }
	inline int32_t get_turnCount_4() const { return ___turnCount_4; }
	inline int32_t* get_address_of_turnCount_4() { return &___turnCount_4; }
	inline void set_turnCount_4(int32_t value)
	{
		___turnCount_4 = value;
	}

	inline static int32_t get_offset_of_turnText_5() { return static_cast<int32_t>(offsetof(TurnScript_t2279615295, ___turnText_5)); }
	inline Text_t1901882714 * get_turnText_5() const { return ___turnText_5; }
	inline Text_t1901882714 ** get_address_of_turnText_5() { return &___turnText_5; }
	inline void set_turnText_5(Text_t1901882714 * value)
	{
		___turnText_5 = value;
		Il2CppCodeGenWriteBarrier((&___turnText_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TURNSCRIPT_T2279615295_H
#ifndef YOURNUM_T2796948509_H
#define YOURNUM_T2796948509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YourNum
struct  YourNum_t2796948509  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text YourNum::yourNumText
	Text_t1901882714 * ___yourNumText_4;
	// System.String YourNum::yourFirstNum
	String_t* ___yourFirstNum_5;
	// System.String YourNum::yourSecondNum
	String_t* ___yourSecondNum_6;
	// System.String YourNum::yourThirdNum
	String_t* ___yourThirdNum_7;

public:
	inline static int32_t get_offset_of_yourNumText_4() { return static_cast<int32_t>(offsetof(YourNum_t2796948509, ___yourNumText_4)); }
	inline Text_t1901882714 * get_yourNumText_4() const { return ___yourNumText_4; }
	inline Text_t1901882714 ** get_address_of_yourNumText_4() { return &___yourNumText_4; }
	inline void set_yourNumText_4(Text_t1901882714 * value)
	{
		___yourNumText_4 = value;
		Il2CppCodeGenWriteBarrier((&___yourNumText_4), value);
	}

	inline static int32_t get_offset_of_yourFirstNum_5() { return static_cast<int32_t>(offsetof(YourNum_t2796948509, ___yourFirstNum_5)); }
	inline String_t* get_yourFirstNum_5() const { return ___yourFirstNum_5; }
	inline String_t** get_address_of_yourFirstNum_5() { return &___yourFirstNum_5; }
	inline void set_yourFirstNum_5(String_t* value)
	{
		___yourFirstNum_5 = value;
		Il2CppCodeGenWriteBarrier((&___yourFirstNum_5), value);
	}

	inline static int32_t get_offset_of_yourSecondNum_6() { return static_cast<int32_t>(offsetof(YourNum_t2796948509, ___yourSecondNum_6)); }
	inline String_t* get_yourSecondNum_6() const { return ___yourSecondNum_6; }
	inline String_t** get_address_of_yourSecondNum_6() { return &___yourSecondNum_6; }
	inline void set_yourSecondNum_6(String_t* value)
	{
		___yourSecondNum_6 = value;
		Il2CppCodeGenWriteBarrier((&___yourSecondNum_6), value);
	}

	inline static int32_t get_offset_of_yourThirdNum_7() { return static_cast<int32_t>(offsetof(YourNum_t2796948509, ___yourThirdNum_7)); }
	inline String_t* get_yourThirdNum_7() const { return ___yourThirdNum_7; }
	inline String_t** get_address_of_yourThirdNum_7() { return &___yourThirdNum_7; }
	inline void set_yourThirdNum_7(String_t* value)
	{
		___yourThirdNum_7 = value;
		Il2CppCodeGenWriteBarrier((&___yourThirdNum_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOURNUM_T2796948509_H
#ifndef BESTREGIONINPREFSPROPERTY_T40435295_H
#define BESTREGIONINPREFSPROPERTY_T40435295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.BestRegionInPrefsProperty
struct  BestRegionInPrefsProperty_t40435295  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.BestRegionInPrefsProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.String Photon.Pun.Demo.Cockpit.BestRegionInPrefsProperty::_cache
	String_t* ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(BestRegionInPrefsProperty_t40435295, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(BestRegionInPrefsProperty_t40435295, ____cache_8)); }
	inline String_t* get__cache_8() const { return ____cache_8; }
	inline String_t** get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(String_t* value)
	{
		____cache_8 = value;
		Il2CppCodeGenWriteBarrier((&____cache_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BESTREGIONINPREFSPROPERTY_T40435295_H
#ifndef CLOUDREGIONPROPERTY_T826945682_H
#define CLOUDREGIONPROPERTY_T826945682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CloudRegionProperty
struct  CloudRegionProperty_t826945682  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CloudRegionProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.String Photon.Pun.Demo.Cockpit.CloudRegionProperty::_cache
	String_t* ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CloudRegionProperty_t826945682, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CloudRegionProperty_t826945682, ____cache_8)); }
	inline String_t* get__cache_8() const { return ____cache_8; }
	inline String_t** get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(String_t* value)
	{
		____cache_8 = value;
		Il2CppCodeGenWriteBarrier((&____cache_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDREGIONPROPERTY_T826945682_H
#ifndef COUNTOFPLAYERSINROOMPROPERTY_T2018334201_H
#define COUNTOFPLAYERSINROOMPROPERTY_T2018334201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CountOfPlayersInRoomProperty
struct  CountOfPlayersInRoomProperty_t2018334201  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CountOfPlayersInRoomProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.CountOfPlayersInRoomProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CountOfPlayersInRoomProperty_t2018334201, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CountOfPlayersInRoomProperty_t2018334201, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTOFPLAYERSINROOMPROPERTY_T2018334201_H
#ifndef COUNTOFPLAYERSONMASTERPROPERTY_T2428894331_H
#define COUNTOFPLAYERSONMASTERPROPERTY_T2428894331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CountOfPlayersOnMasterProperty
struct  CountOfPlayersOnMasterProperty_t2428894331  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CountOfPlayersOnMasterProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.CountOfPlayersOnMasterProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CountOfPlayersOnMasterProperty_t2428894331, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CountOfPlayersOnMasterProperty_t2428894331, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTOFPLAYERSONMASTERPROPERTY_T2428894331_H
#ifndef COUNTOFPLAYERSPROPERTY_T1754900899_H
#define COUNTOFPLAYERSPROPERTY_T1754900899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CountOfPlayersProperty
struct  CountOfPlayersProperty_t1754900899  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CountOfPlayersProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.CountOfPlayersProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CountOfPlayersProperty_t1754900899, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CountOfPlayersProperty_t1754900899, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTOFPLAYERSPROPERTY_T1754900899_H
#ifndef COUNTOFROOMSPROPERTY_T250151811_H
#define COUNTOFROOMSPROPERTY_T250151811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CountOfRoomsProperty
struct  CountOfRoomsProperty_t250151811  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CountOfRoomsProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.CountOfRoomsProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CountOfRoomsProperty_t250151811, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CountOfRoomsProperty_t250151811, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTOFROOMSPROPERTY_T250151811_H
#ifndef CURRENTROOMAUTOCLEANUPPROPERTY_T559513298_H
#define CURRENTROOMAUTOCLEANUPPROPERTY_T559513298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CurrentRoomAutoCleanupProperty
struct  CurrentRoomAutoCleanupProperty_t559513298  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CurrentRoomAutoCleanupProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.CurrentRoomAutoCleanupProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CurrentRoomAutoCleanupProperty_t559513298, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CurrentRoomAutoCleanupProperty_t559513298, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTROOMAUTOCLEANUPPROPERTY_T559513298_H
#ifndef CURRENTROOMEMPTYROOMTTLPROPERTY_T2509994768_H
#define CURRENTROOMEMPTYROOMTTLPROPERTY_T2509994768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CurrentRoomEmptyRoomTtlProperty
struct  CurrentRoomEmptyRoomTtlProperty_t2509994768  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CurrentRoomEmptyRoomTtlProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.CurrentRoomEmptyRoomTtlProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CurrentRoomEmptyRoomTtlProperty_t2509994768, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CurrentRoomEmptyRoomTtlProperty_t2509994768, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTROOMEMPTYROOMTTLPROPERTY_T2509994768_H
#ifndef CURRENTROOMEXPECTEDUSERSPROPERTY_T941134229_H
#define CURRENTROOMEXPECTEDUSERSPROPERTY_T941134229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CurrentRoomExpectedUsersProperty
struct  CurrentRoomExpectedUsersProperty_t941134229  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CurrentRoomExpectedUsersProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.String[] Photon.Pun.Demo.Cockpit.CurrentRoomExpectedUsersProperty::_cache
	StringU5BU5D_t1281789340* ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CurrentRoomExpectedUsersProperty_t941134229, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CurrentRoomExpectedUsersProperty_t941134229, ____cache_8)); }
	inline StringU5BU5D_t1281789340* get__cache_8() const { return ____cache_8; }
	inline StringU5BU5D_t1281789340** get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(StringU5BU5D_t1281789340* value)
	{
		____cache_8 = value;
		Il2CppCodeGenWriteBarrier((&____cache_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTROOMEXPECTEDUSERSPROPERTY_T941134229_H
#ifndef CURRENTROOMISOFFLINEPROPERTY_T1978210793_H
#define CURRENTROOMISOFFLINEPROPERTY_T1978210793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CurrentRoomIsOfflineProperty
struct  CurrentRoomIsOfflineProperty_t1978210793  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CurrentRoomIsOfflineProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.CurrentRoomIsOfflineProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CurrentRoomIsOfflineProperty_t1978210793, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CurrentRoomIsOfflineProperty_t1978210793, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTROOMISOFFLINEPROPERTY_T1978210793_H
#ifndef CURRENTROOMISOPENPROPERTY_T3485205974_H
#define CURRENTROOMISOPENPROPERTY_T3485205974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CurrentRoomIsOpenProperty
struct  CurrentRoomIsOpenProperty_t3485205974  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CurrentRoomIsOpenProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.CurrentRoomIsOpenProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CurrentRoomIsOpenProperty_t3485205974, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CurrentRoomIsOpenProperty_t3485205974, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTROOMISOPENPROPERTY_T3485205974_H
#ifndef CURRENTROOMISVISIBLEPROPERTY_T2609348605_H
#define CURRENTROOMISVISIBLEPROPERTY_T2609348605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CurrentRoomIsVisibleProperty
struct  CurrentRoomIsVisibleProperty_t2609348605  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CurrentRoomIsVisibleProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.CurrentRoomIsVisibleProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CurrentRoomIsVisibleProperty_t2609348605, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CurrentRoomIsVisibleProperty_t2609348605, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTROOMISVISIBLEPROPERTY_T2609348605_H
#ifndef CURRENTROOMMASTERCLIENTIDPROPERTY_T441479020_H
#define CURRENTROOMMASTERCLIENTIDPROPERTY_T441479020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CurrentRoomMasterClientIdProperty
struct  CurrentRoomMasterClientIdProperty_t441479020  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CurrentRoomMasterClientIdProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.CurrentRoomMasterClientIdProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CurrentRoomMasterClientIdProperty_t441479020, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CurrentRoomMasterClientIdProperty_t441479020, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTROOMMASTERCLIENTIDPROPERTY_T441479020_H
#ifndef CURRENTROOMMAXPLAYERSPROPERTY_T269097822_H
#define CURRENTROOMMAXPLAYERSPROPERTY_T269097822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CurrentRoomMaxPlayersProperty
struct  CurrentRoomMaxPlayersProperty_t269097822  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CurrentRoomMaxPlayersProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.CurrentRoomMaxPlayersProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CurrentRoomMaxPlayersProperty_t269097822, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CurrentRoomMaxPlayersProperty_t269097822, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTROOMMAXPLAYERSPROPERTY_T269097822_H
#ifndef CURRENTROOMNAMEPROPERTY_T1589573469_H
#define CURRENTROOMNAMEPROPERTY_T1589573469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CurrentRoomNameProperty
struct  CurrentRoomNameProperty_t1589573469  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CurrentRoomNameProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.String Photon.Pun.Demo.Cockpit.CurrentRoomNameProperty::_cache
	String_t* ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CurrentRoomNameProperty_t1589573469, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CurrentRoomNameProperty_t1589573469, ____cache_8)); }
	inline String_t* get__cache_8() const { return ____cache_8; }
	inline String_t** get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(String_t* value)
	{
		____cache_8 = value;
		Il2CppCodeGenWriteBarrier((&____cache_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTROOMNAMEPROPERTY_T1589573469_H
#ifndef CURRENTROOMPLAYERCOUNTPROPERTY_T895675237_H
#define CURRENTROOMPLAYERCOUNTPROPERTY_T895675237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CurrentRoomPlayerCountProperty
struct  CurrentRoomPlayerCountProperty_t895675237  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CurrentRoomPlayerCountProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.CurrentRoomPlayerCountProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CurrentRoomPlayerCountProperty_t895675237, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CurrentRoomPlayerCountProperty_t895675237, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTROOMPLAYERCOUNTPROPERTY_T895675237_H
#ifndef CURRENTROOMPLAYERTTLPROPERTY_T197288263_H
#define CURRENTROOMPLAYERTTLPROPERTY_T197288263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CurrentRoomPlayerTtlProperty
struct  CurrentRoomPlayerTtlProperty_t197288263  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CurrentRoomPlayerTtlProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.CurrentRoomPlayerTtlProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CurrentRoomPlayerTtlProperty_t197288263, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CurrentRoomPlayerTtlProperty_t197288263, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTROOMPLAYERTTLPROPERTY_T197288263_H
#ifndef CURRENTROOMPROPERTIESLISTEDINLOBBYPROPERTY_T1065004439_H
#define CURRENTROOMPROPERTIESLISTEDINLOBBYPROPERTY_T1065004439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.CurrentRoomPropertiesListedInLobbyProperty
struct  CurrentRoomPropertiesListedInLobbyProperty_t1065004439  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.CurrentRoomPropertiesListedInLobbyProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.String[] Photon.Pun.Demo.Cockpit.CurrentRoomPropertiesListedInLobbyProperty::_cache
	StringU5BU5D_t1281789340* ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(CurrentRoomPropertiesListedInLobbyProperty_t1065004439, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(CurrentRoomPropertiesListedInLobbyProperty_t1065004439, ____cache_8)); }
	inline StringU5BU5D_t1281789340* get__cache_8() const { return ____cache_8; }
	inline StringU5BU5D_t1281789340** get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(StringU5BU5D_t1281789340* value)
	{
		____cache_8 = value;
		Il2CppCodeGenWriteBarrier((&____cache_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTROOMPROPERTIESLISTEDINLOBBYPROPERTY_T1065004439_H
#ifndef ISCONNECTEDANDREADYPROPERTY_T2186703361_H
#define ISCONNECTEDANDREADYPROPERTY_T2186703361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.IsConnectedAndReadyProperty
struct  IsConnectedAndReadyProperty_t2186703361  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.IsConnectedAndReadyProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.IsConnectedAndReadyProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(IsConnectedAndReadyProperty_t2186703361, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(IsConnectedAndReadyProperty_t2186703361, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISCONNECTEDANDREADYPROPERTY_T2186703361_H
#ifndef ISCONNECTEDPROPERTY_T3457877926_H
#define ISCONNECTEDPROPERTY_T3457877926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.IsConnectedProperty
struct  IsConnectedProperty_t3457877926  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.IsConnectedProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.IsConnectedProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(IsConnectedProperty_t3457877926, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(IsConnectedProperty_t3457877926, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISCONNECTEDPROPERTY_T3457877926_H
#ifndef OFFLINEMODEPROPERTY_T4178522675_H
#define OFFLINEMODEPROPERTY_T4178522675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.OfflineModeProperty
struct  OfflineModeProperty_t4178522675  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.OfflineModeProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.OfflineModeProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(OfflineModeProperty_t4178522675, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(OfflineModeProperty_t4178522675, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OFFLINEMODEPROPERTY_T4178522675_H
#ifndef PINGPROPERTY_T304019737_H
#define PINGPROPERTY_T304019737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.PingProperty
struct  PingProperty_t304019737  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.PingProperty::Text
	Text_t1901882714 * ___Text_7;
	// System.Int32 Photon.Pun.Demo.Cockpit.PingProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(PingProperty_t304019737, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(PingProperty_t304019737, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINGPROPERTY_T304019737_H
#ifndef SERVERPROPERTY_T2037082094_H
#define SERVERPROPERTY_T2037082094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.ServerProperty
struct  ServerProperty_t2037082094  : public PropertyListenerBase_t1226442861
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.ServerProperty::Text
	Text_t1901882714 * ___Text_7;
	// Photon.Realtime.ServerConnection Photon.Pun.Demo.Cockpit.ServerProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(ServerProperty_t2037082094, ___Text_7)); }
	inline Text_t1901882714 * get_Text_7() const { return ___Text_7; }
	inline Text_t1901882714 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_t1901882714 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___Text_7), value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(ServerProperty_t2037082094, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERPROPERTY_T2037082094_H
#ifndef MONOBEHAVIOURPUNCALLBACKS_T1810614660_H
#define MONOBEHAVIOURPUNCALLBACKS_T1810614660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.MonoBehaviourPunCallbacks
struct  MonoBehaviourPunCallbacks_t1810614660  : public MonoBehaviourPun_t1682334777
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOURPUNCALLBACKS_T1810614660_H
#ifndef BACKSCRIPT_T341555447_H
#define BACKSCRIPT_T341555447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackScript
struct  BackScript_t341555447  : public MonoBehaviourPunCallbacks_t1810614660
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKSCRIPT_T341555447_H
#ifndef CHOICEENTER_T2353860254_H
#define CHOICEENTER_T2353860254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChoiceEnter
struct  ChoiceEnter_t2353860254  : public MonoBehaviourPunCallbacks_t1810614660
{
public:
	// System.Boolean ChoiceEnter::hasSetFirst
	bool ___hasSetFirst_8;
	// System.Boolean ChoiceEnter::hasSetSecond
	bool ___hasSetSecond_9;
	// System.Boolean ChoiceEnter::hasSetThird
	bool ___hasSetThird_10;
	// System.Boolean ChoiceEnter::hasSet
	bool ___hasSet_11;
	// UnityEngine.GameObject ChoiceEnter::numError
	GameObject_t1113636619 * ___numError_12;
	// UnityEngine.GameObject ChoiceEnter::enemyWaiting
	GameObject_t1113636619 * ___enemyWaiting_13;
	// System.Boolean ChoiceEnter::isChoiceEntered
	bool ___isChoiceEntered_14;
	// System.Boolean ChoiceEnter::isChildChoiceEntered
	bool ___isChildChoiceEntered_15;
	// System.Boolean ChoiceEnter::hasLoad
	bool ___hasLoad_16;
	// UnityEngine.GameObject ChoiceEnter::timerObject
	GameObject_t1113636619 * ___timerObject_17;
	// System.Single ChoiceEnter::timer
	float ___timer_18;
	// UnityEngine.GameObject ChoiceEnter::firstNum0
	GameObject_t1113636619 * ___firstNum0_19;
	// UnityEngine.GameObject ChoiceEnter::secondNum1
	GameObject_t1113636619 * ___secondNum1_20;
	// UnityEngine.GameObject ChoiceEnter::thirdNum2
	GameObject_t1113636619 * ___thirdNum2_21;

public:
	inline static int32_t get_offset_of_hasSetFirst_8() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254, ___hasSetFirst_8)); }
	inline bool get_hasSetFirst_8() const { return ___hasSetFirst_8; }
	inline bool* get_address_of_hasSetFirst_8() { return &___hasSetFirst_8; }
	inline void set_hasSetFirst_8(bool value)
	{
		___hasSetFirst_8 = value;
	}

	inline static int32_t get_offset_of_hasSetSecond_9() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254, ___hasSetSecond_9)); }
	inline bool get_hasSetSecond_9() const { return ___hasSetSecond_9; }
	inline bool* get_address_of_hasSetSecond_9() { return &___hasSetSecond_9; }
	inline void set_hasSetSecond_9(bool value)
	{
		___hasSetSecond_9 = value;
	}

	inline static int32_t get_offset_of_hasSetThird_10() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254, ___hasSetThird_10)); }
	inline bool get_hasSetThird_10() const { return ___hasSetThird_10; }
	inline bool* get_address_of_hasSetThird_10() { return &___hasSetThird_10; }
	inline void set_hasSetThird_10(bool value)
	{
		___hasSetThird_10 = value;
	}

	inline static int32_t get_offset_of_hasSet_11() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254, ___hasSet_11)); }
	inline bool get_hasSet_11() const { return ___hasSet_11; }
	inline bool* get_address_of_hasSet_11() { return &___hasSet_11; }
	inline void set_hasSet_11(bool value)
	{
		___hasSet_11 = value;
	}

	inline static int32_t get_offset_of_numError_12() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254, ___numError_12)); }
	inline GameObject_t1113636619 * get_numError_12() const { return ___numError_12; }
	inline GameObject_t1113636619 ** get_address_of_numError_12() { return &___numError_12; }
	inline void set_numError_12(GameObject_t1113636619 * value)
	{
		___numError_12 = value;
		Il2CppCodeGenWriteBarrier((&___numError_12), value);
	}

	inline static int32_t get_offset_of_enemyWaiting_13() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254, ___enemyWaiting_13)); }
	inline GameObject_t1113636619 * get_enemyWaiting_13() const { return ___enemyWaiting_13; }
	inline GameObject_t1113636619 ** get_address_of_enemyWaiting_13() { return &___enemyWaiting_13; }
	inline void set_enemyWaiting_13(GameObject_t1113636619 * value)
	{
		___enemyWaiting_13 = value;
		Il2CppCodeGenWriteBarrier((&___enemyWaiting_13), value);
	}

	inline static int32_t get_offset_of_isChoiceEntered_14() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254, ___isChoiceEntered_14)); }
	inline bool get_isChoiceEntered_14() const { return ___isChoiceEntered_14; }
	inline bool* get_address_of_isChoiceEntered_14() { return &___isChoiceEntered_14; }
	inline void set_isChoiceEntered_14(bool value)
	{
		___isChoiceEntered_14 = value;
	}

	inline static int32_t get_offset_of_isChildChoiceEntered_15() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254, ___isChildChoiceEntered_15)); }
	inline bool get_isChildChoiceEntered_15() const { return ___isChildChoiceEntered_15; }
	inline bool* get_address_of_isChildChoiceEntered_15() { return &___isChildChoiceEntered_15; }
	inline void set_isChildChoiceEntered_15(bool value)
	{
		___isChildChoiceEntered_15 = value;
	}

	inline static int32_t get_offset_of_hasLoad_16() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254, ___hasLoad_16)); }
	inline bool get_hasLoad_16() const { return ___hasLoad_16; }
	inline bool* get_address_of_hasLoad_16() { return &___hasLoad_16; }
	inline void set_hasLoad_16(bool value)
	{
		___hasLoad_16 = value;
	}

	inline static int32_t get_offset_of_timerObject_17() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254, ___timerObject_17)); }
	inline GameObject_t1113636619 * get_timerObject_17() const { return ___timerObject_17; }
	inline GameObject_t1113636619 ** get_address_of_timerObject_17() { return &___timerObject_17; }
	inline void set_timerObject_17(GameObject_t1113636619 * value)
	{
		___timerObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___timerObject_17), value);
	}

	inline static int32_t get_offset_of_timer_18() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254, ___timer_18)); }
	inline float get_timer_18() const { return ___timer_18; }
	inline float* get_address_of_timer_18() { return &___timer_18; }
	inline void set_timer_18(float value)
	{
		___timer_18 = value;
	}

	inline static int32_t get_offset_of_firstNum0_19() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254, ___firstNum0_19)); }
	inline GameObject_t1113636619 * get_firstNum0_19() const { return ___firstNum0_19; }
	inline GameObject_t1113636619 ** get_address_of_firstNum0_19() { return &___firstNum0_19; }
	inline void set_firstNum0_19(GameObject_t1113636619 * value)
	{
		___firstNum0_19 = value;
		Il2CppCodeGenWriteBarrier((&___firstNum0_19), value);
	}

	inline static int32_t get_offset_of_secondNum1_20() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254, ___secondNum1_20)); }
	inline GameObject_t1113636619 * get_secondNum1_20() const { return ___secondNum1_20; }
	inline GameObject_t1113636619 ** get_address_of_secondNum1_20() { return &___secondNum1_20; }
	inline void set_secondNum1_20(GameObject_t1113636619 * value)
	{
		___secondNum1_20 = value;
		Il2CppCodeGenWriteBarrier((&___secondNum1_20), value);
	}

	inline static int32_t get_offset_of_thirdNum2_21() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254, ___thirdNum2_21)); }
	inline GameObject_t1113636619 * get_thirdNum2_21() const { return ___thirdNum2_21; }
	inline GameObject_t1113636619 ** get_address_of_thirdNum2_21() { return &___thirdNum2_21; }
	inline void set_thirdNum2_21(GameObject_t1113636619 * value)
	{
		___thirdNum2_21 = value;
		Il2CppCodeGenWriteBarrier((&___thirdNum2_21), value);
	}
};

struct ChoiceEnter_t2353860254_StaticFields
{
public:
	// System.String ChoiceEnter::setFirstNum
	String_t* ___setFirstNum_5;
	// System.String ChoiceEnter::setSecondNum
	String_t* ___setSecondNum_6;
	// System.String ChoiceEnter::setThirdNum
	String_t* ___setThirdNum_7;

public:
	inline static int32_t get_offset_of_setFirstNum_5() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254_StaticFields, ___setFirstNum_5)); }
	inline String_t* get_setFirstNum_5() const { return ___setFirstNum_5; }
	inline String_t** get_address_of_setFirstNum_5() { return &___setFirstNum_5; }
	inline void set_setFirstNum_5(String_t* value)
	{
		___setFirstNum_5 = value;
		Il2CppCodeGenWriteBarrier((&___setFirstNum_5), value);
	}

	inline static int32_t get_offset_of_setSecondNum_6() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254_StaticFields, ___setSecondNum_6)); }
	inline String_t* get_setSecondNum_6() const { return ___setSecondNum_6; }
	inline String_t** get_address_of_setSecondNum_6() { return &___setSecondNum_6; }
	inline void set_setSecondNum_6(String_t* value)
	{
		___setSecondNum_6 = value;
		Il2CppCodeGenWriteBarrier((&___setSecondNum_6), value);
	}

	inline static int32_t get_offset_of_setThirdNum_7() { return static_cast<int32_t>(offsetof(ChoiceEnter_t2353860254_StaticFields, ___setThirdNum_7)); }
	inline String_t* get_setThirdNum_7() const { return ___setThirdNum_7; }
	inline String_t** get_address_of_setThirdNum_7() { return &___setThirdNum_7; }
	inline void set_setThirdNum_7(String_t* value)
	{
		___setThirdNum_7 = value;
		Il2CppCodeGenWriteBarrier((&___setThirdNum_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHOICEENTER_T2353860254_H
#ifndef ENEMYLOGSCRIPT_T943560016_H
#define ENEMYLOGSCRIPT_T943560016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyLogScript
struct  EnemyLogScript_t943560016  : public MonoBehaviourPunCallbacks_t1810614660
{
public:
	// System.Int32 EnemyLogScript::hitCount
	int32_t ___hitCount_5;
	// System.Int32 EnemyLogScript::blowCount
	int32_t ___blowCount_6;
	// UnityEngine.GameObject EnemyLogScript::enemyLogTextView
	GameObject_t1113636619 * ___enemyLogTextView_7;
	// UnityEngine.GameObject EnemyLogScript::content
	GameObject_t1113636619 * ___content_8;
	// UnityEngine.GameObject EnemyLogScript::enter
	GameObject_t1113636619 * ___enter_9;
	// Enter EnemyLogScript::enterScript
	Enter_t1090004723 * ___enterScript_10;
	// EnemyLogTextView EnemyLogScript::script
	EnemyLogTextView_t71717095 * ___script_11;

public:
	inline static int32_t get_offset_of_hitCount_5() { return static_cast<int32_t>(offsetof(EnemyLogScript_t943560016, ___hitCount_5)); }
	inline int32_t get_hitCount_5() const { return ___hitCount_5; }
	inline int32_t* get_address_of_hitCount_5() { return &___hitCount_5; }
	inline void set_hitCount_5(int32_t value)
	{
		___hitCount_5 = value;
	}

	inline static int32_t get_offset_of_blowCount_6() { return static_cast<int32_t>(offsetof(EnemyLogScript_t943560016, ___blowCount_6)); }
	inline int32_t get_blowCount_6() const { return ___blowCount_6; }
	inline int32_t* get_address_of_blowCount_6() { return &___blowCount_6; }
	inline void set_blowCount_6(int32_t value)
	{
		___blowCount_6 = value;
	}

	inline static int32_t get_offset_of_enemyLogTextView_7() { return static_cast<int32_t>(offsetof(EnemyLogScript_t943560016, ___enemyLogTextView_7)); }
	inline GameObject_t1113636619 * get_enemyLogTextView_7() const { return ___enemyLogTextView_7; }
	inline GameObject_t1113636619 ** get_address_of_enemyLogTextView_7() { return &___enemyLogTextView_7; }
	inline void set_enemyLogTextView_7(GameObject_t1113636619 * value)
	{
		___enemyLogTextView_7 = value;
		Il2CppCodeGenWriteBarrier((&___enemyLogTextView_7), value);
	}

	inline static int32_t get_offset_of_content_8() { return static_cast<int32_t>(offsetof(EnemyLogScript_t943560016, ___content_8)); }
	inline GameObject_t1113636619 * get_content_8() const { return ___content_8; }
	inline GameObject_t1113636619 ** get_address_of_content_8() { return &___content_8; }
	inline void set_content_8(GameObject_t1113636619 * value)
	{
		___content_8 = value;
		Il2CppCodeGenWriteBarrier((&___content_8), value);
	}

	inline static int32_t get_offset_of_enter_9() { return static_cast<int32_t>(offsetof(EnemyLogScript_t943560016, ___enter_9)); }
	inline GameObject_t1113636619 * get_enter_9() const { return ___enter_9; }
	inline GameObject_t1113636619 ** get_address_of_enter_9() { return &___enter_9; }
	inline void set_enter_9(GameObject_t1113636619 * value)
	{
		___enter_9 = value;
		Il2CppCodeGenWriteBarrier((&___enter_9), value);
	}

	inline static int32_t get_offset_of_enterScript_10() { return static_cast<int32_t>(offsetof(EnemyLogScript_t943560016, ___enterScript_10)); }
	inline Enter_t1090004723 * get_enterScript_10() const { return ___enterScript_10; }
	inline Enter_t1090004723 ** get_address_of_enterScript_10() { return &___enterScript_10; }
	inline void set_enterScript_10(Enter_t1090004723 * value)
	{
		___enterScript_10 = value;
		Il2CppCodeGenWriteBarrier((&___enterScript_10), value);
	}

	inline static int32_t get_offset_of_script_11() { return static_cast<int32_t>(offsetof(EnemyLogScript_t943560016, ___script_11)); }
	inline EnemyLogTextView_t71717095 * get_script_11() const { return ___script_11; }
	inline EnemyLogTextView_t71717095 ** get_address_of_script_11() { return &___script_11; }
	inline void set_script_11(EnemyLogTextView_t71717095 * value)
	{
		___script_11 = value;
		Il2CppCodeGenWriteBarrier((&___script_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYLOGSCRIPT_T943560016_H
#ifndef ENEMYLOGTEXTVIEW_T71717095_H
#define ENEMYLOGTEXTVIEW_T71717095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyLogTextView
struct  EnemyLogTextView_t71717095  : public MonoBehaviourPunCallbacks_t1810614660
{
public:
	// UnityEngine.UI.Text EnemyLogTextView::enemyLogText
	Text_t1901882714 * ___enemyLogText_5;
	// System.String EnemyLogTextView::setEnemyNum
	String_t* ___setEnemyNum_6;

public:
	inline static int32_t get_offset_of_enemyLogText_5() { return static_cast<int32_t>(offsetof(EnemyLogTextView_t71717095, ___enemyLogText_5)); }
	inline Text_t1901882714 * get_enemyLogText_5() const { return ___enemyLogText_5; }
	inline Text_t1901882714 ** get_address_of_enemyLogText_5() { return &___enemyLogText_5; }
	inline void set_enemyLogText_5(Text_t1901882714 * value)
	{
		___enemyLogText_5 = value;
		Il2CppCodeGenWriteBarrier((&___enemyLogText_5), value);
	}

	inline static int32_t get_offset_of_setEnemyNum_6() { return static_cast<int32_t>(offsetof(EnemyLogTextView_t71717095, ___setEnemyNum_6)); }
	inline String_t* get_setEnemyNum_6() const { return ___setEnemyNum_6; }
	inline String_t** get_address_of_setEnemyNum_6() { return &___setEnemyNum_6; }
	inline void set_setEnemyNum_6(String_t* value)
	{
		___setEnemyNum_6 = value;
		Il2CppCodeGenWriteBarrier((&___setEnemyNum_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYLOGTEXTVIEW_T71717095_H
#ifndef ENTER_T1090004723_H
#define ENTER_T1090004723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enter
struct  Enter_t1090004723  : public MonoBehaviourPunCallbacks_t1810614660
{
public:
	// System.String Enter::yourFirstNum
	String_t* ___yourFirstNum_5;
	// System.String Enter::yourSecondNum
	String_t* ___yourSecondNum_6;
	// System.String Enter::yourThirdNum
	String_t* ___yourThirdNum_7;
	// UnityEngine.GameObject Enter::yourLog
	GameObject_t1113636619 * ___yourLog_17;
	// YourLogScript Enter::yourLogScript
	YourLogScript_t3397472296 * ___yourLogScript_18;
	// UnityEngine.GameObject Enter::enemyLog
	GameObject_t1113636619 * ___enemyLog_19;
	// EnemyLogScript Enter::enemyLogScript
	EnemyLogScript_t943560016 * ___enemyLogScript_20;
	// UnityEngine.GameObject Enter::turn
	GameObject_t1113636619 * ___turn_21;
	// TurnScript Enter::turnScript
	TurnScript_t2279615295 * ___turnScript_22;
	// UnityEngine.GameObject Enter::timerObject
	GameObject_t1113636619 * ___timerObject_23;
	// TimerScript Enter::timerScript
	TimerScript_t1618777791 * ___timerScript_24;
	// System.Single Enter::timer
	float ___timer_25;
	// UnityEngine.GameObject Enter::resultManager
	GameObject_t1113636619 * ___resultManager_26;
	// ResultManager Enter::resultManagerScript
	ResultManager_t2208194647 * ___resultManagerScript_27;
	// UnityEngine.GameObject Enter::numError
	GameObject_t1113636619 * ___numError_28;
	// UnityEngine.GameObject Enter::enemyWaiting
	GameObject_t1113636619 * ___enemyWaiting_29;
	// System.Boolean Enter::hasSetFirst
	bool ___hasSetFirst_30;
	// System.Boolean Enter::hasSetSecond
	bool ___hasSetSecond_31;
	// System.Boolean Enter::hasSetThird
	bool ___hasSetThird_32;
	// System.Boolean Enter::hasSet
	bool ___hasSet_33;
	// System.Boolean Enter::isEntered
	bool ___isEntered_34;
	// System.Boolean Enter::isEnemyEntered
	bool ___isEnemyEntered_35;
	// System.Boolean Enter::isFinishedReadingSetEnemyNum
	bool ___isFinishedReadingSetEnemyNum_36;
	// System.Boolean Enter::isYouWin
	bool ___isYouWin_37;
	// System.Boolean Enter::isYouLose
	bool ___isYouLose_38;
	// System.Boolean Enter::hasLoad
	bool ___hasLoad_39;

public:
	inline static int32_t get_offset_of_yourFirstNum_5() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___yourFirstNum_5)); }
	inline String_t* get_yourFirstNum_5() const { return ___yourFirstNum_5; }
	inline String_t** get_address_of_yourFirstNum_5() { return &___yourFirstNum_5; }
	inline void set_yourFirstNum_5(String_t* value)
	{
		___yourFirstNum_5 = value;
		Il2CppCodeGenWriteBarrier((&___yourFirstNum_5), value);
	}

	inline static int32_t get_offset_of_yourSecondNum_6() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___yourSecondNum_6)); }
	inline String_t* get_yourSecondNum_6() const { return ___yourSecondNum_6; }
	inline String_t** get_address_of_yourSecondNum_6() { return &___yourSecondNum_6; }
	inline void set_yourSecondNum_6(String_t* value)
	{
		___yourSecondNum_6 = value;
		Il2CppCodeGenWriteBarrier((&___yourSecondNum_6), value);
	}

	inline static int32_t get_offset_of_yourThirdNum_7() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___yourThirdNum_7)); }
	inline String_t* get_yourThirdNum_7() const { return ___yourThirdNum_7; }
	inline String_t** get_address_of_yourThirdNum_7() { return &___yourThirdNum_7; }
	inline void set_yourThirdNum_7(String_t* value)
	{
		___yourThirdNum_7 = value;
		Il2CppCodeGenWriteBarrier((&___yourThirdNum_7), value);
	}

	inline static int32_t get_offset_of_yourLog_17() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___yourLog_17)); }
	inline GameObject_t1113636619 * get_yourLog_17() const { return ___yourLog_17; }
	inline GameObject_t1113636619 ** get_address_of_yourLog_17() { return &___yourLog_17; }
	inline void set_yourLog_17(GameObject_t1113636619 * value)
	{
		___yourLog_17 = value;
		Il2CppCodeGenWriteBarrier((&___yourLog_17), value);
	}

	inline static int32_t get_offset_of_yourLogScript_18() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___yourLogScript_18)); }
	inline YourLogScript_t3397472296 * get_yourLogScript_18() const { return ___yourLogScript_18; }
	inline YourLogScript_t3397472296 ** get_address_of_yourLogScript_18() { return &___yourLogScript_18; }
	inline void set_yourLogScript_18(YourLogScript_t3397472296 * value)
	{
		___yourLogScript_18 = value;
		Il2CppCodeGenWriteBarrier((&___yourLogScript_18), value);
	}

	inline static int32_t get_offset_of_enemyLog_19() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___enemyLog_19)); }
	inline GameObject_t1113636619 * get_enemyLog_19() const { return ___enemyLog_19; }
	inline GameObject_t1113636619 ** get_address_of_enemyLog_19() { return &___enemyLog_19; }
	inline void set_enemyLog_19(GameObject_t1113636619 * value)
	{
		___enemyLog_19 = value;
		Il2CppCodeGenWriteBarrier((&___enemyLog_19), value);
	}

	inline static int32_t get_offset_of_enemyLogScript_20() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___enemyLogScript_20)); }
	inline EnemyLogScript_t943560016 * get_enemyLogScript_20() const { return ___enemyLogScript_20; }
	inline EnemyLogScript_t943560016 ** get_address_of_enemyLogScript_20() { return &___enemyLogScript_20; }
	inline void set_enemyLogScript_20(EnemyLogScript_t943560016 * value)
	{
		___enemyLogScript_20 = value;
		Il2CppCodeGenWriteBarrier((&___enemyLogScript_20), value);
	}

	inline static int32_t get_offset_of_turn_21() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___turn_21)); }
	inline GameObject_t1113636619 * get_turn_21() const { return ___turn_21; }
	inline GameObject_t1113636619 ** get_address_of_turn_21() { return &___turn_21; }
	inline void set_turn_21(GameObject_t1113636619 * value)
	{
		___turn_21 = value;
		Il2CppCodeGenWriteBarrier((&___turn_21), value);
	}

	inline static int32_t get_offset_of_turnScript_22() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___turnScript_22)); }
	inline TurnScript_t2279615295 * get_turnScript_22() const { return ___turnScript_22; }
	inline TurnScript_t2279615295 ** get_address_of_turnScript_22() { return &___turnScript_22; }
	inline void set_turnScript_22(TurnScript_t2279615295 * value)
	{
		___turnScript_22 = value;
		Il2CppCodeGenWriteBarrier((&___turnScript_22), value);
	}

	inline static int32_t get_offset_of_timerObject_23() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___timerObject_23)); }
	inline GameObject_t1113636619 * get_timerObject_23() const { return ___timerObject_23; }
	inline GameObject_t1113636619 ** get_address_of_timerObject_23() { return &___timerObject_23; }
	inline void set_timerObject_23(GameObject_t1113636619 * value)
	{
		___timerObject_23 = value;
		Il2CppCodeGenWriteBarrier((&___timerObject_23), value);
	}

	inline static int32_t get_offset_of_timerScript_24() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___timerScript_24)); }
	inline TimerScript_t1618777791 * get_timerScript_24() const { return ___timerScript_24; }
	inline TimerScript_t1618777791 ** get_address_of_timerScript_24() { return &___timerScript_24; }
	inline void set_timerScript_24(TimerScript_t1618777791 * value)
	{
		___timerScript_24 = value;
		Il2CppCodeGenWriteBarrier((&___timerScript_24), value);
	}

	inline static int32_t get_offset_of_timer_25() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___timer_25)); }
	inline float get_timer_25() const { return ___timer_25; }
	inline float* get_address_of_timer_25() { return &___timer_25; }
	inline void set_timer_25(float value)
	{
		___timer_25 = value;
	}

	inline static int32_t get_offset_of_resultManager_26() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___resultManager_26)); }
	inline GameObject_t1113636619 * get_resultManager_26() const { return ___resultManager_26; }
	inline GameObject_t1113636619 ** get_address_of_resultManager_26() { return &___resultManager_26; }
	inline void set_resultManager_26(GameObject_t1113636619 * value)
	{
		___resultManager_26 = value;
		Il2CppCodeGenWriteBarrier((&___resultManager_26), value);
	}

	inline static int32_t get_offset_of_resultManagerScript_27() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___resultManagerScript_27)); }
	inline ResultManager_t2208194647 * get_resultManagerScript_27() const { return ___resultManagerScript_27; }
	inline ResultManager_t2208194647 ** get_address_of_resultManagerScript_27() { return &___resultManagerScript_27; }
	inline void set_resultManagerScript_27(ResultManager_t2208194647 * value)
	{
		___resultManagerScript_27 = value;
		Il2CppCodeGenWriteBarrier((&___resultManagerScript_27), value);
	}

	inline static int32_t get_offset_of_numError_28() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___numError_28)); }
	inline GameObject_t1113636619 * get_numError_28() const { return ___numError_28; }
	inline GameObject_t1113636619 ** get_address_of_numError_28() { return &___numError_28; }
	inline void set_numError_28(GameObject_t1113636619 * value)
	{
		___numError_28 = value;
		Il2CppCodeGenWriteBarrier((&___numError_28), value);
	}

	inline static int32_t get_offset_of_enemyWaiting_29() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___enemyWaiting_29)); }
	inline GameObject_t1113636619 * get_enemyWaiting_29() const { return ___enemyWaiting_29; }
	inline GameObject_t1113636619 ** get_address_of_enemyWaiting_29() { return &___enemyWaiting_29; }
	inline void set_enemyWaiting_29(GameObject_t1113636619 * value)
	{
		___enemyWaiting_29 = value;
		Il2CppCodeGenWriteBarrier((&___enemyWaiting_29), value);
	}

	inline static int32_t get_offset_of_hasSetFirst_30() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___hasSetFirst_30)); }
	inline bool get_hasSetFirst_30() const { return ___hasSetFirst_30; }
	inline bool* get_address_of_hasSetFirst_30() { return &___hasSetFirst_30; }
	inline void set_hasSetFirst_30(bool value)
	{
		___hasSetFirst_30 = value;
	}

	inline static int32_t get_offset_of_hasSetSecond_31() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___hasSetSecond_31)); }
	inline bool get_hasSetSecond_31() const { return ___hasSetSecond_31; }
	inline bool* get_address_of_hasSetSecond_31() { return &___hasSetSecond_31; }
	inline void set_hasSetSecond_31(bool value)
	{
		___hasSetSecond_31 = value;
	}

	inline static int32_t get_offset_of_hasSetThird_32() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___hasSetThird_32)); }
	inline bool get_hasSetThird_32() const { return ___hasSetThird_32; }
	inline bool* get_address_of_hasSetThird_32() { return &___hasSetThird_32; }
	inline void set_hasSetThird_32(bool value)
	{
		___hasSetThird_32 = value;
	}

	inline static int32_t get_offset_of_hasSet_33() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___hasSet_33)); }
	inline bool get_hasSet_33() const { return ___hasSet_33; }
	inline bool* get_address_of_hasSet_33() { return &___hasSet_33; }
	inline void set_hasSet_33(bool value)
	{
		___hasSet_33 = value;
	}

	inline static int32_t get_offset_of_isEntered_34() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___isEntered_34)); }
	inline bool get_isEntered_34() const { return ___isEntered_34; }
	inline bool* get_address_of_isEntered_34() { return &___isEntered_34; }
	inline void set_isEntered_34(bool value)
	{
		___isEntered_34 = value;
	}

	inline static int32_t get_offset_of_isEnemyEntered_35() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___isEnemyEntered_35)); }
	inline bool get_isEnemyEntered_35() const { return ___isEnemyEntered_35; }
	inline bool* get_address_of_isEnemyEntered_35() { return &___isEnemyEntered_35; }
	inline void set_isEnemyEntered_35(bool value)
	{
		___isEnemyEntered_35 = value;
	}

	inline static int32_t get_offset_of_isFinishedReadingSetEnemyNum_36() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___isFinishedReadingSetEnemyNum_36)); }
	inline bool get_isFinishedReadingSetEnemyNum_36() const { return ___isFinishedReadingSetEnemyNum_36; }
	inline bool* get_address_of_isFinishedReadingSetEnemyNum_36() { return &___isFinishedReadingSetEnemyNum_36; }
	inline void set_isFinishedReadingSetEnemyNum_36(bool value)
	{
		___isFinishedReadingSetEnemyNum_36 = value;
	}

	inline static int32_t get_offset_of_isYouWin_37() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___isYouWin_37)); }
	inline bool get_isYouWin_37() const { return ___isYouWin_37; }
	inline bool* get_address_of_isYouWin_37() { return &___isYouWin_37; }
	inline void set_isYouWin_37(bool value)
	{
		___isYouWin_37 = value;
	}

	inline static int32_t get_offset_of_isYouLose_38() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___isYouLose_38)); }
	inline bool get_isYouLose_38() const { return ___isYouLose_38; }
	inline bool* get_address_of_isYouLose_38() { return &___isYouLose_38; }
	inline void set_isYouLose_38(bool value)
	{
		___isYouLose_38 = value;
	}

	inline static int32_t get_offset_of_hasLoad_39() { return static_cast<int32_t>(offsetof(Enter_t1090004723, ___hasLoad_39)); }
	inline bool get_hasLoad_39() const { return ___hasLoad_39; }
	inline bool* get_address_of_hasLoad_39() { return &___hasLoad_39; }
	inline void set_hasLoad_39(bool value)
	{
		___hasLoad_39 = value;
	}
};

struct Enter_t1090004723_StaticFields
{
public:
	// System.String Enter::enemyFirstNum
	String_t* ___enemyFirstNum_8;
	// System.String Enter::enemySecondNum
	String_t* ___enemySecondNum_9;
	// System.String Enter::enemyThirdNum
	String_t* ___enemyThirdNum_10;
	// System.String Enter::setFirstNum
	String_t* ___setFirstNum_11;
	// System.String Enter::setSecondNum
	String_t* ___setSecondNum_12;
	// System.String Enter::setThirdNum
	String_t* ___setThirdNum_13;
	// System.String Enter::setEnemyFirstNum
	String_t* ___setEnemyFirstNum_14;
	// System.String Enter::setEnemySecondNum
	String_t* ___setEnemySecondNum_15;
	// System.String Enter::setEnemyThirdNum
	String_t* ___setEnemyThirdNum_16;

public:
	inline static int32_t get_offset_of_enemyFirstNum_8() { return static_cast<int32_t>(offsetof(Enter_t1090004723_StaticFields, ___enemyFirstNum_8)); }
	inline String_t* get_enemyFirstNum_8() const { return ___enemyFirstNum_8; }
	inline String_t** get_address_of_enemyFirstNum_8() { return &___enemyFirstNum_8; }
	inline void set_enemyFirstNum_8(String_t* value)
	{
		___enemyFirstNum_8 = value;
		Il2CppCodeGenWriteBarrier((&___enemyFirstNum_8), value);
	}

	inline static int32_t get_offset_of_enemySecondNum_9() { return static_cast<int32_t>(offsetof(Enter_t1090004723_StaticFields, ___enemySecondNum_9)); }
	inline String_t* get_enemySecondNum_9() const { return ___enemySecondNum_9; }
	inline String_t** get_address_of_enemySecondNum_9() { return &___enemySecondNum_9; }
	inline void set_enemySecondNum_9(String_t* value)
	{
		___enemySecondNum_9 = value;
		Il2CppCodeGenWriteBarrier((&___enemySecondNum_9), value);
	}

	inline static int32_t get_offset_of_enemyThirdNum_10() { return static_cast<int32_t>(offsetof(Enter_t1090004723_StaticFields, ___enemyThirdNum_10)); }
	inline String_t* get_enemyThirdNum_10() const { return ___enemyThirdNum_10; }
	inline String_t** get_address_of_enemyThirdNum_10() { return &___enemyThirdNum_10; }
	inline void set_enemyThirdNum_10(String_t* value)
	{
		___enemyThirdNum_10 = value;
		Il2CppCodeGenWriteBarrier((&___enemyThirdNum_10), value);
	}

	inline static int32_t get_offset_of_setFirstNum_11() { return static_cast<int32_t>(offsetof(Enter_t1090004723_StaticFields, ___setFirstNum_11)); }
	inline String_t* get_setFirstNum_11() const { return ___setFirstNum_11; }
	inline String_t** get_address_of_setFirstNum_11() { return &___setFirstNum_11; }
	inline void set_setFirstNum_11(String_t* value)
	{
		___setFirstNum_11 = value;
		Il2CppCodeGenWriteBarrier((&___setFirstNum_11), value);
	}

	inline static int32_t get_offset_of_setSecondNum_12() { return static_cast<int32_t>(offsetof(Enter_t1090004723_StaticFields, ___setSecondNum_12)); }
	inline String_t* get_setSecondNum_12() const { return ___setSecondNum_12; }
	inline String_t** get_address_of_setSecondNum_12() { return &___setSecondNum_12; }
	inline void set_setSecondNum_12(String_t* value)
	{
		___setSecondNum_12 = value;
		Il2CppCodeGenWriteBarrier((&___setSecondNum_12), value);
	}

	inline static int32_t get_offset_of_setThirdNum_13() { return static_cast<int32_t>(offsetof(Enter_t1090004723_StaticFields, ___setThirdNum_13)); }
	inline String_t* get_setThirdNum_13() const { return ___setThirdNum_13; }
	inline String_t** get_address_of_setThirdNum_13() { return &___setThirdNum_13; }
	inline void set_setThirdNum_13(String_t* value)
	{
		___setThirdNum_13 = value;
		Il2CppCodeGenWriteBarrier((&___setThirdNum_13), value);
	}

	inline static int32_t get_offset_of_setEnemyFirstNum_14() { return static_cast<int32_t>(offsetof(Enter_t1090004723_StaticFields, ___setEnemyFirstNum_14)); }
	inline String_t* get_setEnemyFirstNum_14() const { return ___setEnemyFirstNum_14; }
	inline String_t** get_address_of_setEnemyFirstNum_14() { return &___setEnemyFirstNum_14; }
	inline void set_setEnemyFirstNum_14(String_t* value)
	{
		___setEnemyFirstNum_14 = value;
		Il2CppCodeGenWriteBarrier((&___setEnemyFirstNum_14), value);
	}

	inline static int32_t get_offset_of_setEnemySecondNum_15() { return static_cast<int32_t>(offsetof(Enter_t1090004723_StaticFields, ___setEnemySecondNum_15)); }
	inline String_t* get_setEnemySecondNum_15() const { return ___setEnemySecondNum_15; }
	inline String_t** get_address_of_setEnemySecondNum_15() { return &___setEnemySecondNum_15; }
	inline void set_setEnemySecondNum_15(String_t* value)
	{
		___setEnemySecondNum_15 = value;
		Il2CppCodeGenWriteBarrier((&___setEnemySecondNum_15), value);
	}

	inline static int32_t get_offset_of_setEnemyThirdNum_16() { return static_cast<int32_t>(offsetof(Enter_t1090004723_StaticFields, ___setEnemyThirdNum_16)); }
	inline String_t* get_setEnemyThirdNum_16() const { return ___setEnemyThirdNum_16; }
	inline String_t** get_address_of_setEnemyThirdNum_16() { return &___setEnemyThirdNum_16; }
	inline void set_setEnemyThirdNum_16(String_t* value)
	{
		___setEnemyThirdNum_16 = value;
		Il2CppCodeGenWriteBarrier((&___setEnemyThirdNum_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTER_T1090004723_H
#ifndef ITEMSCRIPT_T3045424977_H
#define ITEMSCRIPT_T3045424977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemScript
struct  ItemScript_t3045424977  : public MonoBehaviourPunCallbacks_t1810614660
{
public:
	// System.String ItemScript::enemyFirstNum
	String_t* ___enemyFirstNum_5;
	// System.String ItemScript::enemySecondNum
	String_t* ___enemySecondNum_6;
	// System.String ItemScript::enemyThirdNum
	String_t* ___enemyThirdNum_7;
	// UnityEngine.GameObject ItemScript::yourLTV
	GameObject_t1113636619 * ___yourLTV_8;
	// YourLogTextView ItemScript::yourLTVScript
	YourLogTextView_t1250600108 * ___yourLTVScript_9;
	// UnityEngine.GameObject ItemScript::yourContent
	GameObject_t1113636619 * ___yourContent_10;
	// UnityEngine.GameObject ItemScript::enemyLTV
	GameObject_t1113636619 * ___enemyLTV_11;
	// EnemyLogTextView ItemScript::enemyLTVScript
	EnemyLogTextView_t71717095 * ___enemyLTVScript_12;
	// UnityEngine.GameObject ItemScript::enemyContent
	GameObject_t1113636619 * ___enemyContent_13;
	// System.String ItemScript::firstHL
	String_t* ___firstHL_14;
	// System.String ItemScript::secondHL
	String_t* ___secondHL_15;
	// System.String ItemScript::thirdHL
	String_t* ___thirdHL_16;
	// System.Boolean ItemScript::isUsedHL
	bool ___isUsedHL_17;

public:
	inline static int32_t get_offset_of_enemyFirstNum_5() { return static_cast<int32_t>(offsetof(ItemScript_t3045424977, ___enemyFirstNum_5)); }
	inline String_t* get_enemyFirstNum_5() const { return ___enemyFirstNum_5; }
	inline String_t** get_address_of_enemyFirstNum_5() { return &___enemyFirstNum_5; }
	inline void set_enemyFirstNum_5(String_t* value)
	{
		___enemyFirstNum_5 = value;
		Il2CppCodeGenWriteBarrier((&___enemyFirstNum_5), value);
	}

	inline static int32_t get_offset_of_enemySecondNum_6() { return static_cast<int32_t>(offsetof(ItemScript_t3045424977, ___enemySecondNum_6)); }
	inline String_t* get_enemySecondNum_6() const { return ___enemySecondNum_6; }
	inline String_t** get_address_of_enemySecondNum_6() { return &___enemySecondNum_6; }
	inline void set_enemySecondNum_6(String_t* value)
	{
		___enemySecondNum_6 = value;
		Il2CppCodeGenWriteBarrier((&___enemySecondNum_6), value);
	}

	inline static int32_t get_offset_of_enemyThirdNum_7() { return static_cast<int32_t>(offsetof(ItemScript_t3045424977, ___enemyThirdNum_7)); }
	inline String_t* get_enemyThirdNum_7() const { return ___enemyThirdNum_7; }
	inline String_t** get_address_of_enemyThirdNum_7() { return &___enemyThirdNum_7; }
	inline void set_enemyThirdNum_7(String_t* value)
	{
		___enemyThirdNum_7 = value;
		Il2CppCodeGenWriteBarrier((&___enemyThirdNum_7), value);
	}

	inline static int32_t get_offset_of_yourLTV_8() { return static_cast<int32_t>(offsetof(ItemScript_t3045424977, ___yourLTV_8)); }
	inline GameObject_t1113636619 * get_yourLTV_8() const { return ___yourLTV_8; }
	inline GameObject_t1113636619 ** get_address_of_yourLTV_8() { return &___yourLTV_8; }
	inline void set_yourLTV_8(GameObject_t1113636619 * value)
	{
		___yourLTV_8 = value;
		Il2CppCodeGenWriteBarrier((&___yourLTV_8), value);
	}

	inline static int32_t get_offset_of_yourLTVScript_9() { return static_cast<int32_t>(offsetof(ItemScript_t3045424977, ___yourLTVScript_9)); }
	inline YourLogTextView_t1250600108 * get_yourLTVScript_9() const { return ___yourLTVScript_9; }
	inline YourLogTextView_t1250600108 ** get_address_of_yourLTVScript_9() { return &___yourLTVScript_9; }
	inline void set_yourLTVScript_9(YourLogTextView_t1250600108 * value)
	{
		___yourLTVScript_9 = value;
		Il2CppCodeGenWriteBarrier((&___yourLTVScript_9), value);
	}

	inline static int32_t get_offset_of_yourContent_10() { return static_cast<int32_t>(offsetof(ItemScript_t3045424977, ___yourContent_10)); }
	inline GameObject_t1113636619 * get_yourContent_10() const { return ___yourContent_10; }
	inline GameObject_t1113636619 ** get_address_of_yourContent_10() { return &___yourContent_10; }
	inline void set_yourContent_10(GameObject_t1113636619 * value)
	{
		___yourContent_10 = value;
		Il2CppCodeGenWriteBarrier((&___yourContent_10), value);
	}

	inline static int32_t get_offset_of_enemyLTV_11() { return static_cast<int32_t>(offsetof(ItemScript_t3045424977, ___enemyLTV_11)); }
	inline GameObject_t1113636619 * get_enemyLTV_11() const { return ___enemyLTV_11; }
	inline GameObject_t1113636619 ** get_address_of_enemyLTV_11() { return &___enemyLTV_11; }
	inline void set_enemyLTV_11(GameObject_t1113636619 * value)
	{
		___enemyLTV_11 = value;
		Il2CppCodeGenWriteBarrier((&___enemyLTV_11), value);
	}

	inline static int32_t get_offset_of_enemyLTVScript_12() { return static_cast<int32_t>(offsetof(ItemScript_t3045424977, ___enemyLTVScript_12)); }
	inline EnemyLogTextView_t71717095 * get_enemyLTVScript_12() const { return ___enemyLTVScript_12; }
	inline EnemyLogTextView_t71717095 ** get_address_of_enemyLTVScript_12() { return &___enemyLTVScript_12; }
	inline void set_enemyLTVScript_12(EnemyLogTextView_t71717095 * value)
	{
		___enemyLTVScript_12 = value;
		Il2CppCodeGenWriteBarrier((&___enemyLTVScript_12), value);
	}

	inline static int32_t get_offset_of_enemyContent_13() { return static_cast<int32_t>(offsetof(ItemScript_t3045424977, ___enemyContent_13)); }
	inline GameObject_t1113636619 * get_enemyContent_13() const { return ___enemyContent_13; }
	inline GameObject_t1113636619 ** get_address_of_enemyContent_13() { return &___enemyContent_13; }
	inline void set_enemyContent_13(GameObject_t1113636619 * value)
	{
		___enemyContent_13 = value;
		Il2CppCodeGenWriteBarrier((&___enemyContent_13), value);
	}

	inline static int32_t get_offset_of_firstHL_14() { return static_cast<int32_t>(offsetof(ItemScript_t3045424977, ___firstHL_14)); }
	inline String_t* get_firstHL_14() const { return ___firstHL_14; }
	inline String_t** get_address_of_firstHL_14() { return &___firstHL_14; }
	inline void set_firstHL_14(String_t* value)
	{
		___firstHL_14 = value;
		Il2CppCodeGenWriteBarrier((&___firstHL_14), value);
	}

	inline static int32_t get_offset_of_secondHL_15() { return static_cast<int32_t>(offsetof(ItemScript_t3045424977, ___secondHL_15)); }
	inline String_t* get_secondHL_15() const { return ___secondHL_15; }
	inline String_t** get_address_of_secondHL_15() { return &___secondHL_15; }
	inline void set_secondHL_15(String_t* value)
	{
		___secondHL_15 = value;
		Il2CppCodeGenWriteBarrier((&___secondHL_15), value);
	}

	inline static int32_t get_offset_of_thirdHL_16() { return static_cast<int32_t>(offsetof(ItemScript_t3045424977, ___thirdHL_16)); }
	inline String_t* get_thirdHL_16() const { return ___thirdHL_16; }
	inline String_t** get_address_of_thirdHL_16() { return &___thirdHL_16; }
	inline void set_thirdHL_16(String_t* value)
	{
		___thirdHL_16 = value;
		Il2CppCodeGenWriteBarrier((&___thirdHL_16), value);
	}

	inline static int32_t get_offset_of_isUsedHL_17() { return static_cast<int32_t>(offsetof(ItemScript_t3045424977, ___isUsedHL_17)); }
	inline bool get_isUsedHL_17() const { return ___isUsedHL_17; }
	inline bool* get_address_of_isUsedHL_17() { return &___isUsedHL_17; }
	inline void set_isUsedHL_17(bool value)
	{
		___isUsedHL_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMSCRIPT_T3045424977_H
#ifndef LAUNCHER_T75719789_H
#define LAUNCHER_T75719789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Launcher
struct  Launcher_t75719789  : public MonoBehaviourPunCallbacks_t1810614660
{
public:
	// System.String Launcher::gameVersion
	String_t* ___gameVersion_5;

public:
	inline static int32_t get_offset_of_gameVersion_5() { return static_cast<int32_t>(offsetof(Launcher_t75719789, ___gameVersion_5)); }
	inline String_t* get_gameVersion_5() const { return ___gameVersion_5; }
	inline String_t** get_address_of_gameVersion_5() { return &___gameVersion_5; }
	inline void set_gameVersion_5(String_t* value)
	{
		___gameVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___gameVersion_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAUNCHER_T75719789_H
#ifndef PUNCOCKPITEMBED_T1735463960_H
#define PUNCOCKPITEMBED_T1735463960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Photon.Pun.Demo.Cockpit.PunCockpitEmbed
struct  PunCockpitEmbed_t1735463960  : public MonoBehaviourPunCallbacks_t1810614660
{
public:
	// System.String Photon.Pun.Demo.Cockpit.PunCockpitEmbed::PunCockpit_scene
	String_t* ___PunCockpit_scene_5;
	// System.Boolean Photon.Pun.Demo.Cockpit.PunCockpitEmbed::EmbeddCockpit
	bool ___EmbeddCockpit_6;
	// System.String Photon.Pun.Demo.Cockpit.PunCockpitEmbed::CockpitGameTitle
	String_t* ___CockpitGameTitle_7;
	// UnityEngine.GameObject Photon.Pun.Demo.Cockpit.PunCockpitEmbed::LoadingIndicator
	GameObject_t1113636619 * ___LoadingIndicator_8;
	// Photon.Pun.UtilityScripts.ConnectAndJoinRandom Photon.Pun.Demo.Cockpit.PunCockpitEmbed::AutoConnect
	ConnectAndJoinRandom_t1495527429 * ___AutoConnect_9;

public:
	inline static int32_t get_offset_of_PunCockpit_scene_5() { return static_cast<int32_t>(offsetof(PunCockpitEmbed_t1735463960, ___PunCockpit_scene_5)); }
	inline String_t* get_PunCockpit_scene_5() const { return ___PunCockpit_scene_5; }
	inline String_t** get_address_of_PunCockpit_scene_5() { return &___PunCockpit_scene_5; }
	inline void set_PunCockpit_scene_5(String_t* value)
	{
		___PunCockpit_scene_5 = value;
		Il2CppCodeGenWriteBarrier((&___PunCockpit_scene_5), value);
	}

	inline static int32_t get_offset_of_EmbeddCockpit_6() { return static_cast<int32_t>(offsetof(PunCockpitEmbed_t1735463960, ___EmbeddCockpit_6)); }
	inline bool get_EmbeddCockpit_6() const { return ___EmbeddCockpit_6; }
	inline bool* get_address_of_EmbeddCockpit_6() { return &___EmbeddCockpit_6; }
	inline void set_EmbeddCockpit_6(bool value)
	{
		___EmbeddCockpit_6 = value;
	}

	inline static int32_t get_offset_of_CockpitGameTitle_7() { return static_cast<int32_t>(offsetof(PunCockpitEmbed_t1735463960, ___CockpitGameTitle_7)); }
	inline String_t* get_CockpitGameTitle_7() const { return ___CockpitGameTitle_7; }
	inline String_t** get_address_of_CockpitGameTitle_7() { return &___CockpitGameTitle_7; }
	inline void set_CockpitGameTitle_7(String_t* value)
	{
		___CockpitGameTitle_7 = value;
		Il2CppCodeGenWriteBarrier((&___CockpitGameTitle_7), value);
	}

	inline static int32_t get_offset_of_LoadingIndicator_8() { return static_cast<int32_t>(offsetof(PunCockpitEmbed_t1735463960, ___LoadingIndicator_8)); }
	inline GameObject_t1113636619 * get_LoadingIndicator_8() const { return ___LoadingIndicator_8; }
	inline GameObject_t1113636619 ** get_address_of_LoadingIndicator_8() { return &___LoadingIndicator_8; }
	inline void set_LoadingIndicator_8(GameObject_t1113636619 * value)
	{
		___LoadingIndicator_8 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingIndicator_8), value);
	}

	inline static int32_t get_offset_of_AutoConnect_9() { return static_cast<int32_t>(offsetof(PunCockpitEmbed_t1735463960, ___AutoConnect_9)); }
	inline ConnectAndJoinRandom_t1495527429 * get_AutoConnect_9() const { return ___AutoConnect_9; }
	inline ConnectAndJoinRandom_t1495527429 ** get_address_of_AutoConnect_9() { return &___AutoConnect_9; }
	inline void set_AutoConnect_9(ConnectAndJoinRandom_t1495527429 * value)
	{
		___AutoConnect_9 = value;
		Il2CppCodeGenWriteBarrier((&___AutoConnect_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUNCOCKPITEMBED_T1735463960_H
#ifndef YOURLOGSCRIPT_T3397472296_H
#define YOURLOGSCRIPT_T3397472296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YourLogScript
struct  YourLogScript_t3397472296  : public MonoBehaviourPunCallbacks_t1810614660
{
public:
	// System.Int32 YourLogScript::hitCount
	int32_t ___hitCount_5;
	// System.Int32 YourLogScript::blowCount
	int32_t ___blowCount_6;
	// UnityEngine.GameObject YourLogScript::yourLogTextView
	GameObject_t1113636619 * ___yourLogTextView_7;
	// UnityEngine.GameObject YourLogScript::content
	GameObject_t1113636619 * ___content_8;
	// YourLogTextView YourLogScript::script
	YourLogTextView_t1250600108 * ___script_9;
	// UnityEngine.GameObject YourLogScript::enter
	GameObject_t1113636619 * ___enter_10;
	// Enter YourLogScript::enterScript
	Enter_t1090004723 * ___enterScript_11;
	// System.String YourLogScript::setYourNum
	String_t* ___setYourNum_12;

public:
	inline static int32_t get_offset_of_hitCount_5() { return static_cast<int32_t>(offsetof(YourLogScript_t3397472296, ___hitCount_5)); }
	inline int32_t get_hitCount_5() const { return ___hitCount_5; }
	inline int32_t* get_address_of_hitCount_5() { return &___hitCount_5; }
	inline void set_hitCount_5(int32_t value)
	{
		___hitCount_5 = value;
	}

	inline static int32_t get_offset_of_blowCount_6() { return static_cast<int32_t>(offsetof(YourLogScript_t3397472296, ___blowCount_6)); }
	inline int32_t get_blowCount_6() const { return ___blowCount_6; }
	inline int32_t* get_address_of_blowCount_6() { return &___blowCount_6; }
	inline void set_blowCount_6(int32_t value)
	{
		___blowCount_6 = value;
	}

	inline static int32_t get_offset_of_yourLogTextView_7() { return static_cast<int32_t>(offsetof(YourLogScript_t3397472296, ___yourLogTextView_7)); }
	inline GameObject_t1113636619 * get_yourLogTextView_7() const { return ___yourLogTextView_7; }
	inline GameObject_t1113636619 ** get_address_of_yourLogTextView_7() { return &___yourLogTextView_7; }
	inline void set_yourLogTextView_7(GameObject_t1113636619 * value)
	{
		___yourLogTextView_7 = value;
		Il2CppCodeGenWriteBarrier((&___yourLogTextView_7), value);
	}

	inline static int32_t get_offset_of_content_8() { return static_cast<int32_t>(offsetof(YourLogScript_t3397472296, ___content_8)); }
	inline GameObject_t1113636619 * get_content_8() const { return ___content_8; }
	inline GameObject_t1113636619 ** get_address_of_content_8() { return &___content_8; }
	inline void set_content_8(GameObject_t1113636619 * value)
	{
		___content_8 = value;
		Il2CppCodeGenWriteBarrier((&___content_8), value);
	}

	inline static int32_t get_offset_of_script_9() { return static_cast<int32_t>(offsetof(YourLogScript_t3397472296, ___script_9)); }
	inline YourLogTextView_t1250600108 * get_script_9() const { return ___script_9; }
	inline YourLogTextView_t1250600108 ** get_address_of_script_9() { return &___script_9; }
	inline void set_script_9(YourLogTextView_t1250600108 * value)
	{
		___script_9 = value;
		Il2CppCodeGenWriteBarrier((&___script_9), value);
	}

	inline static int32_t get_offset_of_enter_10() { return static_cast<int32_t>(offsetof(YourLogScript_t3397472296, ___enter_10)); }
	inline GameObject_t1113636619 * get_enter_10() const { return ___enter_10; }
	inline GameObject_t1113636619 ** get_address_of_enter_10() { return &___enter_10; }
	inline void set_enter_10(GameObject_t1113636619 * value)
	{
		___enter_10 = value;
		Il2CppCodeGenWriteBarrier((&___enter_10), value);
	}

	inline static int32_t get_offset_of_enterScript_11() { return static_cast<int32_t>(offsetof(YourLogScript_t3397472296, ___enterScript_11)); }
	inline Enter_t1090004723 * get_enterScript_11() const { return ___enterScript_11; }
	inline Enter_t1090004723 ** get_address_of_enterScript_11() { return &___enterScript_11; }
	inline void set_enterScript_11(Enter_t1090004723 * value)
	{
		___enterScript_11 = value;
		Il2CppCodeGenWriteBarrier((&___enterScript_11), value);
	}

	inline static int32_t get_offset_of_setYourNum_12() { return static_cast<int32_t>(offsetof(YourLogScript_t3397472296, ___setYourNum_12)); }
	inline String_t* get_setYourNum_12() const { return ___setYourNum_12; }
	inline String_t** get_address_of_setYourNum_12() { return &___setYourNum_12; }
	inline void set_setYourNum_12(String_t* value)
	{
		___setYourNum_12 = value;
		Il2CppCodeGenWriteBarrier((&___setYourNum_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOURLOGSCRIPT_T3397472296_H
#ifndef YOURLOGTEXTVIEW_T1250600108_H
#define YOURLOGTEXTVIEW_T1250600108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// YourLogTextView
struct  YourLogTextView_t1250600108  : public MonoBehaviourPunCallbacks_t1810614660
{
public:
	// UnityEngine.UI.Text YourLogTextView::yourLogText
	Text_t1901882714 * ___yourLogText_5;
	// System.String YourLogTextView::setYourNum
	String_t* ___setYourNum_6;

public:
	inline static int32_t get_offset_of_yourLogText_5() { return static_cast<int32_t>(offsetof(YourLogTextView_t1250600108, ___yourLogText_5)); }
	inline Text_t1901882714 * get_yourLogText_5() const { return ___yourLogText_5; }
	inline Text_t1901882714 ** get_address_of_yourLogText_5() { return &___yourLogText_5; }
	inline void set_yourLogText_5(Text_t1901882714 * value)
	{
		___yourLogText_5 = value;
		Il2CppCodeGenWriteBarrier((&___yourLogText_5), value);
	}

	inline static int32_t get_offset_of_setYourNum_6() { return static_cast<int32_t>(offsetof(YourLogTextView_t1250600108, ___setYourNum_6)); }
	inline String_t* get_setYourNum_6() const { return ___setYourNum_6; }
	inline String_t** get_address_of_setYourNum_6() { return &___setYourNum_6; }
	inline void set_setYourNum_6(String_t* value)
	{
		___setYourNum_6 = value;
		Il2CppCodeGenWriteBarrier((&___setYourNum_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YOURLOGTEXTVIEW_T1250600108_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (U3CclearStatusU3Ec__Iterator0_t675715044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[4] = 
{
	U3CclearStatusU3Ec__Iterator0_t675715044::get_offset_of_U24this_0(),
	U3CclearStatusU3Ec__Iterator0_t675715044::get_offset_of_U24current_1(),
	U3CclearStatusU3Ec__Iterator0_t675715044::get_offset_of_U24disposing_2(),
	U3CclearStatusU3Ec__Iterator0_t675715044::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (UserIdField_t3654608899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[4] = 
{
	UserIdField_t3654608899::get_offset_of_Manager_4(),
	UserIdField_t3654608899::get_offset_of_PropertyValueInput_5(),
	UserIdField_t3654608899::get_offset_of__cache_6(),
	UserIdField_t3654608899::get_offset_of_registered_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (AppVersionProperty_t243272796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[2] = 
{
	AppVersionProperty_t243272796::get_offset_of_Text_4(),
	AppVersionProperty_t243272796::get_offset_of__cache_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (BestRegionInPrefsProperty_t40435295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[2] = 
{
	BestRegionInPrefsProperty_t40435295::get_offset_of_Text_7(),
	BestRegionInPrefsProperty_t40435295::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (CloudRegionProperty_t826945682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[2] = 
{
	CloudRegionProperty_t826945682::get_offset_of_Text_7(),
	CloudRegionProperty_t826945682::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (CountOfPlayersInRoomProperty_t2018334201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[2] = 
{
	CountOfPlayersInRoomProperty_t2018334201::get_offset_of_Text_7(),
	CountOfPlayersInRoomProperty_t2018334201::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (CountOfPlayersOnMasterProperty_t2428894331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2706[2] = 
{
	CountOfPlayersOnMasterProperty_t2428894331::get_offset_of_Text_7(),
	CountOfPlayersOnMasterProperty_t2428894331::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (CountOfPlayersProperty_t1754900899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[2] = 
{
	CountOfPlayersProperty_t1754900899::get_offset_of_Text_7(),
	CountOfPlayersProperty_t1754900899::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (CountOfRoomsProperty_t250151811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[2] = 
{
	CountOfRoomsProperty_t250151811::get_offset_of_Text_7(),
	CountOfRoomsProperty_t250151811::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (CurrentRoomAutoCleanupProperty_t559513298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2709[2] = 
{
	CurrentRoomAutoCleanupProperty_t559513298::get_offset_of_Text_7(),
	CurrentRoomAutoCleanupProperty_t559513298::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (CurrentRoomEmptyRoomTtlProperty_t2509994768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2710[2] = 
{
	CurrentRoomEmptyRoomTtlProperty_t2509994768::get_offset_of_Text_7(),
	CurrentRoomEmptyRoomTtlProperty_t2509994768::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (CurrentRoomExpectedUsersProperty_t941134229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2711[2] = 
{
	CurrentRoomExpectedUsersProperty_t941134229::get_offset_of_Text_7(),
	CurrentRoomExpectedUsersProperty_t941134229::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (CurrentRoomIsOfflineProperty_t1978210793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2712[2] = 
{
	CurrentRoomIsOfflineProperty_t1978210793::get_offset_of_Text_7(),
	CurrentRoomIsOfflineProperty_t1978210793::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (CurrentRoomIsOpenProperty_t3485205974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[2] = 
{
	CurrentRoomIsOpenProperty_t3485205974::get_offset_of_Text_7(),
	CurrentRoomIsOpenProperty_t3485205974::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (CurrentRoomIsVisibleProperty_t2609348605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[2] = 
{
	CurrentRoomIsVisibleProperty_t2609348605::get_offset_of_Text_7(),
	CurrentRoomIsVisibleProperty_t2609348605::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (CurrentRoomMasterClientIdProperty_t441479020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2715[2] = 
{
	CurrentRoomMasterClientIdProperty_t441479020::get_offset_of_Text_7(),
	CurrentRoomMasterClientIdProperty_t441479020::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (CurrentRoomMaxPlayersProperty_t269097822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2716[2] = 
{
	CurrentRoomMaxPlayersProperty_t269097822::get_offset_of_Text_7(),
	CurrentRoomMaxPlayersProperty_t269097822::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (CurrentRoomNameProperty_t1589573469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2717[2] = 
{
	CurrentRoomNameProperty_t1589573469::get_offset_of_Text_7(),
	CurrentRoomNameProperty_t1589573469::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (CurrentRoomPlayerCountProperty_t895675237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2718[2] = 
{
	CurrentRoomPlayerCountProperty_t895675237::get_offset_of_Text_7(),
	CurrentRoomPlayerCountProperty_t895675237::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (CurrentRoomPlayerTtlProperty_t197288263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[2] = 
{
	CurrentRoomPlayerTtlProperty_t197288263::get_offset_of_Text_7(),
	CurrentRoomPlayerTtlProperty_t197288263::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (CurrentRoomPropertiesListedInLobbyProperty_t1065004439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2720[2] = 
{
	CurrentRoomPropertiesListedInLobbyProperty_t1065004439::get_offset_of_Text_7(),
	CurrentRoomPropertiesListedInLobbyProperty_t1065004439::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (GameVersionProperty_t2599490529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2721[2] = 
{
	GameVersionProperty_t2599490529::get_offset_of_Text_4(),
	GameVersionProperty_t2599490529::get_offset_of__cache_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (IsConnectedAndReadyProperty_t2186703361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[2] = 
{
	IsConnectedAndReadyProperty_t2186703361::get_offset_of_Text_7(),
	IsConnectedAndReadyProperty_t2186703361::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (IsConnectedProperty_t3457877926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2723[2] = 
{
	IsConnectedProperty_t3457877926::get_offset_of_Text_7(),
	IsConnectedProperty_t3457877926::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (OfflineModeProperty_t4178522675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2724[2] = 
{
	OfflineModeProperty_t4178522675::get_offset_of_Text_7(),
	OfflineModeProperty_t4178522675::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (PingProperty_t304019737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2725[2] = 
{
	PingProperty_t304019737::get_offset_of_Text_7(),
	PingProperty_t304019737::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (PropertyListenerBase_t1226442861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2726[3] = 
{
	PropertyListenerBase_t1226442861::get_offset_of_UpdateIndicator_4(),
	PropertyListenerBase_t1226442861::get_offset_of_fadeInstruction_5(),
	PropertyListenerBase_t1226442861::get_offset_of_Duration_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (U3CFadeOutU3Ec__Iterator0_t712175311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[7] = 
{
	U3CFadeOutU3Ec__Iterator0_t712175311::get_offset_of_U3CelapsedTimeU3E__0_0(),
	U3CFadeOutU3Ec__Iterator0_t712175311::get_offset_of_image_1(),
	U3CFadeOutU3Ec__Iterator0_t712175311::get_offset_of_U3CcU3E__0_2(),
	U3CFadeOutU3Ec__Iterator0_t712175311::get_offset_of_U24this_3(),
	U3CFadeOutU3Ec__Iterator0_t712175311::get_offset_of_U24current_4(),
	U3CFadeOutU3Ec__Iterator0_t712175311::get_offset_of_U24disposing_5(),
	U3CFadeOutU3Ec__Iterator0_t712175311::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (ServerAddressProperty_t960619058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[2] = 
{
	ServerAddressProperty_t960619058::get_offset_of_Text_4(),
	ServerAddressProperty_t960619058::get_offset_of__cache_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (ServerProperty_t2037082094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2729[2] = 
{
	ServerProperty_t2037082094::get_offset_of_Text_7(),
	ServerProperty_t2037082094::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (ScoreHelper_t1549971229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[2] = 
{
	ScoreHelper_t1549971229::get_offset_of_Score_4(),
	ScoreHelper_t1549971229::get_offset_of__currentScore_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (PunCockpitEmbed_t1735463960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2731[5] = 
{
	PunCockpitEmbed_t1735463960::get_offset_of_PunCockpit_scene_5(),
	PunCockpitEmbed_t1735463960::get_offset_of_EmbeddCockpit_6(),
	PunCockpitEmbed_t1735463960::get_offset_of_CockpitGameTitle_7(),
	PunCockpitEmbed_t1735463960::get_offset_of_LoadingIndicator_8(),
	PunCockpitEmbed_t1735463960::get_offset_of_AutoConnect_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (U3CStartU3Ec__Iterator0_t496713993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[4] = 
{
	U3CStartU3Ec__Iterator0_t496713993::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t496713993::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t496713993::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t496713993::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (DocLinks_t1816087656), -1, sizeof(DocLinks_t1816087656_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2733[9] = 
{
	DocLinks_t1816087656_StaticFields::get_offset_of_Version_0(),
	DocLinks_t1816087656_StaticFields::get_offset_of_Language_1(),
	DocLinks_t1816087656_StaticFields::get_offset_of_Product_2(),
	DocLinks_t1816087656_StaticFields::get_offset_of_ApiUrlRoot_3(),
	DocLinks_t1816087656_StaticFields::get_offset_of_DocUrlFormat_4(),
	DocLinks_t1816087656_StaticFields::get_offset_of_ProductsFolders_5(),
	DocLinks_t1816087656_StaticFields::get_offset_of_ApiLanguagesFolder_6(),
	DocLinks_t1816087656_StaticFields::get_offset_of_DocLanguagesFolder_7(),
	DocLinks_t1816087656_StaticFields::get_offset_of_VersionsFolder_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (DocTypes_t77080171)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2734[3] = 
{
	DocTypes_t77080171::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (Products_t1821662994)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2735[8] = 
{
	Products_t1821662994::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (Versions_t239389637)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2736[4] = 
{
	Versions_t239389637::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (Languages_t2208671880)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2737[5] = 
{
	Languages_t2208671880::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (BackScript_t341555447), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (ChoiceEnter_t2353860254), -1, sizeof(ChoiceEnter_t2353860254_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2739[17] = 
{
	ChoiceEnter_t2353860254_StaticFields::get_offset_of_setFirstNum_5(),
	ChoiceEnter_t2353860254_StaticFields::get_offset_of_setSecondNum_6(),
	ChoiceEnter_t2353860254_StaticFields::get_offset_of_setThirdNum_7(),
	ChoiceEnter_t2353860254::get_offset_of_hasSetFirst_8(),
	ChoiceEnter_t2353860254::get_offset_of_hasSetSecond_9(),
	ChoiceEnter_t2353860254::get_offset_of_hasSetThird_10(),
	ChoiceEnter_t2353860254::get_offset_of_hasSet_11(),
	ChoiceEnter_t2353860254::get_offset_of_numError_12(),
	ChoiceEnter_t2353860254::get_offset_of_enemyWaiting_13(),
	ChoiceEnter_t2353860254::get_offset_of_isChoiceEntered_14(),
	ChoiceEnter_t2353860254::get_offset_of_isChildChoiceEntered_15(),
	ChoiceEnter_t2353860254::get_offset_of_hasLoad_16(),
	ChoiceEnter_t2353860254::get_offset_of_timerObject_17(),
	ChoiceEnter_t2353860254::get_offset_of_timer_18(),
	ChoiceEnter_t2353860254::get_offset_of_firstNum0_19(),
	ChoiceEnter_t2353860254::get_offset_of_secondNum1_20(),
	ChoiceEnter_t2353860254::get_offset_of_thirdNum2_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (EnemyLogScript_t943560016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2740[7] = 
{
	EnemyLogScript_t943560016::get_offset_of_hitCount_5(),
	EnemyLogScript_t943560016::get_offset_of_blowCount_6(),
	EnemyLogScript_t943560016::get_offset_of_enemyLogTextView_7(),
	EnemyLogScript_t943560016::get_offset_of_content_8(),
	EnemyLogScript_t943560016::get_offset_of_enter_9(),
	EnemyLogScript_t943560016::get_offset_of_enterScript_10(),
	EnemyLogScript_t943560016::get_offset_of_script_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (EnemyLogTextView_t71717095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2741[2] = 
{
	EnemyLogTextView_t71717095::get_offset_of_enemyLogText_5(),
	EnemyLogTextView_t71717095::get_offset_of_setEnemyNum_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (Enter_t1090004723), -1, sizeof(Enter_t1090004723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2742[35] = 
{
	Enter_t1090004723::get_offset_of_yourFirstNum_5(),
	Enter_t1090004723::get_offset_of_yourSecondNum_6(),
	Enter_t1090004723::get_offset_of_yourThirdNum_7(),
	Enter_t1090004723_StaticFields::get_offset_of_enemyFirstNum_8(),
	Enter_t1090004723_StaticFields::get_offset_of_enemySecondNum_9(),
	Enter_t1090004723_StaticFields::get_offset_of_enemyThirdNum_10(),
	Enter_t1090004723_StaticFields::get_offset_of_setFirstNum_11(),
	Enter_t1090004723_StaticFields::get_offset_of_setSecondNum_12(),
	Enter_t1090004723_StaticFields::get_offset_of_setThirdNum_13(),
	Enter_t1090004723_StaticFields::get_offset_of_setEnemyFirstNum_14(),
	Enter_t1090004723_StaticFields::get_offset_of_setEnemySecondNum_15(),
	Enter_t1090004723_StaticFields::get_offset_of_setEnemyThirdNum_16(),
	Enter_t1090004723::get_offset_of_yourLog_17(),
	Enter_t1090004723::get_offset_of_yourLogScript_18(),
	Enter_t1090004723::get_offset_of_enemyLog_19(),
	Enter_t1090004723::get_offset_of_enemyLogScript_20(),
	Enter_t1090004723::get_offset_of_turn_21(),
	Enter_t1090004723::get_offset_of_turnScript_22(),
	Enter_t1090004723::get_offset_of_timerObject_23(),
	Enter_t1090004723::get_offset_of_timerScript_24(),
	Enter_t1090004723::get_offset_of_timer_25(),
	Enter_t1090004723::get_offset_of_resultManager_26(),
	Enter_t1090004723::get_offset_of_resultManagerScript_27(),
	Enter_t1090004723::get_offset_of_numError_28(),
	Enter_t1090004723::get_offset_of_enemyWaiting_29(),
	Enter_t1090004723::get_offset_of_hasSetFirst_30(),
	Enter_t1090004723::get_offset_of_hasSetSecond_31(),
	Enter_t1090004723::get_offset_of_hasSetThird_32(),
	Enter_t1090004723::get_offset_of_hasSet_33(),
	Enter_t1090004723::get_offset_of_isEntered_34(),
	Enter_t1090004723::get_offset_of_isEnemyEntered_35(),
	Enter_t1090004723::get_offset_of_isFinishedReadingSetEnemyNum_36(),
	Enter_t1090004723::get_offset_of_isYouWin_37(),
	Enter_t1090004723::get_offset_of_isYouLose_38(),
	Enter_t1090004723::get_offset_of_hasLoad_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (FirstNum_t3448397475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[1] = 
{
	FirstNum_t3448397475::get_offset_of_yellowNum_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (HomeScript_t3660720983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2744[2] = 
{
	HomeScript_t3660720983::get_offset_of_resultManager_4(),
	HomeScript_t3660720983::get_offset_of_resultManagerScript_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (ItemScript_t3045424977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[13] = 
{
	ItemScript_t3045424977::get_offset_of_enemyFirstNum_5(),
	ItemScript_t3045424977::get_offset_of_enemySecondNum_6(),
	ItemScript_t3045424977::get_offset_of_enemyThirdNum_7(),
	ItemScript_t3045424977::get_offset_of_yourLTV_8(),
	ItemScript_t3045424977::get_offset_of_yourLTVScript_9(),
	ItemScript_t3045424977::get_offset_of_yourContent_10(),
	ItemScript_t3045424977::get_offset_of_enemyLTV_11(),
	ItemScript_t3045424977::get_offset_of_enemyLTVScript_12(),
	ItemScript_t3045424977::get_offset_of_enemyContent_13(),
	ItemScript_t3045424977::get_offset_of_firstHL_14(),
	ItemScript_t3045424977::get_offset_of_secondHL_15(),
	ItemScript_t3045424977::get_offset_of_thirdHL_16(),
	ItemScript_t3045424977::get_offset_of_isUsedHL_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (Launcher_t75719789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[1] = 
{
	Launcher_t75719789::get_offset_of_gameVersion_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (ResultManager_t2208194647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[1] = 
{
	ResultManager_t2208194647::get_offset_of_resultCoad_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (ResultScript_t2333723299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2748[4] = 
{
	ResultScript_t2333723299::get_offset_of_resultCoad_4(),
	ResultScript_t2333723299::get_offset_of_resultText_5(),
	ResultScript_t2333723299::get_offset_of_resultManager_6(),
	ResultScript_t2333723299::get_offset_of_resultManagerScript_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (RetryScript_t1070381244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[2] = 
{
	RetryScript_t1070381244::get_offset_of_resultManager_4(),
	RetryScript_t1070381244::get_offset_of_resultManagerScript_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (SecondNum_t3374771210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2750[1] = 
{
	SecondNum_t3374771210::get_offset_of_yellowNum_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (SomebodyScript_t208421239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (ThirdNum_t2751324300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2752[1] = 
{
	ThirdNum_t2751324300::get_offset_of_yellowNum_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (TimerScript_t1618777791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[3] = 
{
	TimerScript_t1618777791::get_offset_of_timerText_4(),
	TimerScript_t1618777791::get_offset_of_timer_5(),
	TimerScript_t1618777791::get_offset_of_isStoped_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (TurnScript_t2279615295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[2] = 
{
	TurnScript_t2279615295::get_offset_of_turnCount_4(),
	TurnScript_t2279615295::get_offset_of_turnText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (YourLogScript_t3397472296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2755[8] = 
{
	YourLogScript_t3397472296::get_offset_of_hitCount_5(),
	YourLogScript_t3397472296::get_offset_of_blowCount_6(),
	YourLogScript_t3397472296::get_offset_of_yourLogTextView_7(),
	YourLogScript_t3397472296::get_offset_of_content_8(),
	YourLogScript_t3397472296::get_offset_of_script_9(),
	YourLogScript_t3397472296::get_offset_of_enter_10(),
	YourLogScript_t3397472296::get_offset_of_enterScript_11(),
	YourLogScript_t3397472296::get_offset_of_setYourNum_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (YourLogTextView_t1250600108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2756[2] = 
{
	YourLogTextView_t1250600108::get_offset_of_yourLogText_5(),
	YourLogTextView_t1250600108::get_offset_of_setYourNum_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (YourNum_t2796948509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2757[4] = 
{
	YourNum_t2796948509::get_offset_of_yourNumText_4(),
	YourNum_t2796948509::get_offset_of_yourFirstNum_5(),
	YourNum_t2796948509::get_offset_of_yourSecondNum_6(),
	YourNum_t2796948509::get_offset_of_yourThirdNum_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255369), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2758[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U24fieldU2DE74588860CA220F5327520E16546CBB6016903F4_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (U24ArrayTypeU3D512_t3839624093)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D512_t3839624093 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
